package com.ggs.makeawishcometrueuniverse.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ggs.makeawishcometrueuniverse.MainActivity;
import com.ggs.makeawishcometrueuniverse.MakeWishFragment;
import com.ggs.makeawishcometrueuniverse.R;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

public class Space extends AppCompatActivity {
TextView sended;
Button back;
ImageView cosmo;
    Animation animation,ses;
    private InterstitialAd mInterstitialAd;



//    private static final String AD_UNIT_ID = "ca-app-pub-7802337876801234/2485172978";


    private InterstitialAd interstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_space);
        interstitialAd = new InterstitialAd(this);
//interstitial.setAdUnitId(AdRequest.DEVICE_ID_EMULATOR);
// Create ad request.
        AdRequest adRequestI = new AdRequest.Builder().addTestDevice("ca-app-pub-7802337876801234/2485172978").build();
        interstitialAd.setAdUnitId("ca-app-pub-7802337876801234/2485172978");
// Begin loading your interstitial.
        interstitialAd.loadAd(adRequestI);

        // Defined in res/values/strings.xml


        // Initialize the Mobile Ads SDK.
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {}
        });

        // Create the InterstitialAd and set the adUnitId.


        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
               // Toast.makeText(Space.this, "onAdLoaded()", Toast.LENGTH_SHORT).show();
                showInterstitial();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
//                Toast.makeText(Space.this,
//                        "onAdFailedToLoad() with error code: " + errorCode,
//                        Toast.LENGTH_SHORT).show();
                canvel();
            }

            @Override
            public void onAdClosed() {
                canvel();
            }



        });









        back = findViewById(R.id.back);
sended = findViewById(R.id.sended);
cosmo = findViewById(R.id.cosmo);
        animation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fout);
        ses = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.rer);



cosmo.startAnimation(animation);
sended.startAnimation(ses);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



               canvel();





            }

        });

    }

    private void canvel() {
        Intent bb = new Intent(Space.this, MainActivity.class);
        startActivity(bb);
        finish();
    }

    private void showInterstitial() {
        interstitialAd.show();
        if (interstitialAd != null && interstitialAd.isLoaded()) {
            interstitialAd.show();
        } else {
         //   Toast.makeText(this, "Ad did not load", Toast.LENGTH_SHORT).show();
            startGame();
        }
   
    }

    private void startGame() {
        if (!interstitialAd.isLoading() && !interstitialAd.isLoaded()) {
            AdRequest adRequest = new AdRequest.Builder().build();
            interstitialAd.loadAd(adRequest);
        }

    }
}