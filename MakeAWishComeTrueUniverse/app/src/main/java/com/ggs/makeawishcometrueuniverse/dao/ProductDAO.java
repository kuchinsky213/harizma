package com.ggs.makeawishcometrueuniverse.dao;





import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.ggs.makeawishcometrueuniverse.entity.Product;

import java.util.List;

@Dao
public interface ProductDAO {
    @Insert
    void insert(Product product);

    @Update
    void update(Product product);

    @Delete
    void delete(Product product);

    @Query("DELETE FROM products")
    void deleteAll();

    @Query("SELECT * from products ORDER BY product_name ASC")
    LiveData<List<Product>> getAllProduct();
}
