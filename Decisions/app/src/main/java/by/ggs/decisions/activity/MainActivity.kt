package by.ggs.decisions.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

import by.ggs.decisions.R
import by.ggs.decisions.app.addFragment
import by.ggs.decisions.app.setFragment
import by.ggs.decisions.fragment.IssuesFragment
import by.ggs.decisions.fragment.PreferencesFragment
import kotlinx.android.synthetic.main.toolbar.*

class MainActivity : AppCompatActivity() {
	override fun onSupportNavigateUp(): Boolean {
		val fm = supportFragmentManager
		if (fm.backStackEntryCount > 0) {
			fm.popBackStack()
		} else {
			finish()
		}
		return true
	}

	override fun onCreate(state: Bundle?) {
		super.onCreate(state)
		setContentView(R.layout.activity_main)
		initToolbar()

		if (state == null) {
			setFragment(supportFragmentManager, IssuesFragment())
			if (intent?.getBooleanExtra(OPEN_PREFERENCES, false) == true) {
				addFragment(supportFragmentManager, PreferencesFragment())
			}
		}
	}

	private fun initToolbar() {
		setSupportActionBar(toolbar)
		supportFragmentManager.addOnBackStackChangedListener {
			updateUpArrow()
		}
		updateUpArrow()
	}

	private fun updateUpArrow() {
		supportActionBar?.setDisplayHomeAsUpEnabled(
			supportFragmentManager.backStackEntryCount > 0
		)
	}

	companion object {
		const val OPEN_PREFERENCES = "open_preferences"
	}
}
