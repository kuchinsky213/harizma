package com.ggs.universe_wish.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.ggs.universe_wish.Model.Notes;
import com.ggs.universe_wish.R;
import com.ggs.universe_wish.ViewModel.NotesViewModel;
import com.ggs.universe_wish.databinding.ActivityUpdateNotesBinding;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class UpdateNotesActivity extends AppCompatActivity {
ActivityUpdateNotesBinding binding;
    String priority = "1";
    NotesViewModel notesViewModel;
    String  stitle, ssubtitle, snotes, spriority;
        int iid;
    private static final String AD_UNIT_ID = "ca-app-pub-7802337876801234/5649399736";
    private static final String TAG = "MyActivity";

    private InterstitialAd interstitialAd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivityUpdateNotesBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        // Initialize the Mobile Ads SDK.
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {}
        });

        loadAd();

        notesViewModel = ViewModelProviders.of(this).get(NotesViewModel.class);
        iid = getIntent().getIntExtra("id",0);
        stitle = getIntent().getStringExtra("title");
        ssubtitle = getIntent().getStringExtra("subTitle");
        spriority = getIntent().getStringExtra("priority");
        snotes = getIntent().getStringExtra("note");

        binding.upTitle.setText(stitle);
        binding.upSubTitle.setText(ssubtitle);
        binding.upNotes.setText(snotes);


        switch (spriority) {
            case "1":
                binding.greenPriority.setImageResource(R.drawable.ic_done);
                binding.redPriority.setImageResource(0);
                binding.yellowPriority.setImageResource(0);
                priority="1";
                break;
            case "2":
                binding.yellowPriority.setImageResource(R.drawable.ic_done);
                binding.redPriority.setImageResource(0);
                binding.greenPriority.setImageResource(0);
                priority="2";
                break;
            case "3":
                binding.redPriority.setImageResource(R.drawable.ic_done);
                binding.greenPriority.setImageResource(0);
                binding.yellowPriority.setImageResource(0);
                priority="3";
                break;
        }




        binding.greenPriority.setOnClickListener(v -> {
            binding.greenPriority.setImageResource(R.drawable.ic_done);
            binding.redPriority.setImageResource(0);
            binding.yellowPriority.setImageResource(0);
            priority="1";
        });

        binding.yellowPriority.setOnClickListener(v -> {

            binding.yellowPriority.setImageResource(R.drawable.ic_done);
            binding.redPriority.setImageResource(0);
            binding.greenPriority.setImageResource(0);
            priority="2";
        });

        binding.redPriority.setOnClickListener(v -> {
            binding.redPriority.setImageResource(R.drawable.ic_done);
            binding.greenPriority.setImageResource(0);
            binding.yellowPriority.setImageResource(0);
            priority="3";
        });


        binding.updateNotesBtn.setOnClickListener(v -> {
           String title = binding.upTitle.getText().toString();
           String subtitle = binding.upSubTitle.getText().toString();
           String notes = binding.upNotes.getText().toString();
            UpdateNotes(title, subtitle, notes);
            showInterstitial();
        });



    }

    public void loadAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        InterstitialAd.load(
                this,
                AD_UNIT_ID,
                adRequest,
                new InterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                        // The mInterstitialAd reference will be null until
                        // an ad is loaded.
                        UpdateNotesActivity.this.interstitialAd = interstitialAd;
                        Log.i(TAG, "onAdLoaded");

                        interstitialAd.setFullScreenContentCallback(
                                new FullScreenContentCallback() {
                                    @Override
                                    public void onAdDismissedFullScreenContent() {
                                        // Called when fullscreen content is dismissed.
                                        // Make sure to set your reference to null so you don't
                                        // show it a second time.
                                        UpdateNotesActivity.this.interstitialAd = null;
                                        Log.d("TAG", "The ad was dismissed.");
                                    }

                                    @Override
                                    public void onAdFailedToShowFullScreenContent(AdError adError) {
                                        // Called when fullscreen content failed to show.
                                        // Make sure to set your reference to null so you don't
                                        // show it a second time.
                                        UpdateNotesActivity.this.interstitialAd = null;
                                        Log.d("TAG", "The ad failed to show.");
                                    }

                                    @Override
                                    public void onAdShowedFullScreenContent() {
                                        // Called when fullscreen content is shown.
                                        Log.d("TAG", "The ad was shown.");
                                    }
                                });
                    }

                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        // Handle the error
                        Log.i(TAG, loadAdError.getMessage());
                        interstitialAd = null;

                        String error =
                                String.format(
                                        "domain: %s, code: %d, message: %s",
                                        loadAdError.getDomain(), loadAdError.getCode(), loadAdError.getMessage());

                    }
                });
    }

    private void showInterstitial() {
        // Show the ad if it's ready. Otherwise toast and restart the game.
        if (interstitialAd != null) {
            interstitialAd.show(this);
        } else {
            loadAd();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        getMenuInflater().inflate(R.menu.delete_menu, menu);




        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {


        if (item.getItemId()==R.id.ic_delet){
            BottomSheetDialog sheetDialog = new BottomSheetDialog(UpdateNotesActivity.this,
                    R.style.BottomSheetStyle);

            View view = LayoutInflater.from(UpdateNotesActivity.this).inflate(R.layout.delete_bottom,
                    (LinearLayout)findViewById(R.id.bottomSheet));


            sheetDialog.setContentView(view);


            TextView yes, no;

            yes = view.findViewById(R.id.delete_yes);
            no = view.findViewById(R.id.delete_no);




            yes.setOnClickListener(v -> {
                showInterstitial();
                notesViewModel.deleteNote(iid);
                finish();

            });


            no.setOnClickListener(v -> {
                showInterstitial();
            sheetDialog.dismiss();
            });







            sheetDialog.show();
        }



        return true;
    }

    private void UpdateNotes(String title, String subtitle, String notes) {
        showInterstitial();

        CharSequence sequence = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

        Notes updateNotes = new Notes();

        updateNotes.id = iid;
        updateNotes.notesTitle = title;
        updateNotes.notesSubtitle = subtitle;
        updateNotes.notes = notes;
        updateNotes.notesDate = sequence.toString();
        updateNotes.notesPriority = priority;


        notesViewModel.updateNote(updateNotes);
        Toast.makeText(this, (R.string.cr), Toast.LENGTH_SHORT).show();
        finish();

    }
}