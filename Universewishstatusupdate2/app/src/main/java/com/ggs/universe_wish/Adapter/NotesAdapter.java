package com.ggs.universe_wish.Adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ggs.universe_wish.Activities.MainActivity;
import com.ggs.universe_wish.Activities.UpdateNotesActivity;
import com.ggs.universe_wish.Model.Notes;
import com.ggs.universe_wish.R;

import java.util.ArrayList;
import java.util.List;

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.notesViewholder> {
    MainActivity mainActivity;
    List<Notes> notes;
    List<Notes> allNotesItem;

    public NotesAdapter(MainActivity mainActivity, List<Notes> notes) {
        this.mainActivity = mainActivity;
        this.notes = notes;
        allNotesItem = new ArrayList<>(notes);
    }

    public void searchNotes(List<Notes> filteredName){
        this.notes = filteredName;
        notifyDataSetChanged();
    }

    @Override
    public notesViewholder onCreateViewHolder( ViewGroup parent, int viewType) {
        return new notesViewholder(LayoutInflater.from(mainActivity).inflate(R.layout.item_notes, parent, false));
    }

    @Override
    public void onBindViewHolder( NotesAdapter.notesViewholder holder, int position) {
        Notes note = notes.get(position);


        switch (note.notesPriority) {
            case "1":
                holder.notesPriority.setBackgroundResource(R.drawable.green_shape);
                break;
            case "2":
                holder.notesPriority.setBackgroundResource(R.drawable.yellow_shape);
                break;
            case "3":
                holder.notesPriority.setBackgroundResource(R.drawable.red_shape);
                break;
        }
        holder.title.setText(note.notesTitle);
        holder.subTitle.setText(note.notesSubtitle);
        holder.notesDate.setText(note.notesDate);

        holder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(mainActivity, UpdateNotesActivity.class);

            intent.putExtra("id", note.id);
            intent.putExtra("title", note.notesTitle);
            intent.putExtra("subTitle", note.notesSubtitle);
            intent.putExtra("priority", note.notesPriority);
            intent.putExtra("note", note.notes);

            mainActivity.startActivity(intent);


        });

    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    static class notesViewholder extends RecyclerView.ViewHolder {

        TextView title, subTitle, notesDate;

        View notesPriority;

                public notesViewholder( View itemView) {
                    super(itemView);

                title = itemView.findViewById(R.id.notesTitle);
                    subTitle = itemView.findViewById(R.id.notesSubTitle);
                    notesDate = itemView.findViewById(R.id.notesData);
                    notesPriority = itemView.findViewById(R.id.notesPriority);





        }
    }
}
