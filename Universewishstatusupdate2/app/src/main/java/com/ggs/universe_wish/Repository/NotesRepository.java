package com.ggs.universe_wish.Repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.ggs.universe_wish.Dao.NotesDao;
import com.ggs.universe_wish.Database.NotesDatabase;
import com.ggs.universe_wish.Model.Notes;

import java.util.List;

public class NotesRepository {


    public NotesDao notesDao;

    public LiveData<List<Notes>> getAllNotes;
    public LiveData<List<Notes>> highToLow;
    public LiveData<List<Notes>> lowtohigh;




    public NotesRepository(Application application){

        NotesDatabase database = NotesDatabase.getDatabaseInstance(application);
        notesDao=database.notesDao();
        getAllNotes = notesDao.getAllNotes();
        highToLow = notesDao.highToLow();
        lowtohigh = notesDao.lowToHigh();




    }

    public void insertNotes(Notes notes){
        notesDao.insertNotes(notes);
    }

    public void deleteNotes(int id){
        notesDao.deleteNotes(id);
    }

    public  void updateNotes(Notes notes){
        notesDao.updateNotes(notes);
    }


}
