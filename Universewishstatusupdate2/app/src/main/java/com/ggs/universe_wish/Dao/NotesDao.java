package com.ggs.universe_wish.Dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.ggs.universe_wish.Model.Notes;

import java.util.List;

@Dao
public interface NotesDao {

    @Query("SELECT * FROM Notes_Database")
    LiveData<List<Notes>> getAllNotes();

    //List<Notes> getAllNotes();


    @Query("SELECT * FROM Notes_Database ORDER BY notes_priority DESC")
    LiveData<List<Notes>> highToLow();


    @Query("SELECT * FROM Notes_Database ORDER BY notes_priority ASC")
    LiveData<List<Notes>> lowToHigh();






    @Insert
    public  void  insertNotes(Notes... notes);

    @Query("DELETE FROM Notes_Database WHERE id=:id")
    public  void deleteNotes(int id);



    @Update
    public void updateNotes(Notes notes);






}
