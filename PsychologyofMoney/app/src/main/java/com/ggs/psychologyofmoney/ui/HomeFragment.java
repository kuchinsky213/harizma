package com.ggs.psychologyofmoney.ui;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ggs.psychologyofmoney.Heart.CofAdapter;
import com.ggs.psychologyofmoney.Heart.CoffeeItem;
import com.ggs.psychologyofmoney.R;
import com.ggs.psychologyofmoney.RecyclerItemClickListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.util.ArrayList;


public class HomeFragment extends Fragment {

    private ArrayList<CoffeeItem> coffeeItems = new ArrayList<>();
    private InterstitialAd mInterstitialAd;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);

        RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new CofAdapter(coffeeItems, getActivity()));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        coffeeItems.add(new CoffeeItem(R.drawable.cup_alina, getString(R.string.niwee), "0", "0"));
        coffeeItems.add(new CoffeeItem(R.drawable.c2, getString(R.string.mi), "1", "0"));
        coffeeItems.add(new CoffeeItem(R.drawable.c1, getString(R.string.psih), "2", "0"));
        coffeeItems.add(new CoffeeItem(R.drawable.c3, getString(R.string.asp), "3", "0"));
        coffeeItems.add(new CoffeeItem(R.drawable.c4, getString(R.string.mangirl), "4", "0"));
        coffeeItems.add(new CoffeeItem(R.drawable.c5, getString(R.string.otric), "5", "0"));
        coffeeItems.add(new CoffeeItem(R.drawable.c6, getString(R.string.ubejd), "6", "0"));
        coffeeItems.add(new CoffeeItem(R.drawable.c7, getString(R.string.easy), "7", "0"));
        coffeeItems.add(new CoffeeItem(R.drawable.c8, getString(R.string.secret), "8", "0"));



//        recyclerView.addOnItemTouchListener(
//                new RecyclerItemClickListener(getContext(), recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
//                    @Override public void onItemClick(View view, int position) {
//                        Toast.makeText(getContext(), " Клик прошел", Toast.LENGTH_SHORT).show();
//                    }
//
//                    @Override public void onLongItemClick(View view, int position) {
//                        // do whatever
//                    }
//                })
//        );







        return root;
    }







}
