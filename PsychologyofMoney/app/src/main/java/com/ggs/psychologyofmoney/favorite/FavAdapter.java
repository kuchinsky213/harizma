package com.ggs.psychologyofmoney.favorite;

import com.google.android.gms.ads.InterstitialAd;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ggs.psychologyofmoney.Heart.FavDB;
import com.ggs.psychologyofmoney.Heart.FavItem;
import com.ggs.psychologyofmoney.R;
import com.ggs.psychologyofmoney.Text.AspektiDeneg;
import com.ggs.psychologyofmoney.Text.KakBednyaki;
import com.ggs.psychologyofmoney.Text.LegkoPrivlech;
import com.ggs.psychologyofmoney.Text.MujAndWom;
import com.ggs.psychologyofmoney.Text.Niwee;
import com.ggs.psychologyofmoney.Text.OtricanieDeneg;
import com.ggs.psychologyofmoney.Text.PsihologyPrivl;
import com.ggs.psychologyofmoney.Text.Secreti;
import com.ggs.psychologyofmoney.Text.Ubejdeniya;


import java.util.List;

public class FavAdapter extends RecyclerView.Adapter<FavAdapter.ViewHolder> {

    private InterstitialAd mInterstitialAd;
    private Context context;
    private List<FavItem> favItemList;
    private FavDB favDB;
  CardView keks;


 

    public FavAdapter(Context context, List<FavItem> favItemList) {
        this.context = context;
        this.favItemList = favItemList;


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fav_item,
                parent, false);


        favDB = new FavDB(context);
        return new ViewHolder(view);
    }



    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.favTextView.setText(favItemList.get(position).getItem_title());
        holder.favImageView.setImageResource(favItemList.get(position).getItem_image());
    }

    @Override
    public int getItemCount() {
        return favItemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView favTextView;
        Button favBtn;
        ImageView favImageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            favTextView = itemView.findViewById(R.id.favTextView);
            favBtn = itemView.findViewById(R.id.favBtn);
            favImageView = itemView.findViewById(R.id.favImageView);
            keks = itemView.findViewById(R.id.keks);
            //remove from fav after click
            favBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    final FavItem favItem = favItemList.get(position);
                    favDB.remove_fav(favItem.getKey_id());
                    removeItem(position);
                }
            });





            keks.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                  //  final FavItem favItem = favItemList.get(favTextView);



                    if(favTextView.getText().toString().contains("«Нищее»") || favTextView.getText().toString().contains("\"Poor\" thinking")){
                        context.startActivity(new Intent(context, Niwee.class));
                    }
                    else if(favTextView.getText().toString().contains("Мы мыслим как бедняки") || favTextView.getText().toString().contains("Do we think like poor")){
                        context.startActivity(new Intent(context, KakBednyaki.class));
                    }
                    else if(favTextView.getText().toString().contains("Психология привлечения денег") ||  favTextView.getText().toString().contains("Psychology of attracting")){
                        context.startActivity(new Intent(context, PsihologyPrivl.class));
                    }

                    else if(favTextView.getText().toString().contains("Аспекты денег") || favTextView.getText().toString().contains("Aspects of money") ){
                        context.startActivity(new Intent(context, AspektiDeneg.class));
                    }
                    else if(favTextView.getText().toString().contains("Психология денег мужчины и женщины") || favTextView.getText().toString().contains("Psychology of money men") ){
                        context.startActivity(new Intent(context, MujAndWom.class));
                    }
                    else if(favTextView.getText().toString().contains("У вас отрицание денег") || favTextView.getText().toString().contains("Do you have money denial") ){
                        context.startActivity(new Intent(context, OtricanieDeneg.class));
                    }
 else if(favTextView.getText().toString().contains("Полезные убеждения о деньгах") || favTextView.getText().toString().contains("Useful Beliefs about Money") ){
                        context.startActivity(new Intent(context, Ubejdeniya.class));
                    }
else if(favTextView.getText().toString().contains("Как легко привлечь деньги") || favTextView.getText().toString().contains("How easy is it to raise money") ){
                        context.startActivity(new Intent(context, LegkoPrivlech.class));
                    }
else if(favTextView.getText().toString().contains("Секреты успешных людей") || favTextView.getText().toString().contains("Secrets of successful") ){
                        context.startActivity(new Intent(context, Secreti.class));
                    }



                }
            });








        }







    }

    private void removeItem(int position) {
        favItemList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, favItemList.size());
    }




}
