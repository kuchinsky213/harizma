package com.ggs.psychologyofmoney.Heart;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.ggs.psychologyofmoney.R;
import com.ggs.psychologyofmoney.RecyclerItemClickListener;
import com.ggs.psychologyofmoney.Text.AspektiDeneg;
import com.ggs.psychologyofmoney.Text.KakBednyaki;
import com.ggs.psychologyofmoney.Text.LegkoPrivlech;
import com.ggs.psychologyofmoney.Text.MujAndWom;
import com.ggs.psychologyofmoney.Text.Niwee;
import com.ggs.psychologyofmoney.Text.OtricanieDeneg;
import com.ggs.psychologyofmoney.Text.PsihologyPrivl;
import com.ggs.psychologyofmoney.Text.Secreti;
import com.ggs.psychologyofmoney.Text.Ubejdeniya;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.util.ArrayList;

public class CofAdapter extends RecyclerView.Adapter<CofAdapter.ViewHolder>  {

    private ArrayList<CoffeeItem> coffeeItems;
    private Context context;
    private FavDB favDB;

    public CofAdapter(ArrayList<CoffeeItem> coffeeItems, Context context) {
        this.coffeeItems = coffeeItems;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        favDB = new FavDB(context);
        //create table on first
        SharedPreferences prefs = context.getSharedPreferences("prefs", Context.MODE_PRIVATE);
        boolean firstStart = prefs.getBoolean("firstStart", true);
        if (firstStart) {
            createTableOnFirstStart();
        }

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_item,
                parent, false);


        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull CofAdapter.ViewHolder holder, int position) {
        final CoffeeItem coffeeItem = coffeeItems.get(position);

        readCursorData(coffeeItem, holder);
        holder.imageView.setImageResource(coffeeItem.getImageResourse());
        holder.titleTextView.setText(coffeeItem.getTitle());
    }


    @Override
    public int getItemCount() {
        return coffeeItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView titleTextView, likeCountTextView;
        Button favBtn;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageView);
            titleTextView = itemView.findViewById(R.id.titleTextView);
            favBtn = itemView.findViewById(R.id.favBtn);
            likeCountTextView = itemView.findViewById(R.id.likeCountTextView);

            //add to fav btn
            favBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    CoffeeItem coffeeItem = coffeeItems.get(position);

                    if (coffeeItem.getFavStatus().equals("0")) {
                        coffeeItem.setFavStatus("1");
                        favDB.insertIntoTheDatabase(coffeeItem.getTitle(), coffeeItem.getImageResourse(),
                                coffeeItem.getKey_id(), coffeeItem.getFavStatus());
                        favBtn.setBackgroundResource(R.drawable.ic_favorite_red_24dp);

                    } else {
                        coffeeItem.setFavStatus("0");
                        favDB.remove_fav(coffeeItem.getKey_id());
                        favBtn.setBackgroundResource(R.drawable.ic_favorite_shadow_24dp);

                    }
                }
            });







        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                int position = getAdapterPosition();

                if (position==0){
                    context.startActivity(new Intent(context, Niwee.class));
                }
                else if (position==1){
                    context.startActivity(new Intent(context, KakBednyaki.class));
                }
                else if (position==2){
                    context.startActivity(new Intent(context, PsihologyPrivl.class));
                }
                else if (position==3){
                    context.startActivity(new Intent(context, AspektiDeneg.class));
                }
                else if (position==4){
                    context.startActivity(new Intent(context, MujAndWom.class));
                }

      else if (position==5){
                    context.startActivity(new Intent(context, OtricanieDeneg.class));
                }
                else if (position==6){
                    context.startActivity(new Intent(context, Ubejdeniya.class));
                }

else if (position==7){
                    context.startActivity(new Intent(context, LegkoPrivlech.class));
                }
else if (position==8){
                    context.startActivity(new Intent(context, Secreti.class));
                }



              //  Toast.makeText(v.getContext(), "Позиция = " +position, Toast.LENGTH_SHORT).show();


            }
        });



















        }
    }

    private void createTableOnFirstStart() {
        favDB.insertEmpty();

        SharedPreferences prefs = context.getSharedPreferences("prefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("firstStart", false);
        editor.apply();
    }

    private void readCursorData(CoffeeItem coffeeItem, ViewHolder viewHolder) {
        Cursor cursor = favDB.read_all_data(coffeeItem.getKey_id());
        SQLiteDatabase db = favDB.getReadableDatabase();
        try {
            while (cursor.moveToNext()) {
                String item_fav_status = cursor.getString(cursor.getColumnIndex(FavDB.FAVORITE_STATUS));
                coffeeItem.setFavStatus(item_fav_status);

                //check fav status
                if (item_fav_status != null && item_fav_status.equals("1")) {
                    viewHolder.favBtn.setBackgroundResource(R.drawable.ic_favorite_red_24dp);
                } else if (item_fav_status != null && item_fav_status.equals("0")) {
                    viewHolder.favBtn.setBackgroundResource(R.drawable.ic_favorite_shadow_24dp);
                }
            }
        } finally {
            if (cursor != null && cursor.isClosed())
                cursor.close();
            db.close();
        }
    }
}
