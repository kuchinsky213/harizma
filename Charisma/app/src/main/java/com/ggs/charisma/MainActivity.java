package com.ggs.charisma;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.ggs.charisma.Act.Five;
import com.ggs.charisma.Act.Four;
import com.ggs.charisma.Act.One;
import com.ggs.charisma.Act.Seven;
import com.ggs.charisma.Act.Six;
import com.ggs.charisma.Act.Three;
import com.ggs.charisma.Act.Two;

import soup.neumorphism.NeumorphCardView;

public class MainActivity extends AppCompatActivity {
    NeumorphCardView a1, a2, a3, a4, a5, a6, a7;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        a1 = findViewById(R.id.gosUsl);
        a2 = findViewById(R.id.kapRem);
        a3 = findViewById(R.id.dosP);
        a4 = findViewById(R.id.ispKontrakta);
        a5 = findViewById(R.id.potrebit);
        a6 = findViewById(R.id.alimenti);
        a7 = findViewById(R.id.kek);


        a1.setOnClickListener(v -> {
            startActivity(new Intent(MainActivity.this, One.class));
        });
        a2.setOnClickListener(v -> {
            startActivity(new Intent(MainActivity.this, Two.class));
        });
        a3.setOnClickListener(v -> {
            startActivity(new Intent(MainActivity.this, Three.class));
        });
        a4.setOnClickListener(v -> {
            startActivity(new Intent(MainActivity.this, Four.class));
        });
        a5.setOnClickListener(v -> {
            startActivity(new Intent(MainActivity.this, Five.class));
        });
        a6.setOnClickListener(v -> {
            startActivity(new Intent(MainActivity.this, Six.class));
        });
        a7.setOnClickListener(v -> {
            startActivity(new Intent(MainActivity.this, Seven.class));
        });




    }
}