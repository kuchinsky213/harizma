package com.ggs.charisma.Act;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.codesgood.views.JustifiedTextView;
import com.ggs.charisma.R;

import soup.neumorphism.NeumorphButton;

public class Seven extends AppCompatActivity {
    JustifiedTextView c1, c2, c3,c4,c5,c6,c7,c8,c9,c10,
    c11,c12,c13,c14,c15,c16,c17,c18,c19,c20,c21,c22;
    NeumorphButton back;
    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seven);









        back=findViewById(R.id.main7);
        back.setOnClickListener(v -> {
            onBackPressed();
            finish();
        });
        c1 = findViewById(R.id.c1);
        c2 = findViewById(R.id.c2);
        c3 = findViewById(R.id.c3);
        c4 = findViewById(R.id.c4);
        c5 = findViewById(R.id.c5);
        c6 = findViewById(R.id.c6);
        c7 = findViewById(R.id.c7);
        c8 = findViewById(R.id.c8);
        c9 = findViewById(R.id.c9);
        c10 = findViewById(R.id.c10);
        c11 = findViewById(R.id.c11);
        c12 = findViewById(R.id.c12);
        c13 = findViewById(R.id.c13);
        c14 = findViewById(R.id.c14);
        c15 = findViewById(R.id.c15);
        c16 = findViewById(R.id.c16);
        c17 = findViewById(R.id.c17);
        c18 = findViewById(R.id.c18);
        c19 = findViewById(R.id.c19);
        c20 = findViewById(R.id.c20);
        c21 = findViewById(R.id.c21);
        c22 = findViewById(R.id.c22);

        c1.setOnClickListener(v -> {
            CopyCB(c1);
        });

       c2.setOnClickListener(v -> {
            CopyCB(c2);
        });

       c3.setOnClickListener(v -> {
            CopyCB(c3);
        });

       c4.setOnClickListener(v -> {
            CopyCB(c4);
        });

  c5.setOnClickListener(v -> {
            CopyCB(c5);
        });

  c6.setOnClickListener(v -> {
            CopyCB(c6);
        });

  c7.setOnClickListener(v -> {
            CopyCB(c7);
        });

  c8.setOnClickListener(v -> {
            CopyCB(c8);
        });

  c9.setOnClickListener(v -> {
            CopyCB(c9);
        });

  c10.setOnClickListener(v -> {
            CopyCB(c10);
        });

  c11.setOnClickListener(v -> {
            CopyCB(c11);
        });

  c12.setOnClickListener(v -> {
            CopyCB(c12);
        });

  c13.setOnClickListener(v -> {
            CopyCB(c13);
        });

  c14.setOnClickListener(v -> {
            CopyCB(c14);
        });

  c15.setOnClickListener(v -> {
            CopyCB(c15);
        });

  c16.setOnClickListener(v -> {
            CopyCB(c16);
        });

  c17.setOnClickListener(v -> {
            CopyCB(c17);
        });

  c18.setOnClickListener(v -> {
            CopyCB(c18);
        });

  c19.setOnClickListener(v -> {
            CopyCB(c19);
        });

  c20.setOnClickListener(v -> {
            CopyCB(c20);
        });

  c21.setOnClickListener(v -> {
            CopyCB(c21);
        });

  c22.setOnClickListener(v -> {
            CopyCB(c22);
        });










    }

























 public void   CopyCB(TextView tv){

     ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
     ClipData clip = ClipData.newPlainText("", tv.getText().toString());
     clipboard.setPrimaryClip(clip);
     Toast.makeText(this, (R.string.copy), Toast.LENGTH_SHORT).show();




    }
}