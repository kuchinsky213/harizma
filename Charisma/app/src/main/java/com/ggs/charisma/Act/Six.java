package com.ggs.charisma.Act;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.ggs.charisma.R;

import soup.neumorphism.NeumorphButton;

public class Six extends AppCompatActivity {
    NeumorphButton back;
    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_six);
        back=findViewById(R.id.main6);
        back.setOnClickListener(v -> {
            onBackPressed();
            finish();
        });
    }
}