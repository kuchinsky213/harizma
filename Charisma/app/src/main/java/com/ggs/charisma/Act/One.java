package com.ggs.charisma.Act;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.ggs.charisma.R;

import soup.neumorphism.NeumorphButton;

public class One extends AppCompatActivity {
NeumorphButton back;

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one);

        back=findViewById(R.id.main1);
        back.setOnClickListener(v -> {
            onBackPressed();
            finish();
        });
    }
}