package com.ggs.rass4et;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;

public class FragmentOne extends Fragment {
    EditText v, k , b, h;
    float vc, kc , bc, hc;
    SendMessage SM, SM2;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.fragment_one, container, false);
        v = (EditText) rootView.findViewById(R.id.v);
        k = (EditText) rootView.findViewById(R.id.k);
        b = (EditText) rootView.findViewById(R.id.b);
        h = (EditText) rootView.findViewById(R.id.h);
        return rootView;


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Button btnPassData = (Button) view.findViewById(R.id.btnPassData);
       TextView inData = (TextView) view.findViewById(R.id.inMessage);


        v.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        k.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        b.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        h.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);

        btnPassData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View ve) {
                vc = Float.valueOf(v.getText().toString());
                kc = Float.valueOf(k.getText().toString());
                bc = Float.valueOf(b.getText().toString());
                hc = Float.valueOf(h.getText().toString());
                float pk = 60*vc*kc*bc*hc;

                String result = String.format("%.2f",pk);
                inData.setText(result);
                   SM.sendData(inData.getText().toString().trim());


                System.out.print(result);//  34,767

            }
        });

    }

    interface SendMessage {
        void sendData(String message);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            SM = (SendMessage) getActivity();


        } catch (ClassCastException e) {
            throw new ClassCastException("Error in retrieving data. Please try again");
        }
    }
}