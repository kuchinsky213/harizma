package com.ggs.rass4et;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class FragmentTwo extends Fragment {

    TextView txtData;
    EditText s, h;
    float sc;
   SendMessage1 SM, SM2;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.fragment_two, container, false);
        return rootView;



    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        txtData = (TextView)view.findViewById(R.id.txtData);
        s = (EditText) view.findViewById(R.id.s);
        TextView inData = (TextView) view.findViewById(R.id.itog);

        Button kek = (Button) view.findViewById(R.id.kek);
        kek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View ve) {
                sc = Float.valueOf(s.getText().toString());


         float tp = 2.5f;

                String result = String.format("%.2f",tp);
                inData.setText(result);
                SM.sendData2(inData.getText().toString().trim());

                System.out.print(result);//  34,767

            }
        });
    }

    protected void displayReceivedData(String message)
    {
        txtData.setText(message);
    }
    interface SendMessage1 {
        void sendData2(String message);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            SM = (FragmentTwo.SendMessage1) getActivity();

        } catch (ClassCastException e) {
            throw new ClassCastException("Error in retrieving data. Please try again");
        }
    }
}