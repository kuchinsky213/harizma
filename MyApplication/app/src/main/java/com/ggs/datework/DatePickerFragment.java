package com.ggs.datework;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;

import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.google.android.gms.ads.*;

import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {
    static final int START_DATE = 1;
    static final int END_DATE = 2;
    static final int START_DATE_WORK_DAYS = 3;
    static final int END_DATE_WORK_DAYS = 4;
    private InterstitialAd mInterstitialAd;
    private int mChosenDate;

    int cur = 0;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

        if (cur == START_DATE) {
            DateInterval dateIntervalActivity = (DateInterval) getActivity();
            dateIntervalActivity.processFromDatePickerResult(year, month, dayOfMonth);
        } else if (cur == END_DATE) {
            DateInterval dateIntervalActivity = (DateInterval) getActivity();
            dateIntervalActivity.processToDatePickerResult(year, month, dayOfMonth);
        } else if (cur == START_DATE_WORK_DAYS) {
            alupDays alupDaysActivity = (alupDays) getActivity();
            alupDaysActivity.processFromDatePickerResult(year, month, dayOfMonth);
        } else {
            alupDays alupDaysActivity = (alupDays) getActivity();
            alupDaysActivity.processToDatePickerResult(year, month, dayOfMonth);
        }

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker.
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            mChosenDate = bundle.getInt("DATE", 1);
        }


        switch (mChosenDate) {

            case START_DATE:
                cur = START_DATE;
                // Create a new instance of DatePickerDialog and return it.
                return new DatePickerDialog(getActivity(), this, year, month, day);

            case END_DATE:
                cur = END_DATE;
                // Create a new instance of DatePickerDialog and return it.
                return new DatePickerDialog(getActivity(), this, year, month, day);

            case START_DATE_WORK_DAYS:
                cur = START_DATE_WORK_DAYS;
                return new DatePickerDialog(getActivity(), this, year, month, day);


            case END_DATE_WORK_DAYS:
                cur = END_DATE_WORK_DAYS;
                return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        return null;
    }
}
