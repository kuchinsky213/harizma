package com.ggs.datework;

import android.content.Context;
import android.os.Build;

import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import com.ggs.datework.DateIntervalAdapter;
import com.ggs.datework.DatePickerFragment;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

import com.ggs.datework.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

@RequiresApi(api = Build.VERSION_CODES.O)

public class DateInterval extends AppCompatActivity {

    private EditText fromEditText, toEditText;
    private TextView resultDate;
    private String fromDateString, toDateString;
    private InterstitialAd interstitialAd;
    AdView mAdView;
    ArrayList<DataModel> dataModels;
    ListView listView;
    private DateIntervalAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_interval);
        mAdView = new AdView(this);
        mAdView.setAdSize(AdSize.BANNER);
        MobileAds.initialize(DateInterval.this, "ca-app-pub-7802337876801234~4668697812");
        mAdView.setAdUnitId("ca-app-pub-7802337876801234/1315582645");

        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId("ca-app-pub-7802337876801234/9046926525");
        interstitialAd.loadAd(new AdRequest.Builder().build());
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);




        initialize();
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                interstitialAd.show();

            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
                interstitialAd.loadAd(new AdRequest.Builder().build());
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the interstitial ad is closed.

            }
        });
    }

    private void initialize() {
        fromEditText = (EditText) findViewById(R.id.from_date_editText);
        fromEditText.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.onTouchEvent(event);
                InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
                return true;
            }
        });

        toEditText = (EditText) findViewById(R.id.to_date_editText);
        toEditText.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.onTouchEvent(event);
                InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
                return true;
            }
        });

        resultDate = (TextView) findViewById(R.id.result_date_textView);

        listView = (ListView) findViewById(R.id.date_interval_list);

        dataModels = new ArrayList<>();


        adapter = new DateIntervalAdapter(dataModels, getApplicationContext());

        listView.setAdapter(adapter);
    }

    public void getFromDatePicker(View view) {
        DialogFragment newFragment = new DatePickerFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("DATE", 1);
        newFragment.setArguments(bundle);
        newFragment.show(getSupportFragmentManager(),
                getString(R.string.from_date_picker));
    }

    public void getToDatePicker(View view) {
        DialogFragment newFragment = new DatePickerFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("DATE", 2);
        newFragment.setArguments(bundle);
        newFragment.show(getSupportFragmentManager(),
                getString(R.string.to_date_picker));
    }

    public void processFromDatePickerResult(int year, int monthOfYear, int dayOfMonth) {
        int month = monthOfYear + 1;
        String month_string = "" + month;
        String day_string = "" + dayOfMonth;
        String year_string = Integer.toString(year);


        if (month < 10) {

            month_string = "0" + month;
        }
        if (dayOfMonth < 10) {

            day_string = "0" + dayOfMonth;
        }
        fromDateString = year_string + "-" + month_string + "-" + day_string;
        fromEditText.setText(fromDateString);
    }


    public void processToDatePickerResult(int year, int monthOfYear, int dayOfMonth) {
        int month = monthOfYear + 1;
        String month_string = "" + month;
        String day_string = "" + dayOfMonth;
        String year_string = Integer.toString(year);


        if (month < 10) {
            month_string = "0" + month;
        }
        if (dayOfMonth < 10) {
            day_string = "0" + dayOfMonth;
        }

        toDateString = year_string + "-" + month_string + "-" + day_string;
        toEditText.setText(toDateString);
    }

    private String differenceBetweenTowDates() {
        LocalDate fromDate = LocalDate.parse(fromDateString);
        LocalDate toDate = LocalDate.parse(toDateString);

        Period diffInDate = Period.between(fromDate, toDate);

        long diffInDays = ChronoUnit.DAYS.between(fromDate, toDate);
        long diffInMonths = ChronoUnit.MONTHS.between(fromDate, toDate);
        long diffInYears = ChronoUnit.YEARS.between(fromDate, toDate);
        long diffInWeeks = ChronoUnit.WEEKS.between(fromDate, toDate);
        long diffInHours = diffInDays * 24;
        long diffInMins = diffInHours * 60;
        long diffInSeconds = diffInMins * 60;

        if (dataModels.size() != 0) dataModels.clear();

        fillArrayListWithData(dataModels, diffInYears, diffInMonths, diffInWeeks, diffInDays, diffInHours, diffInMins, diffInSeconds);

        String resultDate
                = diffInDate.getYears() + getString(R.string.let)
                + diffInDate.getMonths() + getString(R.string.mesyac)
                + diffInDate.getDays() + getString(R.string.dney);
        return resultDate;
    }

    public void getDateInterval(View view) {

        resultDate.setText(differenceBetweenTowDates());
    }

    private ArrayList<DataModel> fillArrayListWithData(ArrayList<DataModel> array, long years, long month, long weeks, long days, long hours, long mins, long seconds) {
        array.add(new DataModel(getString(R.string.l), years));
        array.add(new DataModel(getString(R.string.m), month));
        array.add(new DataModel(getString(R.string.n), weeks));
        array.add(new DataModel(getString(R.string.d), days));
        array.add(new DataModel(getString(R.string.ch), hours));
        array.add(new DataModel(getString(R.string.min), mins));
        array.add(new DataModel(getString(R.string.s), seconds));

        return array;
    }
    private void showInterstitial() {
        // Show the ad if it's ready. Otherwise toast and restart the game.
        if (interstitialAd != null && interstitialAd.isLoaded()) {
            interstitialAd.show();
        } else {
            Toast.makeText(this, "Ad did not load", Toast.LENGTH_SHORT).show();

        }
    }
    //Model
    public static class DataModel {

        String section;
        long value;


        public DataModel(String name, long value) {
            this.section = name;
            this.value = value;
        }

        public String getName() {
            return section;
        }

        public long getType() {
            return value;
        }
    }

    @Override
    public void onBackPressed()
    {

        interstitialAd.loadAd(new AdRequest.Builder().build());

        super.onBackPressed();  // optional depending on your needs
    }
}
