package com.ggs.datework;

import android.content.Context;
import android.os.Build;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;


import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import org.w3c.dom.Text;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

import com.ggs.datework.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

@RequiresApi(api = Build.VERSION_CODES.O)
public class alupDays extends AppCompatActivity {
    private EditText fromEditText, toEditText;
    private TextView weekendDaysTextView, workdaysTextView;

    private String fromDateString, toDateString;
    private InterstitialAd mInterstitialAd;
    ArrayList<DataModelWork> dataModelWorks;
    ListView listView;
    private WorkDaysAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_days);
        MobileAds.initialize(alupDays.this,
                "ca-app-pub-7802337876801234~4668697812");
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(      "ca-app-pub-7802337876801234/9046926525");


        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                mInterstitialAd.show();

            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the interstitial ad is closed.

            }
        });

        initialize();
    }

    private void initialize() {
        weekendDaysTextView = (TextView) findViewById(R.id.weekend_days_value_textView);
        workdaysTextView = (TextView) findViewById(R.id.work_days_value_textView);
        fromEditText = (EditText) findViewById(R.id.from_date_work_editText);

        fromEditText.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.onTouchEvent(event);
                InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
                return true;
            }
        });

        toEditText = (EditText) findViewById(R.id.to_date_work_editText);

        toEditText.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.onTouchEvent(event);
                InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
                return true;
            }
        });

        listView = (ListView) findViewById(R.id.work_date_list);

        dataModelWorks = new ArrayList<>();

        adapter = new WorkDaysAdapter(dataModelWorks, getApplicationContext());

        listView.setAdapter(adapter);

    }

    public void getFromDatePicker(View view) {

        DialogFragment newFragment = new DatePickerFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("DATE", 3);
        newFragment.setArguments(bundle);
        newFragment.show(getSupportFragmentManager(),
                getString(R.string.from_date_picker));
    }

    public void getToDatePicker(View view) {

        DialogFragment newFragment = new DatePickerFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("DATE", 4);
        newFragment.setArguments(bundle);
        newFragment.show(getSupportFragmentManager(),
                getString(R.string.to_date_picker));
    }


    public void processFromDatePickerResult(int year, int monthOfYear, int dayOfMonth) {
        int month = monthOfYear + 1;
        String month_string = "" + month;
        String day_string = "" + dayOfMonth;
        String year_string = Integer.toString(year);

        if (month < 10) {

            month_string = "0" + month;
        }
        if (dayOfMonth < 10) {

            day_string = "0" + dayOfMonth;
        }
        fromDateString = year_string + "-" + month_string + "-" + day_string;
        fromEditText.setText(fromDateString);
    }


    public void processToDatePickerResult(int year, int monthOfYear, int dayOfMonth) {
        int month = monthOfYear + 1;
        String month_string = "" + month;
        String day_string = "" + dayOfMonth;
        String year_string = Integer.toString(year);

        if (month < 10) {
            month_string = "0" + month;
        }
        if (dayOfMonth < 10) {
            day_string = "0" + dayOfMonth;
        }

        toDateString = year_string + "-" + month_string + "-" + day_string;
        toEditText.setText(toDateString);
    }

    public int[] workDaysAndWeekendDays() {
        LocalDate fromDate = LocalDate.parse(fromDateString);
        LocalDate toDate = LocalDate.parse(toDateString);

        long totalPeriodInDaysUnits = ChronoUnit.DAYS.between(fromDate, toDate);

        int[] value = new int[2];
        int friTracker = 0,
                satTracker = 0,
                sunTracker = 0,
                monTracker = 0,
                tueTracker = 0,
                wedTracker = 0,
                thuTracker = 0;

        for (int index = 0; index <= totalPeriodInDaysUnits; index++) {
            switch (fromDate.plusDays(index).getDayOfWeek()) {
                case FRIDAY:
                    friTracker += 1;
                    break;
                case SATURDAY:
                    satTracker += 1;
                    break;
                case SUNDAY:
                    sunTracker += 1;
                    break;
                case MONDAY:
                    monTracker += 1;
                    break;
                case TUESDAY:
                    tueTracker += 1;
                    break;
                case WEDNESDAY:
                    wedTracker += 1;
                    break;
                case THURSDAY:
                    thuTracker += 1;
                    break;
            }
        }

        value[0] = friTracker;
        value[1] = (int) totalPeriodInDaysUnits;
        if (dataModelWorks.size() != 0) dataModelWorks.clear();

        fillArrayListWithData(dataModelWorks,
                sunTracker,
                monTracker,
                tueTracker,
                wedTracker,
                thuTracker,
                friTracker,
                satTracker);

        return value;
    }

    private ArrayList<DataModelWork> fillArrayListWithData(ArrayList<DataModelWork> array, int sunTracker,
                                                           int monTracker, int tueTracker, int wedTracker, int thuTracker, int friTracker,
                                                           int satTracker) {

        array.add(new DataModelWork(getString(R.string.p), monTracker));
        array.add(new DataModelWork(getString(R.string.sr), tueTracker));
        array.add(new DataModelWork(getString(R.string.sred), wedTracker));
        array.add(new DataModelWork(getString(R.string.chetv), thuTracker));
        array.add(new DataModelWork(getString(R.string.piatn), friTracker));
        array.add(new DataModelWork(getString(R.string.subbotka), satTracker));
        array.add(new DataModelWork(getString(R.string.v), sunTracker));
        dataModelWorks = array;
        adapter.notifyDataSetChanged();  //very Important Method
        return dataModelWorks;
    }

    public void getWeekendAndWorkDays(View view) {
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        weekendDaysTextView.setText("" + workDaysAndWeekendDays()[0]*2);
        workdaysTextView.setText("" + (workDaysAndWeekendDays()[1] + 1 - workDaysAndWeekendDays()[0]*2));
    }

    //Model
    public static class DataModelWork {

        String weekName;
        long weekValue;


        public DataModelWork(String name, long value) {
            this.weekName = name;
            this.weekValue = value;
        }

        public String getName() {
            return weekName;
        }

        public long getType() {
            return weekValue;
        }
    }
}
