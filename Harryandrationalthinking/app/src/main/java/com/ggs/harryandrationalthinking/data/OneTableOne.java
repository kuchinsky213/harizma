package com.ggs.harryandrationalthinking.data;

public class OneTableOne {
    public final String [] onescenarioone_ru = {


    "Так выглядит гостиная дома, в котором живут известный профессор Майкл Веррес-Эванс и его жена, миссис Петуния Эванс-Веррес, а также их приёмный сын, Гарри Джеймс Поттер-Эванс-Веррес. ",
     "На столе в гостиной лежит письмо, на конверте изумрудно-зелёными чернилами написано, что письмо адресовано «мистеру Г. Поттеру».",
      "Профессор спорит с женой, не повышая голос, так как считает, что кричать — некультурно.",

            "Майкл: Это ведь шутка, да?",
            "Петуния: Моя сестра была ведьмой. — А её муж — волшебником. ",
            "Майкл: Это абсурд.",
            "Петуния: Я просила их ничего тебе не рассказывать",
            "Майкл: Дорогая, я знаю, ты не читаешь скептическую литературу и можешь не понимать, как легко для искусного фокусника делать невозможные на первый взгляд вещи. Помнишь, я учил Гарри гнуть ложки? И если вдруг тебе казалось, что твоя сестра и её муж угадывали твои мысли, то такой приём называется «холодное чтение».",
            "Петуния: Это было не сгибание ложек.",
            "Петуния: Так просто не рассказать. Ты подумаешь, что я..",
            "Петуния: Послушай, Майкл, я не всегда была… такой, — она махнула рукой вниз, обозначая точёную фигуру. — Лили изменила мою внешность. Потому что я… я буквально умоляла её. Долгие годы я умоляла. Всё детство я плохо к ней относилась, потому что она всегда, всегда была красивее меня, а потом у неё проявился магический дар. Можешь представить, как я себя чувствовала? Я годами умоляла её сделать меня красивой. Пусть у меня не будет магии, но будет хотя бы красота. ",
            "Петуния: Лили отказывала мне по разным нелепым причинам, говорила, будто наступит конец света, если она немного поможет родной сестре, или что кентавр запретил ей это делать, и тому подобную чепуху, и я её за это ненавидела. А сразу после университета я встречалась с этим Верноном Дурслем, он был толстый, но кроме него никто из парней со мной вообще не разговаривал. Он говорил, что хочет детей и чтобы первенца звали Дадли. Я тогда подумала: «Какие же родители назовут своего ребёнка Дадли Дурсль?» И тут вся моя будущая жизнь словно встала у меня перед глазами, и это было невыносимо. Я написала сестре, что, если она мне не поможет, то я…",
            "Петуния:  В конце концов она сдалась. Она говорила, что это опасно, но мне было наплевать. Я выпила зелье и тяжело болела две недели. Зато потом моя кожа стала чистой, фигура похорошела и… Я стала красивой, люди начали относиться ко мне добрее, — её голос сорвался. — После этого я больше не могла ненавидеть сестру, особенно когда узнала, к чему в итоге привела её эта магия. ",
            "Майкл: Дорогая, ты заболела, набрала правильный вес, пока лежала в кровати, а кожа стала лучше сама по себе. Или болезнь заставила тебя изменить рацион.",
            "Петуния: Она была ведьмой. Я видела, как она творила чудеса.",

            //----вариант 1


"Папа! Мама!",
"Они замолчали и оглянулись на Гарри, который, оказывается, тоже был в гостиной всё это время. ",


            //----вариант 2

            "Майкл: Петуния,  ты же знаешь, что это не может быть правдой. Мне точно нужно объяснять почему?",
            "Петуния: Милый, я всегда проигрываю тебе в споре, но, пожалуйста, поверь мне сейчас…",


//---------------------после выбора
            "Гарри сделал глубокий вдох. ",
       "Гарри: Мама, насколько я понимаю, у твоих родителей не было магических способностей?",
            "Петуния: Нет",
       "Гарри: Получается, что никто из членов вашей семьи не знал о магии, пока Лили не получила пригласительное письмо. Каким образом убедили их?" ,
            "Петуния: Тогда было не только письмо. К нам приходил профессор из Хогвартса. Он…  он показал нам несколько заклинаний.",
        "Гарри: Значит, спорить по этому поводу совершенно ни к чему, Если всё это правда, то мы можем просто пригласить профессора из Хогвартса. Если он продемонстрирует нам магию, то папе придётся признать, что она существует. А если нет, то мама согласится, что всё это выдумка. Нужно не ссориться, а провести эксперимент.",
            "Майкл: Гарри? Магия? В самом деле? Я думал, уж ты-то знаешь достаточно, чтобы не воспринимать её всерьёз, хоть тебе и десять лет. Сынок, магия — самая ненаучная вещь, которую только можно себе представить!",

            "Гарри кисло улыбнулся.",

            " Майкл относился к нему хорошо — вероятно, лучше, чем большинство родных отцов относятся к своим детям." +
                    " Гарри отправляли учиться в лучшие школы, а когда с ними ничего не вышло, ему стали нанимать частных преподавателей из бесконечной вереницы голодающих студентов. " +
                    "Родители всегда поддерживали Гарри в изучении всего, что только привлекало его внимание. " +
                    "Ему покупали все интересующие его книги, помогали с участием в различных конкурсах по математике и естественнонаучным предметам. " +
                    "Он получал практически всё, что хотел, в разумных пределах. Единственное, в чём ему отказывали, так это в малейшей доле уважения. " +
                    "Впрочем, с какой стати штатному профессору Оксфорда, преподающему биохимию, прислушиваться к советам маленького мальчика? ",




            //**** 2 варианта
            //---------первый

            "Мам,  если ты хочешь выиграть у папы этот спор, посмотри вторую главу из первого тома лекций Фейнмана по физике. Там есть цитата, в которой говорится, что философы тратят уйму слов, выясняя, без чего наука не может обойтись, и все они неправы, потому что в науке есть только одно правило: последний судья — это наблюдение. Нужно просто посмотреть на мир и рассказать о том, что ты видишь. И… я не могу вспомнить с ходу подходящую цитату, но с научной точки зрения решать разногласия нужно опытным путём, а не спорами.",
            "Петуния: Спасибо, Гарри, но… я не хочу выигрывать спор у твоего отца. Я лишь хочу, чтобы мой муж прислушался к любящей его жене и поверил ей.",


            //----------Второй

            "Петуния: Майкл, Я лишь хочу, чтобы ты прислушался и поверил мне.",
            " ",

            //--------после любого варианта

            "Гарри на секунду закрыл глаза. Безнадёжны. Его родители просто безнадёжны.",


            "Я пойду в свою комнату.  Мама, папа, пожалуйста, постарайтесь долго не ругаться. Мы ведь и так скоро всё узнаем. ",
            "Майкл: Конечно, Гарри",

            "Гарри поднялся по лестнице в свою спальню, закрыл за собой дверь и попытался всё обдумать. ",
            "Самое интересное, что он просто должен был согласиться с отцом. Никто и никогда не видел ни одного доказательства реальности магии, а из слов мамы следовало, что существует целый волшебный мир. Как можно такое сохранять в тайне? Тоже с помощью магии? Довольно сомнительное объяснение. ",
            "Случай по идее элементарный: мама либо шутит, либо лжёт, либо сошла с ума, в порядке возрастания ужасности. Если мама сама отправила письмо, то это объясняет, как оно попало в почтовый ящик без марки. В конце концов, небольшое сумасшествие гораздо, гораздо более вероятно, чем вселенная, содержащая в себе магию. ",

            "Скривившись, Гарри потёр лоб. «Не верь своим мыслям», — было написано в одной книге. ",

            "Откуда ты взялось, странное маленькое предчувствие? — Гарри крепко задумался.",
            "Почему я верю в то, во что я верю?",
            "Он взял со стола лист линованной бумаги и написал: «Заместителю директора».",
            "Гарри остановился, собираясь с мыслями, потом взял другой лист и выдавил ещё один миллиметр графита из механического карандаша — случай требовал каллиграфического почерка. ",
            "«Уважаемая заместитель директора Минерва МакГонагалл или другое уполномоченное лицо! \n" +
                    "Недавно я получил от Вас пригласительное письмо в Хогвартс на имя мистера Г. Поттера. Вы можете не знать, что мои биологические родители, Джеймс Поттер и Лили Поттер (в девичестве Лили Эванс), мертвы. Я был усыновлён сестрой Лили, Петунией Эванс-Веррес, и её мужем, Майклом Веррес-Эвансом. \n" +
                    "Я крайне заинтересован в посещении Хогвартса, при условии, что такое место на самом деле существует. Моя мать, Петуния, утверждает, что знает про магию, но сама ею пользоваться не способна. Мой отец настроен скептически. Сам я до конца не убеждён. К тому же я не знаю, где приобрести книги и материалы, указанные Вами в пригласительном письме. \n" +
                    "Мама упомянула, что Вы присылали представителя Хогвартса к Лили Поттер (бывшая Лили Эванс), чтобы продемонстрировать её семье существование магии и, я полагаю, чтобы помочь ей с приобретением школьных принадлежностей. Если Вы поступите подобным образом в отношении моей семьи, это существенно поможет делу. \n" +
                    "С уважением, \n" +
                    "Гарри Джеймс Поттер-Эванс-Веррес».",

            "Гарри дописал свой адрес, сложил письмо пополам и засунул в конверт, адресовав его в Хогвартс. После чего, поразмыслив, взял свечку и, капнув воском на угол конверта, выдавил на нём кончиком перочинного ножа свои инициалы: Г. Д. П. Э. В. Сходить с ума — так со вкусом. ",

            //************2 варианта

            //-----------первый вараинт


            "Мама, я хочу проверить гипотезу. Каким образом, согласно твоей теории, я должен послать сову в Хогвартс? ",
            "Мать отвернулась от кухонной раковины и озадаченно посмотрела на него.",
            "Петуния: Я… Я не знаю. Думаю, для этого у тебя должна быть волшебная сова. ",
            "Он должен был с подозрением в голосе сказать: «Ага, выходит, проверить твою теорию никак нельзя», но странная уверенность не желала сдаваться. ",
            "Письмо каким-то образом сюда попало, так что я просто помашу им на улице и крикну: «Письмо в Хогвартс!» Посмотрим, прилетит ли сова, чтобы забрать его. Папа, ты хочешь пойти со мной? ",
            "Отец отрицательно покачал головой и продолжил чтение. ",

            //-----------второй вариант


//----------одинаковое продолжение
            "Выходя через заднюю дверь во двор, Гарри вдруг осознал, что если сова в самом деле прилетит и заберёт письмо, то он не сможет доказать это отцу. ",
            "Но ведь этого же на самом деле произойти не может, так? Что бы там ни твердил мой мозг. А если сова действительно спустится с небес и схватит конверт, то у меня будут заботы поважнее мнения папы на этот счёт. ",
"Гарри глубоко вздохнул и поднял конверт над головой. ",
            "*** Сглотнул ***",
            "Кричать «Письмо в Хогвартс!», размахивая конвертом, на заднем дворе своего дома оказалось вдруг весьма нелепым занятием. ",
            "Нет, я не как папа. Я использую научный метод, даже если буду при этом глупо выглядеть. ",

            "— Письмо…  ",
            "— Письмо в Хогвартс! Можно мне сову?! ",
            "— Гарри?",
            "Произнёс почти над ухом озадаченный голос соседки",
            "Это была миссис Фигг, которая временами за ним приглядывала.",
            "Мисс Фигг: Что ты тут делаешь, Гарри?",
            "Ничего, просто… проверяю одну весьма глупую теорию…",
            "Мисс Фигг: Ты получил пригласительное письмо из Хогвартса?",

            "Гарри застыл на месте. ",

            "Да, я получил письмо из Хогвартса. Они написали, чтобы я отправил им ответ с совой до 31 июля, но…",

            "Мисс Фигг: Но у тебя нет совы. Бедный мальчик! Даже не представляю, о чём они думали, посылая тебе стандартное приглашение.",

            "Морщинистая рука с раскрытой ладонью высунулась из-за забора. С трудом понимая, что происходит, Гарри отдал конверт. ",
            "Мисс Фигг: Я всё сделаю, дорогой, мигом кого-нибудь приведу.",

            "И её лицо, торчавшее над забором, исчезло.",
            "Во дворе надолго воцарилась тишина, которую в конце концов нарушил тихий и спокойный голос мальчика.",
            "Чтооо?!"




//----------конец первой главы

    };



    public final String [] onescenarioone_eng = {


            "It looks like living at home, where you live, renowned Professor Michael Verres-Evans and his wife, Mrs. Petunia Evans-Verres, and their adopted son, Harry James Potter-Evans-Verres. ",
            "On the living room table lay a letter, on the envelope with emerald green ink it is written that the letter is addressed to Mr. H. Potter.",
            "The Professor argues with his wife, not raising his voice, as he considers that scream uncultured.",

            "Michael: this is a joke, right?",
            "Petunia: My sister was a witch. And her husband is a wizard. ",
            "Michael: this is absurd.",
            "Petunia: I asked them not to tell you anything.",
            "Michael: My dear, I know you don't read skeptical literature, and you may not understand how easy it is for a skilled magician to do seemingly impossible things. Remember when I taught Harry how to bend spoons? And if you suddenly thought that your sister and her husband guessed your thoughts, then this technique is called cold reading",
            "Petunia: it wasn't bending spoons.",
            "Petunia: It's not easy to tell. You'll think I'm...",
            "Petunia: Look, Michael, I wasn't always... like this, she waved her hand down to indicate a chiseled figure. Lily changed my appearance. Because I... I literally begged her. I've been begging for years. All my childhood I treated her badly, because she was always, always more beautiful than me, and then she showed a magical gift. Can you imagine how I felt? I've been begging her for years to make me beautiful. I may not have magic, but at least I will have beauty. ",
            "Petunia: Lily refused me for all sorts of ridiculous reasons, saying that the world would end if she helped her own sister a little, or that the centaur forbade her to do it, and all that nonsense, and I hated her for it. And right after University, I dated this Vernon Dursley guy, who was fat, but other than him, none of the guys ever talked to me at all. He said that he wanted children and that the first child's name was Dudley. I thought, what kind of parents would name their child Dudley Dursley?» And then my whole future life seemed to flash before my eyes, and it was unbearable. I wrote to my sister that if she didn't help me, I would…",
            "Petunia: In the end, she gave up. She said it was dangerous, but I didn't care. I drank the potion and was seriously ill for two weeks. But then my skin became clear, my figure became prettier and... I became beautiful, people began to treat me kinder,  her voice broke. After that, I couldn't hate my sister any more, especially when I found out what the magic had finally done to her. ",
            "Michael: Honey, you got sick, gained the right weight while you were in bed, and your skin got better on its own. Or the disease made you change your diet.",
            "Petunia: She was a witch. I've seen her work miracles.",
            //----вариант 1


            "Dad! Mom!",
            "They paused and looked back at Harry, who had also been in the living room the entire time. ",


            //----вариант 2

            "Michael: Petunia, you know that can't be true. Do I really need to explain why?",
            "Petunia: Honey, I always lose an argument with you, but please believe me now…",


//---------------------после выбора
            "Harry took a deep breath. ",
            "Harry: Mom, I understand your parents didn't have any magical abilities?",
            "Petunia: No",
            "Harry: it turns out that none of your family members knew about magic until Lily received the invitation letter. How did you convince them?" ,
            "Petunia: it wasn't just a letter then. A Professor from Hogwarts came to see us. He... he showed us some spells.",
            "Harry: So there's absolutely no need to argue about it, if all this is true, then we can just invite a Professor from Hogwarts. If he shows us magic, then dad will have to admit that it exists. And if not, my mother will agree that it's all fiction. We should not quarrel, but conduct an experiment.",
            "Michael: Harry? Magic? Really? I thought you knew enough not to take her seriously, even if you were ten years old. Son, magic is the most unscientific thing you can imagine!",
            "Гарри кисло улыбнулся.",

            "Michael treated him well — probably better than most birth fathers treat their children." +
                    "Harry was sent to the best schools, and when they didn't work out, they started hiring private teachers from an endless string of starving students. "+
                    "Harry's parents always supported him in learning everything that attracted his attention. "+
                    "They bought all the books that interested him, helped him participate in various competitions in mathematics and natural science subjects. " +
                    "He got almost everything he wanted, within reason. The only thing he was denied was the slightest bit of respect. "+
                    "However, why should a tenured Professor of biochemistry at Oxford listen to the advice of a little boy? ",


            //**** 2 варианта
            //---------первый

            "Mom, if you want to win this argument with dad, look at the second Chapter of the first volume of the Feynman lectures on physics. There's a quote that says that philosophers spend a lot of words figuring out what science can't do without, and they're all wrong, because there's only one rule in science: the last judge is observation. You just need to look at the world and talk about what you see. And... I can't remember the right quote right away, but from a scientific point of view, you need to solve differences by experience, not by arguments.",
            "Petunia: Thank you, Harry, but... I don't want to win an argument with your father. I just want my husband to listen to his loving wife and believe her.",

            //----------Второй

            "Petunia: Michael, I just want you to listen and believe me.",
            " ",

            //--------после любого варианта

            "Harry closed his eyes for a second. Hopeless. His parents are hopeless.",

            "I'll go to my room.  Mom, dad, please try not to swear for a long time. We'll find out soon enough. ",
            "Michael: Of Course, Harry",


            "Harry went up the stairs to his bedroom, closed the door behind him, and tried to think things through. ",
            "The most interesting thing is that he just had to agree with his father. No one had ever seen any proof of the reality of magic, and from what my mother had said, there was a whole magical world. How can this be kept secret? Also with the help of magic? A rather dubious explanation. ",
            "The case is basically elementary: mom is either joking, lying, or crazy, in order of increasing horror. If my mother sent the email herself, it explains how it got into the mailbox without a stamp. After all, a little madness is much, much more likely than a universe containing magic. ",


            "Grimacing, Harry rubbed his forehead. Don't believe your thoughts, was written in one book. ",

            "Where did you come from, strange little premonition?  Harry thought hard.",
            "Why do I believe what I believe?",
            "He took a sheet of lined paper from the table and wrote: To the Deputy Director",
            "Harry paused to collect his thoughts, then picked up another sheet of paper and squeezed another millimeter of graphite out of a mechanical pencil — the case required a calligraphic handwriting. ",
            "Dear Deputy Director Minerva McGonagall or other authorized person! \n" +
            "I recently received an invitation letter from you to Hogwarts addressed to Mr. H. Potter. You may not know that my biological parents, James Potter and Lily Potter (nee Lily Evans), are dead. I was adopted by Lily's sister, Petunia Evans-Verres, and her husband, Michael Verres-Evans. \n" +
            "I am extremely interested in visiting Hogwarts, provided that such a place actually exists. My mother, Petunia, claims to know about magic, but she can't use it herself. My father is skeptical. I'm not entirely convinced myself. In addition, I do not know where to buy the books and materials you indicated in the invitation letter. \n" +
            "Mother mentioned that you sent a Hogwarts representative to Lily Potter (formerly Lily Evans) to demonstrate the existence of magic to her family and, I believe, to help her purchase school supplies. If You do this to my family, it will greatly help the cause. \n" +
            "Sincerely, \n" +
            "Harry James Potter-Evans-Verres.",

            "Harry wrote down his address, folded the letter in half and put it in an envelope, addressing it to Hogwarts. Then, after some thought, he took a candle and, dripping wax on the corner of the envelope, stamped his initials on it with the tip of a penknife: G. D. P. E. V. go crazy — so tasteful. ",
            //************2 варианта

            //-----------первый вараинт


            "Mom, I want to test a hypothesis. How do you think I should send an owl to Hogwarts? ",
            "Mother turned away from the kitchen sink and looked at him with a puzzled expression.",
            "Petunia: I... I don't know. I think you should have a magic owl for that. ",
            "He should have said suspiciously: Yeah, so there's no way to test your theory,but the strange certainty didn't want to give up. ",
            "The letter got here somehow, so I'll just wave it in the street and shout,' letter to Hogwarts!» Let's see if the owl comes to pick him up. Dad, do you want to come with me? ",
            "My father shook his head and continued reading. ",
            //-----------второй вариант


//----------одинаковое продолжение
            "As Harry went out the back door into the yard, he realized that if the owl actually came and took the letter, he wouldn't be able to prove it to his father.",
            "But that can't really happen, can it? No matter what my brain says. And if an owl really comes down from the sky and grabs the envelope, I'll have more important things to worry about than dad's opinion. ",
            "Harry took a deep breath and held the envelope above his head. ",
            "*** Swallowed ***",
            "Shouting' letter to Hogwarts! ' while waving an envelope in the back yard of your house suddenly turned out to be a very ridiculous activity. ",
            "No, I'm not like dad. I use the scientific method, even if it makes me look stupid. ",


            "The letter... ",
            "— A letter to Hogwarts! Can I have an owl?! ",
            "— Harry?",
            "Said the puzzled voice of a neighbor almost in my ear",
            "It was Mrs. Figg who kept an eye on him from time to time.",
            "Miss Figg: what are you doing here, Harry?",
            "Nothing, just... testing a very stupid theory...",
            "Miss Figg: Did you get an invitation letter from Hogwarts?",


            "Harry froze in place. ",

            "Yes, I received a letter from Hogwarts. They told me to send them a reply with an owl by July 31, but…",

            "Miss Figg: but you don't have an owl. Poor boy! I don't know what they were thinking when they sent you the standard invitation.",

            "A wrinkled hand with an open palm poked out from behind the fence. Hardly understanding what was happening, Harry handed the envelope over. ",
            "Miss Figg: I'll do everything, dear, I'll bring someone in a jiffy.",

            "And her face, sticking out over the fence, disappeared.",
            "There was a long silence in the courtyard, which was finally broken by the quiet and calm voice of the boy.",
            "What?!"



//----------конец первой главы

    };








}
