package com.ggs.harryandrationalthinking.levels.level2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;

import com.ggs.harryandrationalthinking.R;

public class Preview extends AppCompatActivity {
    private final int SPLASH_DISPLAY_LENGHT = 35000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);
        Button bext = (Button)findViewById(R.id.bext);
        bext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(Preview.this, Level2.class);

                Preview.this.startActivity(mainIntent);

                Preview.this.finish();
            }
        });
        new Handler().postDelayed(new Runnable() {

            @Override

            public void run() {

                Intent mainIntent = new Intent(Preview.this, Level2.class);

                Preview.this.startActivity(mainIntent);

                Preview.this.finish();

            }

        }, SPLASH_DISPLAY_LENGHT);

    }



    @Override

    public void onBackPressed() {

        super.onBackPressed();

    }

}