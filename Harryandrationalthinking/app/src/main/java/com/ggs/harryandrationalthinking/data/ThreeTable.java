package com.ggs.harryandrationalthinking.data;

public class ThreeTable {
    public final String [] threeScenario_ru = {
            "— Господи боже! — воскликнул бармен, уставившись на Гарри. — Это же… неужели? ",
            "Гарри придвинулся к барной стойке: ",
            "— Я… неужели… возможно… точно не знаю… может, и нет… но тогда вопрос в том, кто? ",
            "— Господи благослови,  Гарри Поттер, какая честь! ",
            "Гарри моргнул",

          //  Что ответить?
//1 variant i prodoljaem
"— Вы крайне наблюдательны, большинство людей не понимают этого так быстро…",

  //  Промолчать -> — Мы торопимся.    Никто не рискнул их задерживать.


          "  — Достаточно, — сказала профессор МакГонагалл, её рука сжала плечо Гарри. — Том, не приставай к мальчику, он к этому не привык.",
           " — Но это он? — встряла пожилая женщина. — Это Гарри Поттер?",
   " Скрипнув стулом, она поднялась. ",
"— Дорис, — остановила её МакГонагалл и обвела зал взглядом, смысл которого был понятен каждому.",
"— Я только хотела пожать ему руку, — прошептала женщина.",
  "  Она нагнулась и протянула Гарри морщинистую ладонь.",
"— Мой внук был аврором, — прошептала она. — Погиб в семьдесят девятом. Спасибо тебе, Гарри Поттер. Хвала небесам, что ты есть.",
       "  — Пожалуйста, — автоматически ответил Гарри, бросив в сторону МакГонагалл испуганный, умоляющий взгляд.",
  " По залу пошло шевеление, люди начали подниматься со своих мест, и тут профессор громко топнула ногой.",
            "— Мы торопимся.", /// 2 вариант
  "  Никто не рискнул их задерживать. ",
"— Профессор? Кто был тот бледный человек в углу? Тот, с дёргающимся глазом?",
            "— М? Его зовут профессор Квиррелл. В этом году он будет преподавать в Хогвартсе защиту от Тёмных искусств.",
           " — У меня появилось странное ощущение, что мы с ним знакомы… — Гарри потёр лоб. — И что мне лучше не здороваться с ним за руку.",
          "  — А об остальном расскажете?",
           " — Мистер Поттер… вы знаете… что вам говорили о том… как погибли ваши родители?",
          "  — Мои родители живы и в добром здравии, но они всегда отказывались рассказывать мне о том, как погибли мои биологические родители. Из чего я сделал вывод, что их смерть была не самой простой. ",
"— Похвальная верность.  Лили и Джеймс были моими друзьями.",
   " Гарри вдруг стало стыдно, и он отвернулся.",
          "  — Простите, но у меня уже есть мама и папа. И я знаю, что почувствую себя несчастным, если буду сравнивать то, что существует в реальности с… с чем-то идеальным, созданным моим воображением.",
          "  — Удивительно мудро с вашей стороны, но ваши «биологические» родители погибли, защищая вас. ",
"— Что… Как именно это случилось? ",
    "МакГонагалл вздохнула. Её волшебная палочка коснулась лба мальчика, и у него на мгновение потемнело в глазах. ",
"— Это для маскировки,  чтобы сцена в трактире не повторилась до тех пор, пока вы не будете готовы.",
    "Затем она направила палочку в сторону кирпичной кладки и постучала по ней три раза…",
           " …Дыра в стене стремительно разрасталась, образуя большую арку; за ней открывался вид на длинные ряды магазинов с рекламными плакатами, на которых красовались котлы и драконья печень.",
    "Гарри даже не повёл бровью: после превращения в кошку это было сущим пустяком.",

   " И они двинулись вперёд, в мир волшебства.",

   " Вдруг Гарри заметил кое-что, заставившее его сильно отклониться от совместного с МакГонагалл курса и направиться прямиком в магазин из синего кирпича с орнаментом из бронзы на витринах.",

      //      Зайти внутрь

   // Пройти мимо

               " Это был книжный магазин с огромным количеством невиданных ранее книг.",
   " Очнулся он, лишь когда МакГонагалл встала на его пути.",
           " — Мистер Поттер?",
           " — Простите! На секунду я забыл, что иду с вами, а не со своей семьёй, У нас есть семейное правило: проходя мимо незнакомого книжного магазина, обязательно нужно зайти внутрь и осмотреться.",
           " — Самое когтевранское правило из тех, что мне приходилось слышать.",
            "— Что?",
           " — Не важно.",

    //proiti mimo
       " — Мистер Поттер, в первую очередь, нам необходимо посетить Гринготтс, банк волшебного мира. Там находится родовое хранилище вашей «биологической» семьи с наследством, которое ваши «биологические» родители вам завещали. Вам нужны деньги, чтобы купить школьные принадлежности, полагаю, некоторую сумму можно будет потратить и на книги. Впрочем, советую воздержаться: в Хогвартсе собрана большая библиотека книг о магии. Кроме того, в башне, в которой, как я подозреваю, вы будете жить, есть своя весьма обширная библиотека. Учитывая это, практически любая купленная сейчас книга окажется лишь бесполезным дубликатом.",
                   " — Не поймите меня неправильно, это прекрасная уловка, чтобы отвлечь моё внимание,  вероятно, лучшая из всех, что были использованы на мне, но не думайте, что я забыл о нашем разговоре.",
           " — Ваши родители, ваша мать уж точно, поступили весьма мудро, не рассказывая вам правды.",
            "— Вы хотите, чтобы я продолжал пребывать в блаженном неведении? Мне кажется, в вашем плане есть определённый изъян, профессор МакГонагалл.",
           " — Полагаю, это бессмысленно, учитывая, что каждый встречный может вам всё рассказать.",
    "И она поведала ему о Том-Кого-Нельзя-Называть, Тёмном Лорде, Волдеморте.",
           " — Волдеморт?",
    "Он решил, что лучше и безопаснее будет использовать фразы-заменители, вроде: Сам-Знаешь-Кто.",
    "Тёмный Лорд бешеным волком свирепствовал по всей магической Британии, разрывая и раздирая привычную канву жизни её обитателей. Другие страны, стиснув зубы, не вмешивались из-за равнодушного эгоизма, либо просто боялись, что первая из них, выступившая против Тёмного Лорда, станет следующей целью его террора.",
    "Эффект свидетеля, — подумал Гарри, вспоминая эксперимент Латана и Дарли, доказавших, что в случае эпилептического припадка вы скорее получите помощь, если рядом с вами будет один человек, нежели трое. — Рассеивание ответственности: каждый думает, что кто-то другой начнёт действовать первым.",

    "Вокруг Тёмного Лорда собралась армия Пожирателей Смерти — стервятников, кормящихся ранеными, и змей, жалящих слабейших. Они были не так сильны и не так безжалостны, как сам Тёмный Лорд, но их было много. Пожиратели полагались не на одну только магию: некоторые из них были весьма состоятельны, обладали политическим влиянием, владели искусством шантажа. Они делали всё возможное, чтобы парализовать любые попытки общества защитить себя.",
"  Старый уважаемый журналист, Йерми Виббл, призывавший к повышению налогов и введению воинской обязанности, заявил, что абсурдно всем бояться нескольких. Его кожа, только его кожа была найдена на следующее утро прибитой к стене в его кабинете рядом с кожей его жены и двух дочерей. Все хотели решительных действий, но мало кто осмеливался сопротивляться в открытую. Тех, кто выделялся из толпы, ожидала схожая судьба.",
   " Среди них оказались Джеймс и Лили Поттеры. По своей природе они были героями и, вероятно, умерли бы с волшебными палочками в руках, ни о чём не сожалея. Но у них был малютка-сын, Гарри Поттер, и ради его благополучия они вели себя осторожно.",
    "В глазах Гарри показались слёзы. Он в гневе, а может, от отчаяния, вытер их.",
   " Я совсем не знал этих людей, сейчас они не мои родители, бессмысленно так сильно грустить из-за них…",
            "— Так что же произошло? — голос Гарри дрожал.",
"— Тёмный Лорд пришёл в Годрикову Лощину, вас должны были спрятать, но вас предали. Тёмный Лорд убил Джеймса, затем Лили, а потом подошёл к вашей колыбели. Он бросил в вас Смертельное проклятие. На этом всё и кончилось. Это проклятие формируется из чистой ненависти и бьёт прямо в душу, отделяя её от тела. Его нельзя блокировать. Единственный способ защиты — уклониться. Но вы смогли выжить. Вы — единственный, кто когда-либо смог выжить. Смертельное проклятие отразилось и попало в Тёмного Лорда, оставив от него лишь обгоревшее тело и шрам на вашем лбу. Так закончилась эпоха террора — мы стали свободны. Вот почему, Гарри Поттер, люди хотят увидеть этот шрам и пожать вам руку.",
"    Приступ плача выжал из Гарри все слёзы.",
            "(Где-то в глубине его сознания возникло едва заметное ощущение, будто в этой истории что-то было не так. Обычно Гарри был способен замечать мельчайшие логические несоответствия, но в данный момент он был в смятении — таково печальное правило: вы чаще всего забываете о вашей способности мыслить здраво именно тогда, когда это больше всего необходимо.)",
"    Гарри отстранился от МакГонагалл.",
   " Да, вы можете продолжать называть их моими родителями, если хотите. Не обязательно добавлять «биологические». У меня могут быть две матери и два отца.",
 "   МакГонагалл промолчала.",
   " И так они шли, погружённые в свои мысли, пока впереди не показалось большое белое здание с широкими, обитыми бронзой дверями. ",
"— Гринготтс, — объявила МакГонагалл."



};


    public final String [] threeScenario_eng = {
            "— My God!  the bartender exclaimed, staring at Harry. That's... really? ",
            "Harry moved closer to the bar.",
            "I... can... it's possible... I don't know... maybe not... but then the question is, who? ",
            "God bless you, Harry Potter, what an honor! ",
            "Harry blinked",

            //  Что ответить?
//1 variant i prodoljaem
            "— You are extremely observant, most people do not understand it so quickly…",

            //  Промолчать -> — Мы торопимся.    Никто не рискнул их задерживать.


            "That's enough, said Professor McGonagall, her hand gripping Harry's shoulder. Tom, don't bother the boy, he's not used to it.",
            "But it's him?  an older woman chimed in. Is This Harry Potter?",
            "With a creak of her chair, she got up. ",
            "Doris, McGonagall said, and looked around the room with a look that everyone could understand.",
            "I just wanted to shake his hand, — the woman whispered.",
            "She bent down and held out a wrinkled hand to Harry.",
            "My grandson was an Auror, — she whispered. He died in' seventy — nine. Thank you, Harry Potter. Thank heaven you are.",
            "Please, said Harry automatically, giving McGonagall a startled, pleading look.",
            "There was a stir in the hall, people started to get up from their seats, and then the Professor stamped her foot loudly.",


            /// 2 вариант
            "— We're in a hurry.",
            "No one dared to detain them. ",
            "— Professor? Who was the pale man in the corner? The one with the twitching eye?",
            "— M? His name is Professor Quirrell. This year, he will be teaching defense against the Dark arts at Hogwarts.",
            "I have a strange feeling that we know each other... — Harry rubbed his forehead. And that I'd better not shake hands with him.",
            "— Can you tell us about the rest?",
            "Mr. Potter... do you know... what they told you about... how your parents died?",
            "My parents are alive and well, but they always refused to tell me how my biological parents died. From which I concluded that their deaths were not the easiest. ",
            "— Laudable loyalty.  Lily and James were my friends.",
            "Harry suddenly felt ashamed and turned away.",
            "I'm sorry, but I already have a mom and dad. And I know that I will feel miserable if I compare what exists in reality with... with something perfect created by my imagination.",
            "— Surprisingly wise of you, but your 'biological' parents died protecting you.",
            "— What… How exactly did this happen? ",
            "McGonagall sighed. Her magic wand touched the boy's forehead, and his eyes went dark for a moment. ",
            "— This is for disguise, so that the scene in the Inn will not be repeated until you are ready.",
            "Then she pointed her wand at the brickwork and tapped it three times…",
            "...The hole in the wall grew rapidly, forming a large arch; behind it, you could see long rows of shops with advertising posters that displayed cauldrons and dragon liver.",
            "Harry didn't even raise an eyebrow: after turning into a cat, it was nothing.",

            "And they moved forward into the world of magic.",

            "Suddenly, Harry noticed something that caused him to veer off course with McGonagall and head straight for the blue brick shop with bronze ornaments on the Windows.",
            //      Зайти внутрь

            // Пройти мимо

            "It was a bookstore with a huge number of previously unseen books.",
            "He didn't Wake up until McGonagall got in his way.",
            "Mr. Potter?",
            "— Sorry! For a second, I forgot that I was going with you and not my family, we have a family rule: when passing an unfamiliar bookstore, you must go inside and look around.",
            "That's the most Ravenclaw rule I've ever heard.",
            "— What?",
            "It doesn't matter.",

            //proiti mimo
            "Mr. Potter, first of all, we need to visit Gringotts, the Bank of the Wizarding world. There is the ancestral repository of your biological family with the inheritance that your biological parents bequeathed to you. You need money to buy school supplies, and I think some money can be spent on books. However, I advise you to refrain: Hogwarts has a large library of books about magic. In addition, the tower in which I suspect you will be living has its own very extensive library. Given this, almost any book purchased now will be just a useless duplicate.",
            "— Don't get me wrong, this is a great trick to distract my attention, probably the best one ever used on me, but don't think I forgot about our conversation.",
            "Your parents, your mother, were very wise not to tell you the truth.",
            "Do you want me to remain blissfully ignorant? I think there's a flaw in your plan, Professor McGonagall.",
            "I guess it's pointless, considering that everyone you meet can tell you everything.",
            "And she told him about He-who-must-Not-be-Named, the Dark Lord, Voldemort.",
            "— Voldemort?",
            "He decided that it would be better and safer to use substitute phrases like: You-Know-Who.",
            "The dark Lord raged like a mad wolf across magical Britain, tearing and tearing the familiar fabric of life of its inhabitants. Other countries, gritting their teeth, did not interfere out of indifferent selfishness, or simply feared that the first of them to oppose the Dark Lord would become the next target of his terror.",
            "The witness effect, Harry thought, remembering the experiment of Lathan and Darley, who had shown that in the event of an epileptic seizure, you would be more likely to get help if you had one person around than three. — Dispersal of responsibility: everyone thinks that someone else will act first.",



            "An army of Death eaters has gathered around the Dark Lord — vultures feeding on the wounded and snakes stinging the weakest. They weren't as strong or as ruthless as the Dark Lord himself, but there were many of them. The death eaters did not rely on magic alone: some of them were very wealthy, had political influence, and were skilled in blackmail. They did everything possible to paralyze any attempts by society to protect themselves.",
            "An old respected journalist, Yermi Vibble, who called for higher taxes and the introduction of military duty, said that it was absurd for everyone to be afraid of a few. His skin, only his skin, was found the next morning nailed to the wall in his office next to the skin of his wife and two daughters. Everyone wanted decisive action, but few dared to resist openly. Those who stood out from the crowd had a similar fate.",
            "Among them were James and Lily Potter. They were heroes by nature, and would probably have died with wands in their hands without regret. But they had a baby son, Harry Potter, and for his welfare they were careful.",
            "There were tears in Harry's eyes. He wiped them away in anger, or perhaps in desperation.",
            "I didn't know these people at all, they are not my parents now, it's pointless to be so sad about them...",
            "So what happened?  Harry's voice was trembling.",
            "— The dark Lord came to Godric's Hollow, you were supposed to be hidden, but you were betrayed. The dark Lord killed James, then Lily, and then came to your cradle. He threw a death Curse at you. That was the end of it. This curse is formed from pure hatred and hits directly into the soul, separating it from the body. You can't block it. The only way to protect yourself is to Dodge. But you were able to survive. You are the only one who has ever been able to survive. The deadly curse reverberated and hit the Dark Lord, leaving only a charred body and a scar on your forehead. So the era of terror ended — we became free. That's why, Harry Potter, people want to see that scar and shake your hand.",
            "A fit of crying squeezed all the tears out of Harry.",
            "(Somewhere in the back of his mind, there was a faint feeling that something was wrong with the story. Usually Harry was able to notice the smallest logical inconsistencies, but at the moment he was confused — this is a sad rule: you often forget about your ability to think straight when it is most necessary.)",
            "Harry pulled away from McGonagall.",
            "Yes, you can continue to call them my parents if you want. You don't have to add biological. I can have two mothers and two fathers.",
            "McGonagall didn't say anything.",
            "And so they went, lost in their own thoughts, until a large white building with wide bronze-studded doors appeared in front of them. ",
            "Gringotts, — McGonagall announced."



    };





}
