package com.ggs.harryandrationalthinking;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.appodeal.ads.Appodeal;
import com.explorestack.consent.Consent;
import com.explorestack.consent.ConsentManager;
import com.ggs.harryandrationalthinking.levels.Level5.Level5;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class FinalBeta extends AppCompatActivity {
    TextView tv;
    AdRequest ar;
    private static final String CONSENT = "consent";
    boolean consent;
    public static final String APP_KEY = "8a79a7084c6bd8c62024f867ee06eaac4d7682bd1bbb0e7b";
    private InterstitialAd mInterstitialAd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_beta);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-7802337876801234/7826926116");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        Consent consent = ConsentManager.getInstance(this).getConsent();
        Appodeal.initialize(this, APP_KEY, Appodeal.INTERSTITIAL|Appodeal.NON_SKIPPABLE_VIDEO, consent);


        Button end = (Button)findViewById(R.id.end);
        final TextView tv= (TextView)findViewById(R.id.textView) ;
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", tv.getText().toString());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(FinalBeta.this, R.string.copyText, Toast.LENGTH_SHORT).show();
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();}
            }
        });



        end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Appodeal.isLoaded(Appodeal.INTERSTITIAL)) {
                    Appodeal.show(FinalBeta.this, Appodeal.INTERSTITIAL);
                    Intent main = new Intent(FinalBeta.this, MainActivity.class);
                    startActivity(main);
                }
                else{
                    Intent main = new Intent(FinalBeta.this, MainActivity.class);
                    startActivity(main);
                    finish();

                }



            };
        });



}}