package com.ggs.harryandrationalthinking.data;

public class TwoTable
{
    public final String [] twoScenario_ru = {

            "— Давайте проясним ситуацию.",
            "— Папа, если профессор поднимет тебя в воздух, причём ты будешь знать, что нет никаких скрытых верёвок, то это будет считаться достаточным доказательством существования магии. Если подобная демонстрация уже сейчас кажется тебе недостаточной, то мы можем придумать другой эксперимент.",
            "Отец Гарри, профессор Майкл Веррес-Эванс, закатил глаза: ",
            "— Да, Гарри. ",
            "— Теперь ты, мама. Твоя теория заключается в том, что профессор сможет сделать это. Но если ничего не произойдёт, то ты признаешь, что ошибалась. ",

            //2i variant
            "Заместитель директора Минерва МакГонагалл с удивлением смотрела на Гарри. Одетая в чёрную мантию и остроконечную шляпу, она выглядела как настоящая ведьма, но разговаривала официальным тоном с шотландским акцентом, что совсем не вязалось с её внешним видом.",
            "— Так этого будет достаточно, мистер Поттер? Можно начинать демонстрацию? ",
            "— Достаточно? Скорее всего, нет. Начинайте, заместитель директора.",
            "— Можно просто «профессор».",

            //i 1i variant i prodoljaetsya vtoroy
            " — Вингардиум левиоса. ",
            "Гарри посмотрел на отца. ",
            "— Гм. ",
            "Тот посмотрел на него и повторил эхом:",
            "— Гм. ",
            "Затем его отец перевёл взгляд на профессора МакГонагалл: ",
            "— Ладно, можете опустить меня вниз. ",
            "Майкл Веррес-Эванс медленно приземлился на пол. ",
            "Гарри взъерошил волосы. Может, дело было в том, что какая-то его часть заранее знала результат, но… ",
            "— Почему-то меня это не впечатлило. Я думал, что моя реакция будет более драматичной, учитывая, что я стал свидетелем события бесконечно малой вероятности… ",
            "Он запнулся: мать, МакГонагалл и даже отец снова смотрели на него тем самым взглядом. ",
            "— Я имею в виду ситуацию, когда всё, во что веришь, оказывается ложью. ",
            "— Вы хотели бы продолжить демонстрацию, мистер Поттер? ",
            "— Это не обязательно, мы провели достаточно убедительный эксперимент. Но… ",
            "Он колебался. Хотелось увидеть больше. В конце концов, сейчас, учитывая обстоятельства, любопытство было правильным и уместным. ",
            "— Что ещё вы можете показать? ",
            "Профессор МакГонагалл превратилась в кошку. ",
            "Гарри отскочил назад так быстро, что споткнулся о стопку книг и звучно шмякнулся на пол.  ",
            "В тот же миг маленькая полосатая кошка вновь стала женщиной в чёрной мантии. ",
            "— Извините, мистер Поттер, Я должна была вас предупредить. ",
            "— Как вы ЭТО сделали?! ",
            "— Это просто трансфигурация, трансформация анимага, если говорить точно. ",
            "— Вы превратились в кошку! В МАЛЕНЬКУЮ кошку! Вы нарушили закон сохранения энергии! Это не какое-то условное правило. Энергия выражается с помощью квантового гамильтониана, а при нарушении закона сохранения теряется унитарность! Получается распространение сигналов быстрее скорости света! И кошки СЛОЖНЫЕ! Человеческий разум просто не в состоянии представить себе всю кошачью анатомию и всю кошачью биохимию, не говоря уже о неврологии. Как можно продолжать думать, используя мозг размером с кошачий?",
            "— Магия. ",
            "— Магии для такого недостаточно. Для этого нужно быть богом! ",
            "— Так меня ещё никто не называл. ",
            "Взгляд Гарри затуманился, его разум принялся подсчитывать причинённый ущерб. Вся идея единообразной вселенной с математически обоснованными законами, все представления физики пошли коту (точнее, кошке) под хвост. \n" +
                    "\n" +
                    "Три тысячи лет люди по маленьким кусочкам складывали картину мира, узнавали, что музыка планет имеет ту же мелодию, что и падающее яблоко, искали истинные универсальные законы … ",
            "А тут женщина превращается в кошку, только и всего. ",
            "Гарри хотел задать тысячу вопросов, но в итоге вырвался один: ",
            "— Что это за словосочетание — «Вингардиум левиоса»? Кто придумывает слова к этим заклинаниям, дети дошкольного возраста? ",
            "— Закончим на этом, мистер Поттер, Если вы хотите изучать магию, нам необходимо обговорить все детали вашего поступления в Хогвартс. ",
            "Сдавленный смешок вырвался изо рта ведьмы, будто его выдернули клещами. ",
            "— Подожди, Гарри.Ты же знаешь, по каким причинам ты до сих пор не посещаешь школу. Что будем делать с ними? ",
            "МакГонагалл повернулась к Майклу:",
            "— Какие причины? О чём вы говорите? ",


            //esli pervy variant
            "— У меня проблемы со сном. В моих биологических сутках двадцать шесть часов, я каждый день ложусь спать на два часа позже. Десять вечера, двенадцать, два часа, четыре утра и так по кругу. Даже если я заставлю себя встать раньше, это не поможет, и весь следующий день я буду не в своей тарелке. Поэтому я до сих пор не хожу в обычную школу. ",
            "— Это одна из причин, — уточнила его мать, награждённая за это свирепым взглядом Гарри. ",
            "— Хм-м, не сталкивалась с подобным прежде. Нужно будет спросить у мадам Помфри, знает ли она подходящее лекарство. ",
            "— Но не думаю, что это может быть препятствием. Я найду решение вашей проблемы со временем.",
            " — Каковы же другие причины? ",
            "Гарри наградил родителей ещё одним свирепым взглядом: ",
            "— Я сознательно возражаю против идеи обязательного посещения школы, основываясь на перманентной неспособности системы школьного образования предоставить мне учителей и учебные пособия минимально приемлемого уровня. ",


            //vtoroy variant i perviy ne ostanavlivaetsya
            "Гарри не контролирует свое тело и язык, когда учитель оказывается глупее его",
            "Родители Гарри рассмеялись, как будто вдруг услышали отличную шутку. ",
            "— Ага, — сказал отец Гарри, сверкнув глазами, — в третьем классе он укусил свою учительницу математики. ",
            "— Она не знала, что такое логарифм! ",
            "— И, конечно, укусить её — весьма взрослый способ решения проблемы, — вторила мать. ",
            "— Мне было семь лет! Как долго вы ещё собираетесь вспоминать тот случай? ",
            "— Да, всё понятно, — с участием в голосе сказала мать. — Ты укусил одного учителя математики, и теперь тебе этого никогда не забудут. ",
            "— Вот видите, с чем мне приходится иметь дело? ",
            "— Гм, значит так, никакого кусания учителей в Хогвартсе. Это понятно, мистер Поттер?",
            "Гарри, насупившись, посмотрел на неё.",
            "— Итак, стоит повременить с покупкой школьных принадлежностей. Займёмся этим за несколько дней до начала учебного года. ",
            "— Что? Почему? Ведь другие дети уже знакомы с магией! Я должен начать готовиться прямо сейчас! ",
            "— Смею вас заверить, мистер Поттер, в Хогвартсе вы сможете начать обучение с самых основ. К тому же, мистер Поттер, если я оставлю вас на два месяца с вашими учебниками даже без волшебной палочки, то, вернувшись сюда, я найду лишь кратер, полный лилового дыма, опустевший город и полчища огненных зебр, терроризирующих остатки Англии. ",
            "Мать и отец Гарри согласно кивнули. ",
            "— Мама! Папа! "
    };





    public final String [] twoScenario_eng = {

            "Let's get this straight.",
            "Dad, if the Professor lifts you up in the air and you know that there are no hidden ropes, it will be considered sufficient proof of the existence of magic. If such a demonstration doesn't seem enough to you right now, we can come up with another experiment.",
            "Harry's father, Professor Michael Verres-Evans, rolled his eyes.",
            "Yes, Harry. ",
            "Now you, mother. Your theory is that the Professor can do it. But if nothing happens, you'll admit that you were wrong. ",

            //2i variant
            "Deputy Director Minerva McGonagall looked at Harry in surprise. Dressed in a black robe and pointy hat, she looked like a real witch, but she spoke in a formal tone with a Scottish accent that didn't match her appearance at all.",
            "Will that be enough, Mr. Potter? Can I start the demo? ",
            "— Enough? Probably not. Get started, Deputy Director.",
            "— You can just say Professor",

            //i 1i variant i prodoljaetsya vtoroy
            "— Wingardium leviosa. ",
            "Harry looked at his father.",
            "— Hm. ",
            "The man looked at him and echoed:",
            "— Hm. ",
            "Then his father looked at Professor McGonagall.",
            "Okay, you can put me down. ",
            "Michael Verres-Evans landed slowly on the floor.",
           " Harry ruffled his hair. Maybe it was because some part of him knew the result beforehand, but… ",
        "For some reason, I wasn't impressed. I thought my reaction would be more dramatic, given that I witnessed an event of infinitesimal probability... ",
        "He paused: his mother, McGonagall, and even his father were looking at him in that same way again.",
        "— I mean a situation where everything you believe turns out to be a lie. ",
        "Would you like to continue the demonstration, Mr. Potter? ",
        "— This is not necessary, we conducted a fairly convincing experiment. But… ",
        "He hesitated. I wanted to see more. After all, now, given the circumstances, curiosity was right and appropriate. ",
        "— What else can you show? ",
        "Professor McGonagall turned into a cat. ",
        "Harry jumped back so fast that he tripped over a stack of books and landed on the floor with a resounding thud.  ",
        "At the same time, the little tabby cat became a black-robed woman again. ",
        "I'm sorry, Mr. Potter, I should have warned you. ",
        "How did you do it?! ",
        "It's just a Transfiguration, an Animagus transformation, to be precise. ",
        "— You turned into a cat! The little cat! You violated the law of conservation of energy! This is not a conditional rule. The energy is expressed using the quantum Hamiltonian, and if the conservation law is violated, unitarity is lost! It turns out that signals propagate faster than the speed of light! And cats ARE complicated! The human mind is simply not able to imagine all of the cat's anatomy and all the cat biochemistry, not to mention of neurology. How can you keep thinking using a cat-sized brain?",
        "— Magic. ",
        "Magic isn't enough for this. To do this, you need to be a God! ",
        "No one has ever called me that before. ",


            "Harry's eyes clouded as his mind began to calculate the damage. The whole idea of a uniform universe with mathematically sound laws, all the ideas of physics went down the drain (or rather, the cat). \n" +
            "\n" +
                    "For three thousand years, people put together a picture of the world in small pieces, learned that the music of the planets has the same melody as the falling Apple, and searched for true universal laws ... ",
            "And here a woman turns into a cat, that's all. ",
            "Harry wanted to ask a thousand questions, but in the end one came out: ",
            "— What is this phrase — Wingardium leviosa? Who comes up with words for these spells, preschool children? ",
            "Let's leave it at that, Mr. Potter, If you want to study magic, we need to discuss all the details of your admission to Hogwarts.",
            "A stifled laugh escaped the witch's mouth, as if it had been pulled out by a pair of tongs. ",
            "Wait, Harry.You know why you still don't attend school. What are we going to do with them? ",
            "McGonagall turned to Michael:",
        "— What are the reasons? What are you talking about? ",


            //esli pervy variant
            "I have trouble sleeping. There are twenty-six hours in my biological day, and I go to bed two hours later every day. Ten at night, twelve, two o'clock, four in the morning, and so on. Even if I force myself to get up earlier, it won't help, and I'll be out of sorts for the next day. That's why I still don't go to a regular school. ",
            "That's one of the reasons, said his mother, rewarded with a glare from Harry. ",
            "Hmmm, I haven't seen anything like this before. I'll have to ask Madam Pomfrey if she knows the right medicine. ",
            "But I don't think it can be an obstacle. I will find a solution to your problem in time.",
            "— What are the other reasons? ",
            "Harry gave his parents another glare.",
            "— I deliberately object to the idea of compulsory school attendance, based on the permanent inability of the school system to provide me with teachers and textbooks of the minimum acceptable level. ",

            //vtoroy variant i perviy ne ostanavlivaetsya
            "Harry doesn't control his body and tongue when the teacher is dumber than he is.",
            "Harry's parents laughed, as if they had suddenly heard a great joke. ",
            "Yeah, said Harry's father, his eyes flashing,  he bit his math teacher in third grade. ",
            "— She didn't know what a logarithm was! ",
            "And, of course, biting her is a very adult way to solve the problem, the mother echoed. ",
            "— I was seven years old! How long are you going to remember that incident? ",
            "Yes, everything is clear, the mother said with concern in her voice. You bit a math teacher, and now you'll never be forgotten.",
            "You see what I have to deal with? ",
            "— Um, okay, no biting of teachers at Hogwarts. Is that clear, Mr. Potter?",
            "Harry frowned at her.",
            "— So, it is worth to wait for the purchase of school supplies. We will do this a few days before the start of the school year. ",
            "What? Why? After all, other children are already familiar with magic! I have to start preparing right now! ",
            "I can assure you, Mr. Potter, that at Hogwarts you will be able to start your education from the very basics. Besides, Mr. Potter, if I leave you with your textbooks for two months without even a magic wand, all I'll find when I get back is a crater full of purple smoke, a deserted city, and hordes of fire zebras terrorizing the remnants of England. ",
            "Harry's mother and father nodded in agreement.",
            "— Mom! Dad! "
    };














}