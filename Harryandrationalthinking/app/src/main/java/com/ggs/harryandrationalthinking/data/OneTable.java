package com.ggs.harryandrationalthinking.data;

import com.ggs.harryandrationalthinking.R;

public class OneTable {
    public final String [] onescenario_ru = {
            "Этот квест содержит множество загадок и подсказок к ним: некоторые из них очевидны, некоторые — не очень. Есть и старательно спрятанные намёки.",
            "Все научные факты, упомянутые в тексте, во время написания считались настоящими научными фактами.",
            "Но с тех пор грянул кризис воспроизводимости и обесценил немало из того, что считалось доказанным в 2010 году, когда автор начинал писать эту историю. ",
            "Особенно это касается экспериментов в психологии, на которые часто опирается главный герой. ",

            "Отказ от прав: Гарри Поттер принадлежит Дж. К. Роулинг, методы рационального мышления не принадлежат никому. "};


    public final String [] onescenario_eng = {
            "This quest contains a lot of riddles and hints to them: some of them are obvious, some — not so much. There are also carefully hidden hints.",
            "All scientific facts mentioned in the text were considered real scientific facts at the time of writing.",
            "But since then, a reproducibility crisis has struck and devalued a lot of what was considered proven in 2010 when the author started writing this story. ",
            "This is especially true for experiments in psychology, which the main character often relies on. ",

            "Disclaimer: Harry Potter belongs to J. K. Rowling, methods of rational thinking do not belong to anyone. "};

}
