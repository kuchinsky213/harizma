package com.ggs.harryandrationalthinking.data;

public class FiveTable {
    public final String [] fiveScenario_ru = {

            "Потребовалось бы сверхъестественное вмешательство, чтобы у него, учитывая его окружение, были твои моральные принципы.",
"«Скрытная лавка» была маленьким причудливым (некоторые бы даже назвали его милым) магазинчиком, удобно устроившимся за овощным киоском, который, в свою очередь, был позади магазина волшебных перчаток, находившегося в двух шагах от ответвления Косого переулка. К большому разочарованию, хозяином лавки оказался не загадочный морщинистый старик, а нервного вида молодая женщина, одетая в выцветшую жёлтую мантию. И сейчас она держала в руках Супер-кошель-скрытень QX31, особенностями которого были увеличенное отверстие и чары незримого расширения, позволяющие класть в него большие вещи (общий объём был, тем не менее, ограничен).",
    "Гарри прямо-таки настаивал на посещении этой лавки сразу после банка, стараясь в то же время не вызвать у МакГонагалл подозрений. Дело в том, что ему нужно было как можно скорее положить кое-что в кошель. И это был не мешочек с галлеонами, которые МакГонагалл разрешила взять из Гринготтса. Это были другие галлеоны, которые Гарри исподтишка засунул в карман после того, как случайно упал на кучу золота. Честное слово, случайно. Гарри был не из тех, кто упускает возможности, но всё действительно произошло спонтанно. Теперь же ему приходилось нести мешочек с дозволенными галлеонами рядом с карманом брюк (это было крайне неудобно), чтобы звон не вызвал подозрений.",
    "Оставался лишь вопрос: как положить те, другие монеты, в кошель и не попасться? Галлеоны, может, и принадлежали ему, но всё равно были краденые. Самоукраденные? Автосворованные?",
    "Гарри оторвал взгляд от Супер кошеля-скрытня QX31, лежавшего на прилавке, и посмотрел вверх на продавщицу.",
            "— Можно я его испытаю? Чтобы убедиться, что он работает… надёжно, — он широко распахнул глаза, изображая наивного, шаловливого мальчика.",
    "Естественно, после десятого повтора операции «положить мешочек в кошель, засунуть руку в кошель, шепнуть «мешочек с золотом», вынуть мешочек», МакГонагалл отошла от Гарри и стала рассматривать другие товары, а хозяйка магазина повернулась в её сторону.",
    "Левой рукой Гарри положил мешочек с золотом в кошель. А правой вытащил из кармана несколько галлеонов, уронил их в кошель и (шепнув «мешочек с золотом») снова заполучил мешочек. Затем ещё раз опустил мешочек в кошель левой рукой, а правой опять полез в карман…",
    "МакГонагалл оглянулась на Гарри лишь раз, но он не вздрогнул и не застыл на месте, так что профессор ничего не заподозрила. Хотя ни в чём нельзя быть уверенным, если у взрослого есть чувство юмора. Операцию пришлось повторить три раза, чтобы украденное у самого себя золото — около тридцати галлеонов — переместилось в кошель-скрытень.",
            "Закончив, Гарри вытер со лба пот и выдохнул:",
            "— Я бы хотел его купить.",
            "Минус 15 галлеонов (за эти деньги можно купить две волшебные палочки) и плюс один Супер кошель-скрытень QX31. Когда Гарри и МакГонагалл вышли из магазина, дверная ручка превратилась в обычную руку и помахала им на прощание, вывернувшись так, что Гарри стало немного не по себе.",
    "А затем, к несчастью…",
            "— Вы… действительно Гарри Поттер? — прошептал пожилой человек. Крупная слеза скатилась по его щеке. — Это ведь правда, да? До меня доходили слухи, что на самом деле вы не пережили Смертельное проклятие, именно поэтому о вас ничего не слышно с тех самых пор.",
    "Похоже, маскирующее заклинание МакГонагалл работало менее эффективно против более опытных волшебников.",
    "Профессор положила руку на плечо мальчику и потянула его в ближайший переулок сразу же, как только услышала «Гарри Поттер?».",
    "Пожилой мужчина последовал за ними, но, по крайней мере, никто больше не услышал его фразу.",
    "Гарри задумался над вопросом. Действительно ли он — Гарри Поттер?",
            "— Я знаю только то, что говорили мне люди. Ведь я не помню, как родился, — он потёр лоб рукой. — У меня всегда был этот шрам. И мне всегда говорили, что моё имя — Гарри Поттер, но если у вас есть причины сомневаться в этом, то можно предположить и существование тайного заговора, участники которого нашли другого сироту-волшебника и вырастили его так, чтобы он верил, что он — Гарри Поттер.",
    "МакГонагалл раздражённо провела рукой по лицу:",
            "— Вы выглядите как ваш отец, когда он впервые прибыл в Хогвартс. Даже по одному вашему характеру можно с уверенностью сказать, что вы связаны родством с грозой Гриффиндора, Джеймсом Поттером.",
            "*— Она *тоже могла бы быть соучастником, — заметил Гарри.",
            "— Нет, — произнёс дрожащим голосом пожилой человек. — Она права. У вас глаза матери.",
            "— Хм, — Гарри нахмурился. — Полагаю, участником заговора могли бы быть и вы…",
            "— Достаточно, мистер Поттер, — оборвала МакГонагалл.",
    "Пожилой человек поднял руку, чтобы прикоснуться к Гарри, но тут же её опустил.",
"— Я рад, что вы живы, — пробормотал он. — Спасибо, Гарри Поттер. Спасибо за то, что вы сделали… А теперь я оставлю вас.",
    "И он медленно направился прочь, к центральной части Косого Переулка.",
    "МакГонагалл мрачно и напряжённо огляделась по сторонам. Гарри последовал её примеру и тоже огляделся вокруг. Но на улице не было ничего, кроме старых листьев, а из прохода, ведущего в Косой переулок, можно было видеть лишь снующих туда-сюда прохожих.",
    "Наконец МакГонагалл расслабилась.",
"— Нехорошо получилось, — тихо сказала она. — Я знаю, вы не привыкли к такому, мистер Поттер, но люди проявляют к вам искренний интерес. Пожалуйста, будьте к ним добры.",
    "Гарри опустил глаза.",
"— Это они зря, — с горечью протянул он. — В смысле проявляют интерес.",
           " — Но вы же спасли их от Сами-Знаете-Кого, — заметила МакГонагалл. — Почему зря?",
    "Гарри посмотрел на профессора и вздохнул:",
            "— Полагаю, если я скажу вам о фундаментальной ошибке атрибуции, вы меня не поймёте?",
    "МакГонагалл покачала головой:",
            "— Нет, но постарайтесь объяснить, если вас не затруднит.",
            "— Ну… — Гарри задумался, как бы объяснить попонятнее. — Представьте, вы пришли на работу и увидели, как ваш коллега пинает стол. Вы думаете: «Какой же у него скверный характер». В это время ваш коллега думает о том, как по дороге на работу его кто-то толкнул, а потом накричал на него. «Кто угодно на моём месте разозлился бы», — думает он. Мы не можем залезть людям в головы и узнать, почему они ведут себя тем или иным образом. Вместо этого мы склонны объяснять поведение людей особенностями их характера, своё же собственное поведение мы чаще объясняем наоборот — внешними обстоятельствами. Таким образом, фундаментальная ошибка атрибуции — это склонность человека объяснять поступки и поведение других людей их личностными особенностями, а не внешними факторами и ситуацией.",
    "Существовал ряд изящных экспериментов, подтверждающих данное явление, но Гарри не хотелось углубляться в детали.",
    "МакГонагалл удивлённо подняла брови.",
            "— Кажется, я поняла… — медленно проговорила она. — Но какое это имеет отношение к вам?",
    "Гарри пнул кирпичную стену так, что стало больно:",
            "— Люди думают, что я спас их от Сами-Знаете-Кого, потому что я какой-нибудь великий воин Света.",
            "— Тот, кто наделён могуществом победить Тёмного Лорда… — пробормотала МакГонагалл. В её голосе прозвучала ирония, которую Гарри тогда не понял.",
"— Да, — в мальчике боролись раздражение и разочарование, — как будто я уничтожил его, потому что мне свойственно убивать тёмных лордов. Мне же было всего пятнадцать месяцев! Я совершенно не представляю, что тогда произошло, но предположу, что это было связано со случайными обстоятельствами. И никак не связано с моими личностными особенностями. Люди проявляют интерес не ко мне, на меня самого они не обращают внимания, они хотят пожать руку плохому объяснению, — Гарри замолчал и посмотрел на МакГонагалл. — Может, вы знаете, что тогда произошло на самом деле?",
            "— Пожалуй, у меня появилась догадка… — сказала МакГонагалл. — После встречи с вами.",





            "— И?",

            "— Вы победили Тёмного Лорда, ибо вы ужасней его, и выжили после проклятья, потому что вы страшнее Смерти.",

            "— Ха. Ха. Ха, — Гарри снова пнул стену.",

    "МакГонагалл усмехнулась.",

            "— Теперь пора заглянуть к мадам Малкин. Ваша магловская одежда привлекает внимание.",

    "По пути они столкнулись ещё с двумя доброжелателями.",






            "* * *",

    "МакГонагалл остановилась у входа в «Магазин мадам Малкин». Это было невероятно скучное здание из обычного красного кирпича. В витринах висели простые чёрные мантии. Не сверкающие, не меняющие цвет, не испускающие странные лучи, которые как бы проходят прямо через рубашку и щекочут тебя, а обычные чёрные мантии — по крайней мере, это всё, что можно было разглядеть через витрину. Дверь магазина была широко раскрыта, будто говоря: «Здесь нет никаких секретов, нам нечего скрывать».",
            "— Я отойду на несколько минут, пока вы будете примерять мантии, — сказала МакГонагалл. — Вы справитесь с этим сами?",
    "Гарри кивнул. Он страстно ненавидел магазины одежды, так что не мог винить МакГонагалл за то же чувство.",
    "Профессор дотронулась до головы Гарри волшебной палочкой.",
"— Я снимаю заклинание маскировки, мадам Малкин нужно будет чётко вас видеть.",
            "— А… — Гарри такое положение немного беспокоило.",
"— Я училась с ней в Хогвартсе, — сказала МакГонагалл. — Даже тогда она была невероятно спокойным человеком. Наведайся к ней в магазин сам Тёмный Лорд, она и глазом не моргнёт, — профессор говорила очень ободряюще. — Мадам Малкин не будет вам надоедать. И никому другому не позволит это делать.",
"— А куда пойдёте вы? — спросил Гарри. — На случай, если, знаете ли, что-нибудь всё-таки произойдёт.",
    "МакГонагалл насторожённо посмотрела на мальчика.",
"— Я пойду туда, — указала она на здание через дорогу, над дверью которого болталась вывеска с изображением бочонка, — и что-нибудь выпью, это мне сейчас просто необходимо. А вы будете примерять мантии, и больше ничего. Я очень скоро вернусь, чтобы проверить ваши успехи, и крайне надеюсь увидеть «Магазин мадам Малкин» в целости и сохранности.",
    "Хозяйка оказалась суетливой пожилой женщиной. Увидев шрам Гарри, она и слова ему не сказала и бросила грозный взгляд на свою помощницу, когда та открыла рот. Мадам Малкин достала набор живых, извивающихся кусочков ткани, которые служили мерными лентами, и приступила к работе.",
    "Рядом с Гарри стоял бледный мальчик с заострённым лицом и обалденными белыми волосами. Похоже, он проходил заключительный этап той же процедуры. Одна из двух помощниц Малкин тщательно осматривала белобрысого и его мантию шахматной расцветки, иногда дотрагивалась до неё палочкой, чтобы подогнать по фигуре.",
            "— Привет, — сказал мальчик. — Тоже в Хогвартс?",


            "Гарри мог легко представить, куда заведёт этот разговор, и решил, что на сегодня с него хватит.",
"— О боже, — прошептал Гарри и широко раскрыл глаза, — не может быть. Ваше… имя, сэр?",

            "— Драко Малфой, — немного озадаченно ответил тот.",
            "— Так это вы! Драко Малфой. Я… Я никогда не думал, что мне выпадет такая честь, сэр, — Гарри было жаль, что он не умеет пускать слезу. Обычно при встрече с ним самим люди начинали плакать именно после этой фразы.",
"— О, — Драко на мгновение смутился. Затем его губы растянулись в самодовольной улыбке. — Приятно встретить человека, который знает своё место.",
    "Одна из помощниц, ранее узнавшая Гарри, поперхнулась.",
    "Гарри продолжал бормотать:",
            "— Я так рад, что встретил вас, мистер Малфой. Не могу выразить словами, как я рад. Я буду учиться с вами на одном курсе! Моё сердце замирает от восторга.",
            "Ой. Кажется, последняя часть прозвучала немного странно, будто он испытывал к Драко не просто уважение, а кое-что большее.",
            "— И я рад видеть человека, который с должным уважением относится к семье Малфоев, — мальчик наградил Гарри той улыбкой, которую сиятельнейший король дарует своему ничтожному подданному, если этот подданный честен, хоть и беден.",
    "Чёрт. Гарри пытался придумать, что же сказать дальше. Хм, всем очень хотелось пожать руку Гарри Поттеру, поэтому:",
            "— Когда я закончу примерку, сэр, не разрешите ли вы пожать вашу руку? Это было бы лучшим событием за весь день. Нет, за месяц. Нет-нет, за всю мою жизнь!",
    "Драко сердито посмотрел в его сторону:",
            "— Какая непозволительная фамильярность! Что ты сделал для семьи Малфоев, чтобы просить о подобном?",
    "Хм, интересная мысль. Теперь я знаю, что сказать следующему человеку, который захочет пожать мне руку.",
    "Гарри склонил голову:",
            "— Простите, сэр, я понимаю. Извините мою дерзость. Для меня скорее будет честью почистить вашу обувь.",
            "— Именно, — огрызнулся Драко, но потом смягчился. — Хотя твоя просьба вполне понятна. Скажи, ты на какой факультет, по-твоему, попадёшь? Я, конечно, пойду в Слизерин, как и мой отец Люциус в своё время. А тебя, полагаю, с радостью примут пуффендуйцы или, пожалуй, домовые эльфы.",
    "Гарри застенчиво улыбнулся:",
            "— Профессор МакГонагалл сказала, что я когтевранец до мозга костей и буду лучшим учеником на этом факультете, и сама Ровена попросит меня поберечь себя и не учиться так усердно, что бы это ни значило, и что я определённо окажусь в Когтевране, если только Распределяющая шляпа не начнёт громко кричать от ужаса, так что никто не сможет разобрать ни слова. Конец цитаты.",
            "— Ух ты, — Драко, похоже, был слегка впечатлён. Грустно вздохнув, он продолжил: — Твоя лесть была хороша, по крайней мере, мне так показалось. В любом случае Слизерин тебе тоже подойдёт. Обычно только моему отцу оказывают такое уважение. Надеюсь, что вот теперь, когда я буду учиться в Хогвартсе, остальные слизеринцы будут относиться ко мне должным образом. Думаю, твоё поведение — хороший знак.",
    "Гарри кашлянул:",
            "— На самом деле я понятия не имею, кто ты, прости.",
            "— Не может быть! — Драко был крайне разочарован. — Зачем тогда ты всё это говорил? — его глаза расширились от внезапной догадки. — Как ты можешь совсем не знать о Малфоях? И что это на тебе надето? Твои родители — маглы?!",
           "— Одни мои папа с мамой мертвы, — сердце Гарри сжалось. — Другие мои родители — маглы, они вырастили меня.",
"— Что?! — сказал Драко. — Да кто же ты?",
            "— Гарри Поттер. Рад познакомиться.",
            "— Гарри Поттер?! — удивлённо выдохнул Драко. — Тот самый Гарри… — мальчик осёкся.",
    "Наступила тишина, затем:",
            "— Гарри Поттер? Тот самый Гарри Поттер?! — с восторгом воскликнул Драко. — Мерлин, я всегда хотел встретиться с тобой!",
    "Помощница мадам Малкин, которая примеряла мантию на Драко, поперхнулась, но тут же продолжила работу.",
"— Заткнись, — сказал Гарри.",
            "— Можно взять у тебя автограф? Нет, лучше сначала сфотографируемся вместе!",
            "— Заткнисьзаткнисьзаткнись.",
"— Я так рад познакомиться с тобой!",
            "— Сдохни.",
"— Но ты же Гарри Поттер, знаменитый спаситель волшебного мира, одержавший победу над Тёмным Лордом! Всеобщий герой Гарри Поттер! Я всегда хотел быть похожим на тебя, когда вырасту, чтобы я тоже мог…",
    "Драко осёкся на середине предложения. Его лицо застыло от ужаса.",
            "Высокий, светловолосый, элегантный мужчина в мантии самого лучшего качества. Его рука сжимает серебряный набалдашник трости, наводящей на мысль о смертельном оружии. Его глаза осмотрели комнату с невозмутимостью палача, для которого убийство — не болезненный и даже не запретный акт, а естественный, как дыхание, процесс. «Совершенство» — вот слово, как нельзя лучше характеризующее появившегося мужчину.",
"— Драко, — сердито сказал мужчина, растягивая слова. — Что ты только что сказал?!",
    "За долю секунды Гарри придумал план спасения Драко.",
            "— Люциус Малфой! — выдохнул Гарри Поттер. — Тот самый Люциус Малфой?",
    "Одной из помощниц мадам Малкин пришлось отвернуться, чтобы в открытую не прыснуть со смеху.",
    "Холодные глаза убийцы посмотрели на него.",
            "— Гарри Поттер.",
            "— Такая честь встретить вас!",
    "Тёмные глаза расширились от удивления.",
"— Ваш сын рассказал мне о вас всё! — Гарри быстро продолжал натиск, не сильно заботясь о том, что говорит. — Но, конечно, я всё знал и раньше, ведь все знают о вас, великом Люциусе Малфое! Лучшем представителе факультета Слизерин всех времён. Я хочу попасть в Слизерин, потому что вы там учились, когда были ребёнком…",
            "— О чём вы говорите, мистер Поттер?! — раздался крик снаружи, а через мгновение в магазин ворвалась профессор МакГонагалл.",
    "На её лице застыло выражение такого ужаса, что Гарри открыл было рот, но тут же его захлопнул, не зная, что сказать.",
"— Профессор МакГонагалл! — воскликнул Драко. — Это действительно вы? Я столько о вас слышал от своего отца. Я хочу попасть в Гриффиндор, потому что…",
            "— Что?! — стоя бок о бок, хором рявкнули Люциус Малфой и профессор МакГонагалл. Они повернулись, чтобы посмотреть друг на друга, а потом отскочили в разные стороны, будто исполняли танец.",
    "Затем Люциус быстро схватил Драко и вытащил его из магазина.",
    "И наступила тишина.",
    "МакГонагалл посмотрела вниз на небольшой бокал вина, который всё ещё держала в руке. Он был сильно наклонён из-за спешки, на дне осталось всего несколько капель.",
    "Профессор прошла вглубь магазина к владелице.",
            "— Мадам Малкин, — тихо сказала МакГонагалл. — Что здесь произошло?",
    "Хозяйка оглянулась, а потом… расхохоталась. Она облокотилась о стену, задыхаясь от смеха. Следом за ней рассмеялись помощницы, одна из которых, истерично хихикая, опустилась на пол.",
    "МакГонагалл медленно повернулась к Гарри:",
            "— Оставила вас на шесть минут. Шесть минут, мистер Поттер. Ровно.",
"— Я всего лишь пошутил, — возмутился Гарри под новый взрыв смеха.",
            "— Драко Малфой сказал перед своим отцом, что хочет попасть в Гриффиндор! Простой шутки на это бы не хватило! — МакГонагалл замолчала, тяжело дыша. — По-моему, вы неправильно меня расслышали. Я сказала: «Подберите себе одежду», а не «Наложите заклинание Конфундус на весь мир»!",
            "— Драко находился в ситуативном контексте, который объясняет его поведение…",
            "— Нет. Даже не пытайтесь. Не хочу знать, что здесь произошло. Никогда. Есть вещи, относительно которых я должна оставаться в неведении, и это — одна из них. Какой бы демонической силой хаоса вы ни обладали, она заразна. Я не желаю закончить как бедный Драко Малфой, бедная мадам Малкин и две её бедные помощницы.",
    "Гарри вздохнул. Совершенно ясно, что профессор МакГонагалл была не в настроении выслушивать разумные объяснения. Он посмотрел на мадам Малкин, всё ещё тяжело дышавшую от смеха, на двух её помощниц, которые уже обе оказались на полу, и, наконец, на себя, обвитого мерными лентами.",
"— Моя мантия ещё не совсем готова, — вежливо сказал Гарри. — Может, вам вернуться и выпить ещё что-нибудь?"






    };



    public final String [] fiveScenario_eng = {

            "It would take supernatural intervention for him, given his environment, to have your morals.",
            "The hidden shop was a small, quaint (some would even call it cute) shop, conveniently located behind a vegetable stand, which, in turn, was behind the magic glove store, which was located two steps from the branch of Diagon alley. Much to his disappointment, the shopkeeper was not a mysterious, wrinkled old man, but a nervous-looking young woman dressed in a faded yellow robe. And now she was holding the QX31 super-stealth purse, which had an enlarged opening and an invisible expansion charm that allowed her to put large items in it (the total volume was, however, limited).",
            "Harry actually insisted on visiting this shop immediately after the Bank, while trying not to arouse McGonagall's suspicions. The thing was, he needed to put something in his wallet as soon as possible. And it wasn't the bag of Galleons that McGonagall had allowed him to take from Gringotts. These were other Galleons that Harry had surreptitiously put in his pocket after accidentally falling on a pile of gold. Honestly, by accident. Harry wasn't the type to miss out on opportunities, but it did happen spontaneously. Now he had to carry a bag of allowed Galleons next to his trousers pocket (this was extremely inconvenient), so that the ringing would not arouse suspicion.",
            "The only question was: how to put those other coins in the purse and not get caught? The Galleons might have been his, but they were still stolen. Self-stolen? Auto-stolen?",
            "Harry looked up from the QX31 super stealth purse on the counter and looked up at the saleswoman.",
            "Can I try it out? To verify that it works... reliably, — it has widely opened eyes, portraying a naive, playful boy.",
            "Of course, after the tenth repeat put the bag in the purse, reach inside the purse, whispering bag of gold out the bag, McGonagall walked away from Harry and began to examine other items, and the store owner turned in her direction.",
            "With his left hand, Harry put the bag of gold in his purse. With his right hand, he pulled a few Galleons out of his pocket, dropped them into his purse, and (whispering bag of gold) got the bag again. Then he put the bag back in the pouch with his left hand and reached into his pocket again with his right…",
            "McGonagall looked back at Harry only once, but he didn't flinch or freeze in place, so the Professor didn't suspect anything. Although you can't be sure of anything if an adult has a sense of humor. The operation had to be repeated three times, so that the gold stolen from himself — about thirty Galleons — was transferred to the hidden purse.",
            "When he finished, Harry wiped the sweat from his brow and exhaled:",


            "— I would like to buy it.",
            "Minus 15 Galleons (for this money you can buy two magic wands) and plus one SUPER purse-skryten QX31. When Harry and McGonagall left the store, the door handle turned into a normal hand and waved them goodbye, twisting in a way that made Harry feel a little uneasy.",
            "And then, unfortunately…",
            "Are you... really Harry Potter?  the old man whispered. A large tear rolled down his cheek. It's true, isn't it? I've heard rumors that you didn't actually survive the Death curse, which is why nothing has been heard of you since.",
            "It seems that McGonagall's cloaking spell worked less effectively against more experienced wizards.",
            "The Professor put her hand on the boy's shoulder and pulled him into the nearest alley as soon as she heard Harry Potter?",
            "The elderly man followed them, but at least no one else heard his sentence.",
            "Harry considered the question. Is he really Harry Potter?",
            "I only know what people have told me. I don't remember how he was born — he rubbed his forehead with his hand. I've always had this scar. And I've always been told that my name is Harry Potter, but if you have any reason to doubt it, you can also assume that there is a secret conspiracy, the participants of which found another orphan wizard and raised him to believe that he is Harry Potter.",
    "McGonagall rubbed an irritated hand across her face:",

            "You look like your father when he first arrived at Hogwarts. Even from your character alone, it's safe to say that you are related to the Gryffindor thunderstorm, James Potter.",
            "*— She *could be an accomplice, too, said Harry.",
            "No, the elderly man said in a trembling voice. She's right. You have your mother's eyes.",
            "Hmm, — Harry frowned. I suppose you could have been part of the conspiracy...",
            "That's enough, Mr. Potter, — McGonagall snapped.",
            "The elderly man raised his hand to touch Harry, but immediately dropped it.",
            "I'm glad you're alive, — he murmured. Thank you for what you did… I'll leave you now.",
            "And he walked slowly away, toward the center of Diagon alley.",
    "McGonagall looked around, grim and tense. Harry followed her example and looked around, too. But there was nothing on the street but old leaves, and from the passage leading to Diagon alley, you could only see passers-by scurrying to and fro.",
            "Finally, McGonagall relaxed.",
            "It wasn't good, — she said softly. I know you're not used to this, Mr. Potter, but people are genuinely interested in you. Please be kind to them.",
            "Harry looked down.",
            "They shouldn't have, — he said bitterly. — In the sense of showing interest.",
            "But you Saved them from You-Know-Who, — McGonagall said.",
            "Harry looked at the Professor and sighed:",
            "I suppose if I tell you about a fundamental attribution error, you won't understand me?",
    "McGonagall shook her head.",
            "No, but try to explain if you don't mind.",
            "Well... Harry wondered how to explain it better. — Imagine, you came to work and saw your colleague kicking the table. Do you think: What a bad temper he has. At this time, your colleague thinks about how someone pushed him on the way to work, and then yelled at him. Anyone would be angry, he thinks. We can't get inside people's heads and find out why they behave in one way or another. Instead, we tend to explain people's behavior by their character traits, while we often explain our own behavior the other way around — by external circumstances. Thus, a fundamental attribution error is a person's tendency to explain the actions and behavior of other people by their personal characteristics, rather than by external factors and situations.",
            "There were a number of elegant experiments confirming this phenomenon, but Harry didn't want to go into details.",
    "McGonagall raised her eyebrows.",
            "I think I understand...she said slowly. But what does this have to do with you?",
            "Harry kicked a brick wall so hard it hurt:",
            "People think I saved them from You-Know-Who because I'm some great warrior of the Light.",
            "Someone with the power to defeat the Dark Lord... — muttered McGonagall. There was an irony in her voice that Harry didn't understand at the time.",
            "Yes, the boy said, his irritation and frustration warring,  it's as if I destroyed him because I tend to kill dark lords. I was only fifteen months old! I have absolutely no idea what happened then, but I will assume that it was due to accidental circumstances. It has nothing to do with my personality. People aren't interested in me, they don't pay attention to me, they want to shake hands with a bad explanation, Harry paused and looked at McGonagall.",
            "I guess I had a hunch, McGonagall said,  after meeting you.",





            "— And?",

            "You defeated the Dark Lord because you are more terrible than him, and you survived the curse because you are more terrible than Death.",

            "Ha. Ha. Ha, — Harry kicked the wall again.",

            "McGonagall chuckled.",

            "Now it's time to see Madame Malkin. Your Muggle clothes attract attention.",

            "Along the way, they encountered two more well-wishers.",






            "* * *",

            "McGonagall stopped at the entrance to Madame Malkin's store. It was an incredibly boring building made of ordinary red brick. Plain black robes hung in the Windows. Not sparkling, not changing color, not emitting strange rays that seem to pass right through your shirt and tickle you, but ordinary black robes — at least that's all you could see through the window. The store's door was wide open, as if to say, there are no secrets here, we have nothing to hide.",
            "I'll step back for a few minutes while you try on your robes, — McGonagall said. Can you handle it yourself?",
            "Harry nodded. He hated clothing stores passionately, so he couldn't blame McGonagall for feeling the same way.",
            "The Professor touched Harry's head with her wand.",
            "— I'm removing the disguise spell, Madam Malkin will need to see you clearly.",
            "Harry was a little worried about this situation.",
            "I went to Hogwarts with her, McGonagall said.  even then, she was an incredibly calm person. If the Dark Lord himself visited her shop, she wouldn't blink,  the Professor said very encouragingly. Madame Malkin won't bother you. And he won't let anyone else do it.",
            "Where will you go?  Harry asked. In case, you know, something does happen.",
    "McGonagall looked at the boy warily.",
            "I'll go there, she said, pointing to the building across the street, where a sign with a picture of a keg hung over the door,  and get something to drink. And you will try on the robes, and nothing else. I'll be back very soon to check on your progress, and I very much hope to see Madame Malkin's store in one piece.",
            "The hostess turned out to be a fussy old woman. When she saw Harry's scar, she didn't say a word to him and glared at her assistant when she opened her mouth. Madame Malkin took out a set of living, wriggling pieces of cloth that served as measuring tapes and set to work.",
            "Next to Harry was a pale boy with a pointed face and awesome white hair. It looks like he was going through the final stage of the same procedure. One of Malkin's two assistants carefully examined the blond man and his chess-colored robe, sometimes touching it with a wand to adjust it to the shape.",
            "Hi, the boy said. Also at Hogwarts?",


            "Harry could easily imagine where this conversation would lead, and decided that he had had enough for the day.",
            "Oh, my God, Harry whispered, and his eyes widened. Your... name, sir?",

            "Draco Malfoy, he said, a little puzzled.",
            "So it's you! Draco Malfoy. I-I never thought I'd be so honored, sir,  Harry said, wishing he could shed a tear. It was usually after this phrase that people started crying when they met him.",
            "Draco was momentarily confused. Then a smug smile curved his lips. It's nice to meet someone who knows their place.",
            "One of the assistants, who had previously recognized Harry, choked.",

            "Harry kept muttering:",
            "I'm so glad I met you, Mr. Malfoy. I can't tell you how happy I am. I will study with you in the same course! My heart skips a beat with delight.",
            "Oh. I think the last part sounded a little strange, like he had more than just respect for Draco.",
            "And I am glad to see a man who has the proper respect for the Malfoy family, the boy said, giving Harry the smile that the most illustrious king bestows on a lowly subject if that subject is honest, even if he is poor.",
            "Trait. Harry tried to think of something to say next. Hmm, everyone really wanted to shake Harry Potter's hand, so:",
            "When I finish fitting, sir, may I shake your hand? That would be the best thing to happen all day. No, for a month. No, no, for all my life!",
    "Draco glared at him.",
            "What an impermissible familiarity! What did you do for the Malfoy family to ask for such a thing?",
            "Hmm, interesting idea. Now I know what to say to the next person who wants to shake my hand.",


            "Harry bowed his head.",
            "I'm sorry, sir, I understand. Excuse my impertinence. I'd rather be honored to clean your shoes.",
            "Exactly, — Draco snapped, but then relented. Your request is understandable, though. Tell me, what Department do you think you'll get into? I will, of course, go to Slytherin, as my father Lucius did in his time. And you, I suppose, will be welcomed by the Hufflepuff, or perhaps the house-elves.",
    "Harry smiled shyly:",
            "Professor McGonagall said that I was a Ravenclaw to the core and would be the best student in this Department, and Rowena herself would ask me to take care of myself and not study so hard, whatever that meant, and that I would definitely end up in Ravenclaw unless the Sorting hat started screaming loudly in terror so that no one could make out a word. Unquote.",
    "Draco seemed slightly impressed. With a sad sigh, he continued,  your flattery was good, or so I thought. In any case, Slytherin will suit you, too. Usually, only my father gets that kind of respect. I hope that now, when I go to Hogwarts, the rest of the Slytherins will treat me properly. I think your behavior is a good sign.",
            "Harry coughed:",


            "I don't really know who you are, I'm sorry.",
            "It can't be!  Draco was extremely disappointed. Then why did you say all this?  his eyes widened at the sudden realization. How can you not know about the Malfoys? What are you wearing? Are your parents Muggles?!",
            "Only my father and mother are dead, Harry's heart sank. My other parents are Muggles, they raised me.",
            "What?!  Draco said. Who are you?",
            "— Harry Potter. Nice to meet you.",
            "— Harry Potter?!  Draco gasped in surprise. The Harry- — the boy stopped.",
            "There was silence, then:",
            "— Harry Potter? The same Harry Potter?!  Draco exclaimed with delight. Merlin, I've always wanted to meet you!",
            "Madame Malkin's assistant, who was trying on Draco's robes, choked, but immediately continued working.",
            "Shut up, — said Harry.",
            "Can I get your autograph? No, we'd better take a picture together first!",
            "— Shut up, shut up, shut up.",
            "— I'm so happy to meet you!",
            "— Die.",


            "But you're Harry Potter, the famous Savior of the Wizarding world, who defeated the Dark Lord! Everyone's hero Harry Potter! I always wanted to be like you when I grew up, so that I could also...",
            "Draco stopped mid-sentence. His face was frozen with horror.",
            "A tall, fair-haired, elegant man in a robe of the best quality. His hand is gripping the silver head of a walking stick that looks like a deadly weapon. His eyes scanned the room with the calmness of an executioner for whom murder is not a painful or even forbidden act, but a natural process, like breathing. Perfection is the word that best describes the man who appeared.",
            "Draco, the man drawled angrily. What did you just say?",
            "In a split second, Harry came up with a plan to save Draco.",
            "— Lucius Malfoy!  Harry Potter gasped. The Lucius Malfoy?",
            "One of Madame Malkin's assistants had to turn away to avoid openly bursting out laughing.",
            "The killer's cold eyes looked at him.",
            "— Harry Potter.",


            "— Such an honor to meet you!",
            "Dark eyes widened in surprise.",
            "Your son told me everything about you!  Harry pressed on quickly, not caring much what he said. But, of course, I knew everything before, because everyone knows about you, the great Lucius Malfoy! The best representative of the Slytherin house of all time. I want to go to Slytherin because you went there when you were a kid...",
            "What are you talking about, Mr. Potter?!  there was a shout from outside, and a moment later Professor McGonagall burst into the shop.",
            "There was an expression of such horror on her face that Harry opened his mouth, then closed it again, not knowing what to say.",
            "Professor McGonagall! — exclaimed Draco. Is that really you? I've heard so much about you from my father. I want to get into Gryffindor because...",
            "What?!  Lucius Malfoy and Professor McGonagall barked in unison, standing side by side. They turned to look at each other, and then bounced off in different directions as if they were performing a dance.",
            "Then Lucius quickly grabbed Draco and dragged him out of the store.",
            "And there was silence.",
    "McGonagall looked down at the small glass of wine still in her hand. It was heavily tilted due to the rush, there were only a few drops left at the bottom.",
            "The Professor went deep into the store to the owner.",
            "Madam Malkin, McGonagall said softly.",
            "The hostess looked around, and then... burst out laughing. She leaned against the wall, choking with laughter. The assistants followed, one of them dropping to the floor, giggling hysterically.",
    "McGonagall turned slowly to Harry.",
            "I left you for six minutes. Six minutes, Mr. Potter. Exactly.",
            "I was only joking, said Harry indignantly, to another burst of laughter.",
            "Draco Malfoy said in front of his father that he wants to go to Gryffindor! A simple joke wouldn't be enough!  McGonagall paused, breathing hard. I don't think you heard me correctly. I said: Pick your own clothes, not cast a Confundus spell on the whole world!",
            "— Draco was in a situational context that explains his behavior...",
            "— No. Even try. I don't want to know what happened here. Never. There are things about which I must remain ignorant, and this is one of them. Whatever demonic power of chaos you possess is contagious. I don't want to end up like poor Draco Malfoy, poor Madam Malkin and her two poor assistants.",
    "Harry sighed. It was clear that Professor McGonagall was in no mood for reasonable explanations. He looked at Madame Malkin, who was still panting with laughter, at her two assistants, who were both on the floor, and finally at himself, who was wrapped in measuring ribbons.",
            "My robes aren't quite ready yet, — Harry said politely. Why don't you go back and have another drink?"





    };






}
