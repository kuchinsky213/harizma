package com.ggs.harryandrationalthinking.levels.Level4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;

import com.ggs.harryandrationalthinking.R;
import com.ggs.harryandrationalthinking.levels.Level3.Level3;
import com.ggs.harryandrationalthinking.levels.Level3.Preview_lvl3;
import com.ggs.harryandrationalthinking.levels.level2.Level2;
import com.ggs.harryandrationalthinking.levels.level2.Preview;

public class Preview_Lvl4 extends AppCompatActivity {
    private final int SPLASH_DISPLAY_LENGHT = 35000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview__lvl4);

        Button bext = (Button)findViewById(R.id.bext1);
        bext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(Preview_Lvl4.this, Level4.class);

                Preview_Lvl4.this.startActivity(mainIntent);

                Preview_Lvl4.this.finish();
            }
        });


        new Handler().postDelayed(new Runnable() {

            @Override

            public void run() {

                Intent mainIntent = new Intent(Preview_Lvl4.this, Level4.class);

                Preview_Lvl4.this.startActivity(mainIntent);

                Preview_Lvl4.this.finish();

            }

        }, SPLASH_DISPLAY_LENGHT);

    }



    @Override

    public void onBackPressed() {

        super.onBackPressed();

    }

}