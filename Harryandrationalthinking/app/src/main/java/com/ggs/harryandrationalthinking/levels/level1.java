package com.ggs.harryandrationalthinking.levels;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.appodeal.ads.Appodeal;
import com.explorestack.consent.Consent;
import com.ggs.harryandrationalthinking.R;
import com.ggs.harryandrationalthinking.data.OneTable;

import java.util.ArrayList;
import java.util.Locale;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import com.appodeal.ads.Appodeal;
import com.appodeal.ads.Native;
import com.appodeal.ads.NativeAd;
import com.appodeal.ads.NativeAdView;
import com.appodeal.ads.UserSettings;

import com.explorestack.consent.Consent;
import com.explorestack.consent.ConsentForm;
import com.explorestack.consent.ConsentFormListener;
import com.explorestack.consent.ConsentManager;
import com.explorestack.consent.exception.ConsentManagerException;

public class level1 extends AppCompatActivity {
    Button dalee;
    TextView tv0_1, tv0_2, tv0_3, tv0_4, tv0_5;
    OneTable oneTable = new OneTable();
    private static final String CONSENT = "consent";
    boolean consent;
    public static final String APP_KEY = "8a79a7084c6bd8c62024f867ee06eaac4d7682bd1bbb0e7b";
    private InterstitialAd mInterstitialAd;
    public static Intent getIntent(Context context, boolean consent) {
        Intent intent = new Intent(context, level1.class);
        intent.putExtra(CONSENT, consent);
        return intent;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       Consent consent = ConsentManager.getInstance(this).getConsent();
        Appodeal.initialize(this, APP_KEY, Appodeal.INTERSTITIAL|Appodeal.NON_SKIPPABLE_VIDEO, consent);

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {

             //   mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }
        });
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-7802337876801234/7826926116");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
//        if (savedInstanceState == null) {
//            consent = getIntent().getBooleanExtra(CONSENT, false);
//        } else {
//            Consent.Status consentStatus = ConsentManager.getInstance(this).getConsentStatus();
//            consent = consentStatus == Consent.Status.PERSONALIZED
//                    || consentStatus == Consent.Status.PARTLY_PERSONALIZED;
//        }
//        MobileAds.initialize(this, new OnInitializationCompleteListener() {
//            @Override
//            public void onInitializationComplete(InitializationStatus initializationStatus) {
//            }
//        });

        setContentView(R.layout.activity_level1);
        SharedPreferences save = getSharedPreferences("Save", MODE_PRIVATE);
        SharedPreferences.Editor editor = save.edit();
        editor.putInt("Level", 0);
        editor.commit();
        final Animation text_game = AnimationUtils.loadAnimation(level1.this, R.anim.text_in_game);

        ArrayList<TextView> arrtextView = new ArrayList<TextView>();

        tv0_1 = findViewById(R.id.tv0_1);
        tv0_2 = findViewById(R.id.tv0_2);
        tv0_3 = findViewById(R.id.tv0_3);
        tv0_4 = findViewById(R.id.tv0_4);
        tv0_5 = findViewById(R.id.tv0_5);
        dalee = findViewById(R.id.buttonN);

//-------------------------------------------------------------------------------
        arrtextView.add(tv0_1);
        arrtextView.add(tv0_2);
        arrtextView.add(tv0_3);
        arrtextView.add(tv0_4);
        arrtextView.add(tv0_5);

        int i;
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                //Toast.makeText(getApplicationContext(), "loaded", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdFailedToLoad(LoadAdError adError) {
                // Code to be executed when an ad request fails.
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }

            @Override
            public void onAdOpened() {
                Intent lv11 = new Intent(level1.this, Level1_1.class);
                startActivity(lv11);
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                Intent lv11 = new Intent(level1.this, Level1_1.class);
                startActivity(lv11);
            }
        });
//----------zapolnyayu text

        if ( Locale.getDefault().getLanguage()=="ru"){
            for (i = 0; i < arrtextView.size(); i++) {
                arrtextView.get(i).setText(oneTable.onescenario_ru[i]);
            }
        }else {
            for (i = 0; i < arrtextView.size(); i++) {
                arrtextView.get(i).setText(oneTable.onescenario_eng[i]);
            }
        }



        for (i = 0; i < arrtextView.size(); i++) {
            arrtextView.get(i).setAnimation(text_game);
            arrtextView.get(i).setVisibility(View.VISIBLE);
            dalee.setVisibility(View.VISIBLE);
        }

        dalee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mInterstitialAd.isLoaded()) {

                    mInterstitialAd.show();


                  //  finish();

                }


//                        Intent lv1 = new Intent(level1.this, Level1_1.class);
//                        startActivity(lv1);
//                        finish();
                    else if  (Appodeal.isLoaded(Appodeal.INTERSTITIAL)) {
                    Appodeal.show(level1.this, Appodeal.INTERSTITIAL);
                    Intent lv11 = new Intent(level1.this, Level1_1.class);
                    startActivity(lv11);
                    finish();
                }





                    else {
                        Intent lv111 = new Intent(level1.this, Level1_1.class);
                        startActivity(lv111);
                        finish();
                    }



                }










        });

    }@Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Appodeal.initialize(this, APP_KEY, Appodeal.INTERSTITIAL|Appodeal.NON_SKIPPABLE_VIDEO, consent);
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-7802337876801234/7826926116");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
    }

















}