package by.ggs.universenewyear;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import by.ggs.universenewyear.activities.home.HomeActivity;
import by.ggs.universenewyear.activities.note.NoteActivity;

public class Start extends AppCompatActivity {
ImageView l2;
LinearLayout l1;
    Animation uptodown,downtoup;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        l1=findViewById(R.id.l1);
        l2=findViewById(R.id.l2);



        uptodown = AnimationUtils.loadAnimation(this,R.anim.uptodown);
        downtoup = AnimationUtils.loadAnimation(this,R.anim.downtoup);
        l1.setAnimation(uptodown);
        l2.setAnimation(downtoup);


l2.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent pristupim = new Intent(Start.this, HomeActivity.class);
        startActivity(pristupim);
    }
});

    }
}