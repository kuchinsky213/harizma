package by.ggs.universenewyear.events;

import by.ggs.universenewyear.database.NotesDAO;
import by.ggs.universenewyear.models.Note;


 
 
public class NoteFoldersUpdatedEvent{

	int noteId;

	public NoteFoldersUpdatedEvent(int noteId){
		this.noteId = noteId;
	}

	public int getNoteId(){
		return noteId;
	}

	public Note getNote(){
		return NotesDAO.getNote(noteId);
	}
}
