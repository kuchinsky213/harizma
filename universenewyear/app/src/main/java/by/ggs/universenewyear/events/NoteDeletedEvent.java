package by.ggs.universenewyear.events;

import by.ggs.universenewyear.models.Note;

 
 
 
public class NoteDeletedEvent{
	Note note;

	public NoteDeletedEvent(Note note){
		this.note = note;
	}

	public Note getNote(){
		return note;
	}
}
