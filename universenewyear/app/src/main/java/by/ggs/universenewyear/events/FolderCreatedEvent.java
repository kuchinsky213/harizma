package by.ggs.universenewyear.events;

import by.ggs.universenewyear.models.Folder;
 
public class FolderCreatedEvent{
	private Folder folder;

	public FolderCreatedEvent(Folder folder){
		this.folder = folder;
	}

	public Folder getFolder(){
		return folder;
	}
}
