package by.ggs.universenewyear.database;


import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.List;

import by.ggs.universenewyear.models.Folder;
import by.ggs.universenewyear.models.Note;
import by.ggs.universenewyear.models.Note_Table;

/**
 * Created by MohMah on 8/21/2016.
 */
public class NotesDAO{
	public static List<Note> getLatestNotes(Folder folder){
		if (folder == null)
			return SQLite.select().from(Note.class).orderBy(Note_Table.createdAt, false).queryList();
		else
			return FolderNoteDAO.getLatestNotes(folder);
	}

	public static Note getNote(int noteId){
		return SQLite.select().from(Note.class).where(Note_Table.id.is(noteId)).querySingle();
	}
}
