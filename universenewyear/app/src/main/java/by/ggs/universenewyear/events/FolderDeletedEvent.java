package by.ggs.universenewyear.events;

import by.ggs.universenewyear.models.Folder;




public class FolderDeletedEvent{
	Folder folder;

	public FolderDeletedEvent(Folder folder){
		this.folder = folder;
	}

	public Folder getFolder(){
		return folder;
	}
}
