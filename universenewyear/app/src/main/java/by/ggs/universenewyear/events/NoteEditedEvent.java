package by.ggs.universenewyear.events;

import by.ggs.universenewyear.database.NotesDAO;
import by.ggs.universenewyear.models.Note;


 
 
public class NoteEditedEvent{
	int noteId;

	public NoteEditedEvent(int noteId){
		this.noteId = noteId;
	}

	public int getNoteId(){
		return noteId;
	}
	
	public Note getNote(){
		return NotesDAO.getNote(noteId);
	}
}
