package by.ggs.universenewyear.activities.editfolders;




interface OpenCloseable{
	void close();

	void open();

	boolean isOpen();
}
