package com.ggs.asktheuniverse;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button star;
    TextView enter;
    EditText name;
    SharedPreferences mSettings;
    public static final String APP_PREFERENCES = "mysettings";
    public static final String APP_PREFERENCES_NAME = "";


    @Override
    protected void onResume() {

        if(mSettings.contains(APP_PREFERENCES_NAME)) {
            enter.setText(R.string.Welcome);
            name.setText(mSettings.getString(APP_PREFERENCES_NAME, ""));
        }
        super.onResume();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        star = findViewById(R.id.star);
        enter = findViewById(R.id.enter);
        name = findViewById(R.id.name);
        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        if(mSettings.contains(APP_PREFERENCES_NAME)) {
            enter.setText(R.string.Welcome);
            name.setText(mSettings.getString(APP_PREFERENCES_NAME, ""));
        }




        star.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (name.getText().toString().isEmpty()){


                }
                else {

                    String strNickName = name.getText().toString();
                    SharedPreferences.Editor editor = mSettings.edit();
                    editor.putString(APP_PREFERENCES_NAME, strNickName);
                    editor.apply();


                    Intent message = new Intent(MainActivity.this, ActivityMessage.class);
                    startActivity(message);
                }


            }});
    }
}