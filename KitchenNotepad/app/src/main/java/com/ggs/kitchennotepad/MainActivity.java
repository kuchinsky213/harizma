package com.ggs.kitchennotepad;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.ggs.kitchennotepad.Db.KitchenNote;

public class MainActivity extends Activity {
    Button kitchen, hranenie, measure, gotovka;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        kitchen=findViewById(R.id.notepad);
        hranenie=findViewById(R.id.hranenie);
        measure=findViewById(R.id.measure);
        gotovka=findViewById(R.id.gotovka);







        kitchen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent kitchen = new Intent(MainActivity.this, KitchenNote.class);
                startActivity(kitchen);
            }
        });


        hranenie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent hranenie = new Intent(MainActivity.this, Hranenie.class);
                startActivity(hranenie);
            }
        });

        gotovka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent hranenie = new Intent(MainActivity.this, Gotovka.class);
                startActivity(hranenie);
            }
        });



    }
}