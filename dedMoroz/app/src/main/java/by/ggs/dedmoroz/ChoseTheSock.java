package by.ggs.dedmoroz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

public class ChoseTheSock extends AppCompatActivity {
ImageView socks, refr;
    Animation anim = null;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chose_the_sock);
        socks = findViewById(R.id.socks);
        refr = findViewById(R.id.imageView2);




        anim = AnimationUtils.loadAnimation(this, R.anim.sleep2);
        refr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                socks.startAnimation(anim);
            }
        });


        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {}
        });

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-7802337876801234/4228030217");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
                       @Override
            public void onAdFailedToLoad(LoadAdError adError) {
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }


        });
        socks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent kek = new Intent(ChoseTheSock.this, RandomGenerator.class);
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                    startActivity(kek);
                } else {
                    mInterstitialAd.show();
                    startActivity(kek);
                    Log.d("TAG", "The interstitial wasn't loaded yet.");
                }


            }
        });

    }

}