package by.ggs.dedmoroz;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.tbouron.shakedetector.library.ShakeDetector;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

public class RandomGenerator extends AppCompatActivity  {
    File imagePath;
    Animation anim, anim2;
    ImageView ded, shlem;
    Button share;
    private String sharePath="no";
    private int[] cards = {R.drawable.airpods, R.drawable.bitcoin, R.drawable.bmw, R.drawable.cat1, R.drawable.cat2,
            R.drawable.cents20,
            R.drawable.chasha, R.drawable.chupa_chups, R.drawable.darts, R.drawable.dendi, R.drawable.dog1,
            R.drawable.dog2, R.drawable.doll100, R.drawable.elka,

            R.drawable.fuc,  R.drawable.futb, R.drawable.gameboy, R.drawable.gamno, R.drawable.cons,
            R.drawable.igrushka_elku, R.drawable.intersect, R.drawable.intersect1, R.drawable.intersect2, R.drawable.intersect3,
            R.drawable.iphone, R.drawable.kykish, R.drawable.ledenec, R.drawable.mac,

            R.drawable.mask,     R.drawable.melki,     R.drawable.medved,     R.drawable.mercedes,
            R.drawable.moped,     R.drawable.moto,     R.drawable.mustang,     R.drawable.photo_camera,
            R.drawable.pizza,     R.drawable.potato,     R.drawable.ps,     R.drawable.puzzle,
            R.drawable.radio,     R.drawable.rose,     R.drawable.rubika,     R.drawable.shampo,
            R.drawable.shampo2,     R.drawable.toy,     R.drawable.velo,     R.drawable.xbox,
            R.drawable.yaht
    };



    private void showToast(String s)
    {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    public static Bitmap getScreenShot(View view) {
        View screenView = view.getRootView();
        screenView.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(screenView.getDrawingCache());
        screenView.setDrawingCacheEnabled(false);
        return bitmap;
    }
    public void onClickApp(Bitmap bitmap) {
        PackageManager pm = getPackageManager();
        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            String path = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, getString(R.string.gett), null);
            Uri imageUri = Uri.parse(path);

         //   @SuppressWarnings("unused")
        //    PackageInfo info = pm.getPackageInfo(pack, PackageManager.GET_META_DATA);

            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("image/*");
         //   waIntent.setPackage(pack);
            waIntent.putExtra(android.content.Intent.EXTRA_STREAM, imageUri);
            waIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.pres));
            startActivity(Intent.createChooser(waIntent, getString(R.string.dhs)));
        } catch (Exception e) {
            Log.e("Error on sharing", e + " ");
            Toast.makeText(getApplicationContext(), "App not Installed", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_random_generator);
        ded=findViewById(R.id.shlem);
        shlem=findViewById(R.id.ded);
        anim = AnimationUtils.loadAnimation(this, R.anim.sleep);
//share = findViewById(R.id.buttonShare);
      //  ded.startAnimation(anim);
        View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
        anim2 = AnimationUtils.loadAnimation(this, R.anim.sleep2);
        final int max = 48; // Максимальное число для диапазона от 0 до max
        final int rnd = rnd(max);

//        share.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                Intent intent = new Intent();
////                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
////                Bitmap bitmap = takeScreenshot();
////                saveBitmap(bitmap);
////                shareIt();
//
//
//
//          }
//
//        });

        ShakeDetector.create(this, new ShakeDetector.OnShakeListener() {
            @Override
            public void OnShake() {
                AlertDialog.Builder alert = new AlertDialog.Builder(RandomGenerator.this);

                alert.setTitle("Share my present");
                alert.setMessage("Share Screenshot?");

                // Set an EditText view to get user input

                alert.setPositiveButton("Share", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        if (ContextCompat.checkSelfPermission(RandomGenerator.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(RandomGenerator.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 202);
                        }
                        else {
                            if (ContextCompat.checkSelfPermission(RandomGenerator.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(RandomGenerator.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 203);
                            } else {
                                View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
                                onClickApp(getScreenShot(rootView));
                           //     onClickApp("org.telegram.messenger", getScreenShot(rootView));

                            }
                        }
                    }
                });

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.
                    }
                });
                alert.show();
            }
        });








        Bitmap[] bitmap = new Bitmap[cards.length];
        for (int i = 0; i < cards.length; i++)
            bitmap[i] = BitmapFactory.decodeResource(getResources(), cards[i]);

        try {
            switch (rnd) {


                case 0:

                    ded.setImageBitmap(bitmap[0]);


                    ded.startAnimation(anim2);

                    break;

                case 1:

                    ded.setImageBitmap(bitmap[1]);


                    ded.startAnimation(anim2);

                    break;

                case 2:

                    ded.setImageBitmap(bitmap[2]);


                    ded.startAnimation(anim2);

                    break;

                case 3:

                    ded.setImageBitmap(bitmap[3]);


                    ded.startAnimation(anim2);

                    break;

                case 4:

                    ded.setImageBitmap(bitmap[4]);


                    ded.startAnimation(anim2);

                    break;


                case 5:

                    ded.setImageBitmap(bitmap[5]);


                    ded.startAnimation(anim2);

                    break;

                case 6:

                    ded.setImageBitmap(bitmap[6]);


                    ded.startAnimation(anim2);

                    break;


                case 7:

                    ded.setImageBitmap(bitmap[7]);


                    ded.startAnimation(anim2);

                    break;



                case 8:

                    ded.setImageBitmap(bitmap[8]);


                    ded.startAnimation(anim2);

                    break;

                case 9:

                    ded.setImageBitmap(bitmap[9]);


                    ded.startAnimation(anim2);

                    break;

                case 10:

                    ded.setImageBitmap(bitmap[10]);


                    ded.startAnimation(anim2);

                    break;

                case 11:

                    ded.setImageBitmap(bitmap[11]);


                    ded.startAnimation(anim2);

                    break;

                case 12:

                    ded.setImageBitmap(bitmap[12]);


                    ded.startAnimation(anim2);

                    break;


                case 13:

                    ded.setImageBitmap(bitmap[13]);


                    ded.startAnimation(anim2);

                    break;

                case 14:

                    ded.setImageBitmap(bitmap[14]);


                    ded.startAnimation(anim2);

                    break;


                case 15:

                    ded.setImageBitmap(bitmap[15]);


                    ded.startAnimation(anim2);

                    break;

                case 16:

                    ded.setImageBitmap(bitmap[16]);


                    ded.startAnimation(anim2);

                    break;

                case 17:

                    ded.setImageBitmap(bitmap[17]);


                    ded.startAnimation(anim2);

                    break;

                case 18:

                    ded.setImageBitmap(bitmap[18]);


                    ded.startAnimation(anim2);

                    break;


                case 19:

                    ded.setImageBitmap(bitmap[19]);


                    ded.startAnimation(anim2);

                    break;

                case 20:

                    ded.setImageBitmap(bitmap[20]);


                    ded.startAnimation(anim2);

                    break;


                case 21:

                    ded.setImageBitmap(bitmap[21]);


                    ded.startAnimation(anim2);

                    break;

                case 22:

                    ded.setImageBitmap(bitmap[22]);


                    ded.startAnimation(anim2);

                    break;


                case 23:

                    ded.setImageBitmap(bitmap[23]);


                    ded.startAnimation(anim2);

                    break;

                case 24:

                    ded.setImageBitmap(bitmap[24]);


                    ded.startAnimation(anim2);

                    break;


                case 25:

                    ded.setImageBitmap(bitmap[25]);


                    ded.startAnimation(anim2);

                    break;

                case 26:

                    ded.setImageBitmap(bitmap[26]);


                    ded.startAnimation(anim2);

                    break;


                case 27:

                    ded.setImageBitmap(bitmap[27]);


                    ded.startAnimation(anim2);

                    break;
                case 28:

                    ded.setImageBitmap(bitmap[28]);


                    ded.startAnimation(anim2);

                    break;

                case 29:

                    ded.setImageBitmap(bitmap[29]);


                    ded.startAnimation(anim2);

                    break;


                case 30:

                    ded.setImageBitmap(bitmap[30]);


                    ded.startAnimation(anim2);

                    break;



                case 31:

                    ded.setImageBitmap(bitmap[31]);


                    ded.startAnimation(anim2);

                    break;

                case 32:

                    ded.setImageBitmap(bitmap[32]);


                    ded.startAnimation(anim2);

                    break;


                case 33:

                    ded.setImageBitmap(bitmap[33]);


                    ded.startAnimation(anim2);

                    break;


                case 34:

                    ded.setImageBitmap(bitmap[34]);


                    ded.startAnimation(anim2);

                    break;


                case 35:

                    ded.setImageBitmap(bitmap[35]);


                    ded.startAnimation(anim2);

                    break;


                case 36:

                    ded.setImageBitmap(bitmap[36]);


                    ded.startAnimation(anim2);

                    break;


                case 37:

                    ded.setImageBitmap(bitmap[37]);


                    ded.startAnimation(anim2);

                    break;


                case 38:

                    ded.setImageBitmap(bitmap[38]);


                    ded.startAnimation(anim2);

                    break;


                case 39:

                    ded.setImageBitmap(bitmap[39]);


                    ded.startAnimation(anim2);

                    break;


                case 40:

                    ded.setImageBitmap(bitmap[40]);


                    ded.startAnimation(anim2);

                    break;

                case 41:

                    ded.setImageBitmap(bitmap[41]);


                    ded.startAnimation(anim2);

                    break;


                case 42:

                    ded.setImageBitmap(bitmap[42]);


                    ded.startAnimation(anim2);

                    break;


                case 43:

                    ded.setImageBitmap(bitmap[43]);


                    ded.startAnimation(anim2);

                    break;


                case 44:

                    ded.setImageBitmap(bitmap[44]);


                    ded.startAnimation(anim2);

                    break;


                case 45:

                    ded.setImageBitmap(bitmap[45]);


                    ded.startAnimation(anim2);

                    break;



                case 46:

                    ded.setImageBitmap(bitmap[46]);


                    ded.startAnimation(anim2);

                    break;



                case 47:

                    ded.setImageBitmap(bitmap[47]);


                    ded.startAnimation(anim2);

                    break;


                case 48:

                    ded.setImageBitmap(bitmap[48]);


                    ded.startAnimation(anim2);

                    break;








            } }catch (Exception e){
            e.printStackTrace();
        }








    }


    public static int rnd(int max)
    {
        return (int) (Math.random() * ++max);
    }


    @Override
    protected void onResume() {
        super.onResume();
        ShakeDetector.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        ShakeDetector.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ShakeDetector.destroy();
    }}