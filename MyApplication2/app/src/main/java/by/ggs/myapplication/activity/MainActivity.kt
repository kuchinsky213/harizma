package by.ggs.myapplication.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import by.ggs.myapplication.R
import by.ggs.myapplication.app.addFragment
import by.ggs.myapplication.app.setFragment
import by.ggs.myapplication.fragm.IssuesFragment
import by.ggs.myapplication.fragm.PreferencesFragment
import kotlinx.android.synthetic.main.toolbar.*

class MainActivity : AppCompatActivity() {
    override fun onSupportNavigateUp(): Boolean {
        val fm = supportFragmentManager
        if (fm.backStackEntryCount > 0) {
            fm.popBackStack()
        } else {
            finish()
        }
        return true
    }

    override fun onCreate(state: Bundle?) {
        super.onCreate(state)
        setContentView(R.layout.activity_main)
        initToolbar()

        if (state == null) {
            setFragment(supportFragmentManager, IssuesFragment())
            if (intent?.getBooleanExtra(OPEN_PREFERENCES, false) == true) {
                addFragment(supportFragmentManager, PreferencesFragment())
            }
        }
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportFragmentManager.addOnBackStackChangedListener {
            updateUpArrow()
        }
        updateUpArrow()
    }

    private fun updateUpArrow() {
        supportActionBar?.setDisplayHomeAsUpEnabled(
            supportFragmentManager.backStackEntryCount > 0
        )
    }

    companion object {
        const val OPEN_PREFERENCES = "open_preferences"
    }
}
