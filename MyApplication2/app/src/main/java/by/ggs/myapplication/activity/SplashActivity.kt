package by.ggs.myapplication.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import by.ggs.myapplication.R

class SplashActivity() : AppCompatActivity() {
    override fun onCreate(state: Bundle?) {
        super.onCreate(state)

        // it's important _not_ to inflate a layout file here
        // because that would happen after the app is fully
        // initialized

        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}
