package by.ggs.myapplication.app

import android.app.Application
import by.ggs.myapplication.database.Database
import by.ggs.myapplication.prefs.Preferences


val db = Database()
val prefs = Preferences()

class LibraApp : Application() {
	override fun onCreate() {
		super.onCreate()
		db.open(this)
		prefs.init(this)
	}
}

