package com.ggs.moneymag.db;




import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.ggs.moneymag.dao.DaoAccess;
import com.ggs.moneymag.model.Note;

@Database(entities = {Note.class}, version = 1, exportSchema = false)
public abstract class NoteDatabase extends RoomDatabase {

    public abstract DaoAccess daoAccess();
}