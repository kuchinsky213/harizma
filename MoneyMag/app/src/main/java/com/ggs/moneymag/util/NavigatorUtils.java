package com.ggs.moneymag.util;

import android.app.Activity;
import android.content.Intent;

import com.ggs.moneymag.AppConstants;
import com.ggs.moneymag.activ.AddNoteActivity;
import com.ggs.moneymag.activ.PwdActivity;
import com.ggs.moneymag.model.Note;

import static com.ggs.moneymag.AppConstants.ACTIVITY_REQUEST_CODE;
import static com.ggs.moneymag.AppConstants.INTENT_TASK;

public class NavigatorUtils implements AppConstants {


    public static void redirectToPwdScreen(Activity activity,
                                           Note note) {
        Intent intent = new Intent(activity, PwdActivity.class);
        intent.putExtra(INTENT_TASK, note);
        activity.startActivityForResult(intent, ACTIVITY_REQUEST_CODE);
    }


    public static void redirectToEditTaskScreen(Activity activity,
                                                Note note) {
        Intent intent = new Intent(activity, AddNoteActivity.class);
        intent.putExtra(INTENT_TASK, note);
        activity.startActivityForResult(intent, ACTIVITY_REQUEST_CODE);
    }

    public static void redirectToViewNoteScreen(Activity activity,
                                                Note note) {
        Intent intent = new Intent(activity, AddNoteActivity.class);
        intent.putExtra(INTENT_TASK, note);
        intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
        activity.startActivity(intent);
        activity.finish();
    }
}
