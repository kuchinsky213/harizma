package by.ggs.selectionassistant.libra.app

import android.app.Application
import by.ggs.selectionassistant.database.Database
import by.ggs.selectionassistant.preferences.Preferences


val db = Database()
val prefs = Preferences()

class LibraApp : Application() {
	override fun onCreate() {
		super.onCreate()
		db.open(this)
		prefs.init(this)
	}
}

