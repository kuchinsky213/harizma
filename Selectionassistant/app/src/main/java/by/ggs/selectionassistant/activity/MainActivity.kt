package by.ggs.selectionassistant.activity
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import by.ggs.selectionassistant.R

import by.ggs.selectionassistant.libra.app.addFragment
import by.ggs.selectionassistant.libra.app.setFragment
import by.ggs.selectionassistant.libra.fragment.IssuesFragment
import by.ggs.selectionassistant.libra.fragment.PreferencesFragment

class MainActivity : AppCompatActivity() {
	override fun onSupportNavigateUp(): Boolean {
		val fm = supportFragmentManager
		if (fm.backStackEntryCount > 0) {
			fm.popBackStack()
		} else {
			finish()
		}
		return true
	}

	override fun onCreate(state: Bundle?) {
		super.onCreate(state)
		setContentView(R.layout.activity_main)
		initToolbar()

		if (state == null) {
			setFragment(supportFragmentManager, IssuesFragment())
			if (intent?.getBooleanExtra(OPEN_PREFERENCES, false) == true) {
				addFragment(supportFragmentManager, PreferencesFragment())
			}
		}
	}

	private fun initToolbar() {
		setSupportActionBar(toolbar)
		supportFragmentManager.addOnBackStackChangedListener {
			updateUpArrow()
		}
		updateUpArrow()
	}

	private fun updateUpArrow() {
		supportActionBar?.setDisplayHomeAsUpEnabled(
			supportFragmentManager.backStackEntryCount > 0
		)
	}

	companion object {
		const val OPEN_PREFERENCES = "open_preferences"
	}
}
