package by.ggs.selectionassistant.activity

import android.content.Intent
import android.os.Bundle
 
import androidx.appcompat.app.AppCompatActivity
import by.ggs.selectionassistant.MainActivity

class SplashActivity() : AppCompatActivity() {
	override fun onCreate(state: Bundle?) {
		super.onCreate(state)

		// it's important _not_ to inflate a layout file here
		// because that would happen after the app is fully
		// initialized

		startActivity(Intent(this, MainActivity::class.java))
		finish()
	}
}
