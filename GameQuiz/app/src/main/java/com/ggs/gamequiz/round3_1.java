package com.ggs.gamequiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

public class round3_1 extends AppCompatActivity implements MediaPlayer.OnPreparedListener {
    ImageView im17, imageView14, imageView13, imageView16, imageView15, imageView18_repeat;
    MediaPlayer mediaPlayer, baraban, gitara, dudka, repeat, violla, harder;
    Handler handler = new Handler();
    public final int DELAY = 1000; //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_round3_1);

        im17=findViewById(R.id.imageView17); //baraban verniy
        imageView14=findViewById(R.id.imageView14); //gitara
        imageView13=findViewById(R.id.imageView13); //dudka
        imageView16=findViewById(R.id.imageView16);  //truba
        imageView15=findViewById(R.id.imageView15); //vil
        imageView18_repeat=findViewById(R.id.imageView18);

        //      startService(                new Intent(this, BackgroundSoundService.class));
        mediaPlayer= MediaPlayer.create( this, R.raw.lvl3_1);
        mediaPlayer.setOnPreparedListener(this);

        harder= MediaPlayer.create( this, R.raw.harder);
        harder.setOnPreparedListener(this);

        repeat= MediaPlayer.create( this, R.raw.repeat);
        repeat.setOnPreparedListener(this);

        im17.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.pause();

                harder.start();

                nextAcrivityWithDelay();
            }
        });

        imageView14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                repeat.start();
            }
        });
        imageView16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                repeat.start();
            }
        });
        imageView15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                repeat.start();
            }
        });
        imageView13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                repeat.start();
            }
        });






        imageView18_repeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.start();
            }
        });
    }
    public void nextAcrivityWithDelay(){
        handler.postDelayed(new Runnable(){
            @Override
            public void run() {
                Intent thr = new Intent(round3_1.this, round3_2.class);
                startActivity(thr);

            }
            //your code start with delay in one second after calling this method
        }, DELAY);finish();}

    @Override
    protected void onPause() {

        //   System.runFinalizersOnExit(true);

        //  System.exit(0);
        //  finishAffinity();
        stopService(
                new Intent(this, BackgroundSoundService.class));

        super.onPause();

    }

    @Override
    protected void onResume() {

        startService(                new Intent(this, BackgroundSoundService.class));
        super.onResume();
    }
    @Override
    public void onPrepared(MediaPlayer mp) {
        mediaPlayer.start();
    }
}
