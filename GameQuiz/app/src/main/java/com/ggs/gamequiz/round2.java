package com.ggs.gamequiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

public class round2 extends AppCompatActivity implements MediaPlayer.OnPreparedListener {
    ImageView im3, im4, imageView7;
    MediaPlayer mediaPlayer, secM, ura, kek;
    boolean playedAtLeastOnce;
    Handler handler = new Handler();
   public final int DELAY = 2700; // one second
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_round2);
        im3=findViewById(R.id.imageView3);
        im4=findViewById(R.id.imageView4);
        imageView7=findViewById(R.id.imageView7);
        mediaPlayer= MediaPlayer.create( this, R.raw.lvl2);
        mediaPlayer.setOnPreparedListener(this);
        imageView7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                secM.start();
            }
        });
        secM= MediaPlayer.create( this, R.raw.lvl2_1);
        secM.setOnPreparedListener(this);

        ura= MediaPlayer.create( this, R.raw.ura);
        ura.setOnPreparedListener(this);
        //      startService(                new Intent(this, BackgroundSoundService.class));
        kek= MediaPlayer.create( this, R.raw.repeat);
        kek.setOnPreparedListener(this);

        playAudioWithDelay();


        im4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kek.start();
//                Intent first = new Intent(round2.this, round1.class);
//                startActivity(first);
            }
        });
        


        im3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ura.start();
                secM.stop();
               Intent first = new Intent(round2.this, round2_2.class);
               startActivity(first);
               finish();

            }
        });

    }

    @Override
    public void onPrepared(MediaPlayer mp) {
    mediaPlayer.start();
        playedAtLeastOnce = true;
    }






    public void playAudioWithDelay(){
        handler.postDelayed(new Runnable(){
            @Override
            public void run() {
                secM.start();
            }
            //your code start with delay in one second after calling this method
        }, DELAY);}


    @Override
    protected void onPause() {

        //   System.runFinalizersOnExit(true);

        //  System.exit(0);
        //  finishAffinity();
        stopService(
                new Intent(this, BackgroundSoundService.class));

        super.onPause();

    }

    @Override
    protected void onResume() {

        startService(                new Intent(this, BackgroundSoundService.class));
        super.onResume();
    }
        
    public boolean isPaused() {
        if (!mediaPlayer.isPlaying() && playedAtLeastOnce)
            return true;
        else
            return false;
        // or you can do it like this
        // return !mMediaPlayer.isPlaying && playedAtLeastOnce;
    }
}
