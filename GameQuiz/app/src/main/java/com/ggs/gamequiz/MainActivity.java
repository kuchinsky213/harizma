package com.ggs.gamequiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
  public   MediaPlayer mediaPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);
//---------------fonoviy zvuk

//        mediaPlayer= MediaPlayer.create( this, R.raw.fone);
//        mediaPlayer.setOnPreparedListener(this);


        Intent svc=new Intent(this, BackgroundSoundService.class);
        startService(svc);

        ImageView pachat = (ImageView)findViewById(R.id.pachat);
        pachat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent babushka = new Intent(MainActivity.this, princess.class);
                startActivity(babushka);
                finish();
            }
        });
    }

//    @Override
//    public void onStop() {
//        super.onStop();
//        mediaPlayer.pause();
//        mediaPlayer.release();
//    }
//    public void onPrepared(MediaPlayer player) {
//        // Start the player
//        player.start();
//    }
//
//    @Override
//    public void onPointerCaptureChanged(boolean hasCapture) {
//
//    }

    @Override
    protected void onPause() {

        //   System.runFinalizersOnExit(true);

        //  System.exit(0);
        //  finishAffinity();
        stopService(
                new Intent(this, BackgroundSoundService.class));

        super.onPause();

    }

    @Override
    protected void onResume() {

        startService(                new Intent(this, BackgroundSoundService.class));
        super.onResume();
    }
}
