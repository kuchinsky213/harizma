package com.ggs.gamequiz;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.widget.Toast;

public class BackgroundSoundService extends Service {
    private static final String TAG = null;
    MediaPlayer player;
    private final static int MAX_VOLUME = 50;
    public IBinder onBind(Intent arg0) {

        return null;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        player = MediaPlayer.create(this, R.raw.fone);
        player.setLooping(true); // Set looping

        player.setVolume((float) (0.10),(float) (0.10)); //set volume takes two paramater


    }
    public int onStartCommand(Intent intent, int flags, int startId) {
        player.start();
        return START_STICKY;
    }

    public void onStart(Intent intent, int startId) {
        // TO DO
    }
    public IBinder onUnBind(Intent arg0) {
        // TO DO Auto-generated method
        return null;
    }

//    public void onPause() {
//        player.pause();
//        player.release();
//        super.onPause();
//    }

    @Override
    public void onDestroy() {
        player.pause();
        player.release();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        Toast.makeText(this, "Мало оперативной памяти на устройстве!", Toast.LENGTH_SHORT).show();
    }



}

