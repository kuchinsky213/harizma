package com.ggs.gamequiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

public class princess extends AppCompatActivity implements MediaPlayer.OnPreparedListener {
ImageView strelka;
    MediaPlayer mediaPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_princess);
        strelka = findViewById(R.id.strelka);
                mediaPlayer= MediaPlayer.create( this, R.raw.princess);
        mediaPlayer.setOnPreparedListener(this);
        //     startService(                new Intent(this, BackgroundSoundService.class));



        strelka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent start = new Intent(princess.this, game.class);
                startActivity(start);
                mediaPlayer.stop();
                finish();
            }
        });

    }


    @Override
    protected void onPause() {

     //   System.runFinalizersOnExit(true);

      //  System.exit(0);
      //  finishAffinity();
        stopService(
                new Intent(princess.this, BackgroundSoundService.class));

        super.onPause();

    }

    @Override
    protected void onResume() {

        startService(                new Intent(this, BackgroundSoundService.class));
        super.onResume();
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.start();
    }
}
