package com.ggs.gamequiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

public class map7_final extends AppCompatActivity implements MediaPlayer.OnPreparedListener {
    ImageView im1, im2, im3, im4, im5, im6,close;
    MediaPlayer mediaPlayer, kon, gitara, dudka, repeat, violla, good;
    Handler handler = new Handler();
    public final int DELAY = 2500; //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_map7_final);
        im1=findViewById(R.id.imageView10); //1
        im2=findViewById(R.id.imageView11); //2
        im3=findViewById(R.id.imageView12); //3
        im4=findViewById(R.id.imageView49); //3
        im5=findViewById(R.id.imageView31); //3
        im6=findViewById(R.id.imageView40); //3
        close = findViewById(R.id.imageView64);  //
        mediaPlayer = MediaPlayer.create(this, R.raw.end);
        mediaPlayer.setOnPreparedListener(this);
        //      startService(                new Intent(this, BackgroundSoundService.class));
zalupa();
close.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        finishAffinity();
        finish();
        onDestroy();

    }
});
        im1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent first = new Intent(map7_final.this, round1.class);
                startActivity(first);
                finish();
            }
        });

        im2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sec = new Intent(map7_final.this, round2.class);
                startActivity(sec);
                finish();

            }
        });


        im3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent kek = new Intent(map7_final.this, round3_1.class);
                startActivity(kek);
                finish();
            }
        });

        im4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ss = new Intent(map7_final.this, round6_1.class);
                startActivity(ss);
                finish();
            }
        });

        im5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cock = new Intent(map7_final.this, round4_1.class);
                startActivity(cock);
                finish();
            }
        });

        im6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent uu = new Intent(map7_final.this, round5_1.class);
                startActivity(uu);
                finish();
            }
        });

    }
    public void zalupa() {
        handler.postDelayed(new Runnable(){
            @Override
            public void run() {

                mediaPlayer.start();
            }

        }, DELAY);}
    @Override
    public void onPrepared(MediaPlayer mp) {
       // mediaPlayer.start();
    }

    @Override
    protected void onPause() {

        //   System.runFinalizersOnExit(true);

        //  System.exit(0);
        //  finishAffinity();
        stopService(
                new Intent(this, BackgroundSoundService.class));

        super.onPause();

    }

    @Override
    protected void onResume() {

        startService(                new Intent(this, BackgroundSoundService.class));
        super.onResume();
    }



}
