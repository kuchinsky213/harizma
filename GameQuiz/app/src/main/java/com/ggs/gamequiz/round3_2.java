package com.ggs.gamequiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

public class round3_2 extends AppCompatActivity implements MediaPlayer.OnPreparedListener {
    ImageView im19, imageView20, imageView21, imageView22, imageView23, imageView24_repeat;
    MediaPlayer mediaPlayer, baraban, gitara, dudka, repeat, violla, harder;
    Handler handler = new Handler();
    public final int DELAY = 2000; //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_round3_2);

        //       startService(                new Intent(this, BackgroundSoundService.class));
        im19=findViewById(R.id.imageView19); //
        imageView20=findViewById(R.id.imageView20); //gitara
        imageView21=findViewById(R.id.imageView21); //pravilniy
        imageView22=findViewById(R.id.imageView22);  //truba
        imageView23=findViewById(R.id.imageView23); //vil
        imageView24_repeat=findViewById(R.id.imageView24);


        mediaPlayer= MediaPlayer.create( this, R.raw.lvl3_2);
        mediaPlayer.setOnPreparedListener(this);
        harder= MediaPlayer.create( this, R.raw.harder);
        harder.setOnPreparedListener(this);

        repeat= MediaPlayer.create( this, R.raw.repeat);
        repeat.setOnPreparedListener(this);

        imageView24_repeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.start();
            }
        });

      //  harder.start();
    //---------------verniy

        imageView21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mediaPlayer.pause();
                Intent sklad = new Intent(round3_2.this, round3_3.class);
                startActivity(sklad);
                finish();
            }
        });
        //------------------------------


        im19.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repeat.start();
            }
        });

        imageView20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repeat.start();
            }
        });


        imageView23.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repeat.start();
            }
        });


        imageView22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repeat.start();
            }
        });






    }



    @Override
    protected void onPause() {

        //   System.runFinalizersOnExit(true);

        //  System.exit(0);
        //  finishAffinity();
        stopService(
                new Intent(this, BackgroundSoundService.class));

        super.onPause();

    }

    @Override
    protected void onResume() {

        startService(                new Intent(this, BackgroundSoundService.class));
        super.onResume();
    }


    @Override
    public void onPrepared(MediaPlayer mp) {
        handler.postDelayed(new Runnable(){
            @Override
            public void run() {
                mediaPlayer.start();
            }

        }, DELAY);

    }
}
