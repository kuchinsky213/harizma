package com.ggs.gamequiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

public class game extends AppCompatActivity implements MediaPlayer.OnPreparedListener {
    MediaPlayer mediaPlayer;
    ImageView oneStart;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_game);
        mediaPlayer= MediaPlayer.create( this, R.raw.map1);
        mediaPlayer.setOnPreparedListener(this);
    //    startService(                new Intent(this, BackgroundSoundService.class));
        oneStart=findViewById(R.id.oneStart);
        oneStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent r1 = new Intent(game.this, round1.class);
                startActivity(r1);
                mediaPlayer.stop();
                finish();
            }
        });
    }




    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.start();
    }
//    @Override
//    protected void onPause() {
//
//        System.runFinalizersOnExit(true);
//
//      //  System.exit(0);
//        finishAffinity();
//
//        super.onPause();
//
//    }
    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
    @Override
    protected void onPause() {

        //   System.runFinalizersOnExit(true);

        //  System.exit(0);
        //  finishAffinity();
        stopService(
                new Intent(this, BackgroundSoundService.class));

        super.onPause();

    }

    @Override
    protected void onResume() {

        startService(                new Intent(this, BackgroundSoundService.class));
        super.onResume();
    }
}
