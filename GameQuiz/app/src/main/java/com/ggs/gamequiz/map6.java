package com.ggs.gamequiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

public class map6 extends AppCompatActivity implements MediaPlayer.OnPreparedListener {
    ImageView im1, im2, im3, im4, im5, im6;
    MediaPlayer mediaPlayer;
    Handler handler = new Handler();
    public final int DELAY = 2000; //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_map6);
        //     startService(                new Intent(this, BackgroundSoundService.class));
        mediaPlayer= MediaPlayer.create( this, R.raw.map6);
        mediaPlayer.setOnPreparedListener(this);


        im1=findViewById(R.id.imageView10); //1
        im2=findViewById(R.id.imageView11); //2
        im3=findViewById(R.id.imageView12); //3
        im4=findViewById(R.id.imageView49); //3
        im5=findViewById(R.id.imageView31); //3
        im6=findViewById(R.id.imageView40); //3




        im1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.stop();
                Intent first = new Intent(map6.this, round1.class);
                startActivity(first);
                finish();
            }
        });

        im2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.stop();
                Intent sec = new Intent(map6.this, round2.class);
                startActivity(sec);
                finish();
            }
        });


        im3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.stop();
                Intent kek = new Intent(map6.this, round3_1.class);
                startActivity(kek);
                finish();
            }
        });

        im4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.stop();
                Intent ss = new Intent(map6.this, round6_1.class);
                startActivity(ss);
                finish();
            }
        });

        im5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.stop();
                Intent cock = new Intent(map6.this, round4_1.class);
                startActivity(cock);
                finish();
            }
        });

        im6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.stop();
                Intent uu = new Intent(map6.this, round5_1.class);
                startActivity(uu);
                finish();
            }
        });

    }
    public void zalupa() {
        handler.postDelayed(new Runnable(){
            @Override
            public void run() {

                mediaPlayer.start();
            }

        }, DELAY);}
    @Override
    public void onPrepared(MediaPlayer mp) {
       // mediaPlayer.start();
        zalupa();
    }

    @Override
    protected void onPause() {

        //   System.runFinalizersOnExit(true);

        //  System.exit(0);
        //  finishAffinity();
        stopService(
                new Intent(this, BackgroundSoundService.class));

        super.onPause();

    }

    @Override
    protected void onResume() {

        startService(                new Intent(this, BackgroundSoundService.class));
        super.onResume();
    }
}
