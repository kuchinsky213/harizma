package com.ggs.gamequiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

public class round3_3 extends AppCompatActivity implements MediaPlayer.OnPreparedListener {
    ImageView im25, imageView26, imageView27, imageView28, imageView29, imageView30_repeat;
    MediaPlayer mediaPlayer, baraban, gitara, dudka, repeat, violla, good;
    Handler handler = new Handler();
    public final int DELAY = 2000; //

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_round3_3);
//        startService(                new Intent(this, BackgroundSoundService.class));

        im25 = findViewById(R.id.imageView25); //
        imageView26 = findViewById(R.id.imageView26); //gitara
        imageView27 = findViewById(R.id.imageView27); //
        imageView28 = findViewById(R.id.imageView28);  //pravilniy
        imageView29 = findViewById(R.id.imageView29); //vil
        imageView30_repeat = findViewById(R.id.imageView30);

        mediaPlayer = MediaPlayer.create(this, R.raw.lvl3_3);
        mediaPlayer.setOnPreparedListener(this);
        good = MediaPlayer.create(this, R.raw.gj);
        good.setOnPreparedListener(this);
        repeat= MediaPlayer.create( this, R.raw.repeat);
        repeat.setOnPreparedListener(this);

        imageView30_repeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.start();
            }
        });

        imageView28.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i28 = new Intent(round3_3.this, map4.class);

                mediaPlayer.pause();
                good.start();
                startActivity(i28);
                finish();
            }
        });

        im25.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repeat.start();
            }
        });


        imageView26.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repeat.start();
            }
        });

        imageView27.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repeat.start();
            }
        });

        imageView29.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repeat.start();
            }
        });


    }

    @Override
    protected void onPause() {

        //   System.runFinalizersOnExit(true);

        //  System.exit(0);
        //  finishAffinity();
        stopService(
                new Intent(this, BackgroundSoundService.class));

        super.onPause();

    }

    @Override
    protected void onResume() {

        startService(                new Intent(this, BackgroundSoundService.class));
        super.onResume();
    }
    @Override
    public void onPrepared(MediaPlayer mp) {
        handler.postDelayed(new Runnable(){
            @Override
            public void run() {
                mediaPlayer.start();
            }

        }, DELAY);

    }
}