package com.ggs.gamequiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

public class round1 extends AppCompatActivity implements MediaPlayer.OnPreparedListener {
    MediaPlayer mediaPlayer, bara, kek;
    ImageView bar_b, skr, trubka, gitara, baraban,imageView8;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_round1);
        mediaPlayer= MediaPlayer.create( this, R.raw.lvl1);
        mediaPlayer.setOnPreparedListener(this);
        bara= MediaPlayer.create( round1.this, R.raw.gj);
        bara.setOnPreparedListener(this);
        kek= MediaPlayer.create( round1.this, R.raw.repeat);
        kek.setOnPreparedListener(this);
        //      startService(                new Intent(this, BackgroundSoundService.class));

        imageView8=findViewById(R.id.imageView8);
        skr=findViewById(R.id.skr);
        trubka=findViewById(R.id.trubka);
        gitara=findViewById(R.id.gitar);
        baraban=findViewById(R.id.baraban);


        imageView8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repeat();
             mediaPlayer.start();
            }
        });

        skr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repeat();
                kek.start();
            }
        });

               trubka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repeat();
                kek.start();
            }
        });

        gitara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repeat();
                kek.start();
            }
        });



baraban.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        repeat();
        bara.start();
       // Toast.makeText(round1.this, "Молодец!", Toast.LENGTH_SHORT).show();
        Intent m2=new Intent(round1.this, map2.class);
        startActivity(m2);
        finish();
    }
});
    }
    public void repeat(){
        mediaPlayer.pause();
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mediaPlayer.start();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    protected void onPause() {

        //   System.runFinalizersOnExit(true);

        //  System.exit(0);
        //  finishAffinity();
        stopService(
                new Intent(this, BackgroundSoundService.class));

        super.onPause();

    }

    @Override
    protected void onResume() {

        startService(                new Intent(this, BackgroundSoundService.class));
        super.onResume();
    }
}
