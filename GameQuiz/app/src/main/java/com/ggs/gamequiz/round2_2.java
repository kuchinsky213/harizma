package com.ggs.gamequiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

public class round2_2 extends AppCompatActivity implements MediaPlayer.OnPreparedListener {
    ImageView im5, im6, imageView9;
    MediaPlayer mediaPlayer, secM, ura, kek;
    boolean playedAtLeastOnce;
    Handler handler = new Handler();
    public final int DELAY = 1900; //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_round2_2);


        im5=findViewById(R.id.imageView5);
        im6=findViewById(R.id.imageView6);
        imageView9=findViewById(R.id.imageView9);

        mediaPlayer= MediaPlayer.create( this, R.raw.lvl2_2);
        mediaPlayer.setOnPreparedListener(this);
        kek= MediaPlayer.create( this, R.raw.repeat);
        kek.setOnPreparedListener(this);
        ura= MediaPlayer.create( this, R.raw.ura);
        ura.setOnPreparedListener(this);
        playAudioWithDelay();
        //     startService(                new Intent(this, BackgroundSoundService.class));
        im5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kek.start();
//                Intent first = new Intent(round2.this, round1.class);
//                startActivity(first);
            }
        });       imageView9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playAudioWithDelay();
//                Intent first = new Intent(round2.this, round1.class);
//                startActivity(first);
            }
        });



        im6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ura.start();
               Intent first = new Intent(round2_2.this, map3.class);
               startActivity(first);
                mediaPlayer.stop();
                finish();
            }
        });

    }
    @Override
    protected void onPause() {

        //   System.runFinalizersOnExit(true);

        //  System.exit(0);
        //  finishAffinity();
        stopService(
                new Intent(this, BackgroundSoundService.class));

        super.onPause();

    }

    @Override
    protected void onResume() {

        startService(                new Intent(this, BackgroundSoundService.class));
        super.onResume();
    }
    public void playAudioWithDelay(){
        handler.postDelayed(new Runnable(){
            @Override
            public void run() {
                mediaPlayer.start();
            }
            //your code start with delay in one second after calling this method
        }, DELAY);}
    @Override
    public void onPrepared(MediaPlayer mp) {

    }
}
