package com.ggs.gamequiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

public class round6_3 extends AppCompatActivity implements MediaPlayer.OnPreparedListener {
    ImageView im1, im2, im3, im4, close;
    MediaPlayer mediaPlayer, kon, gitara, dudka, repeat, violla, good;
    Handler handler = new Handler();
    public final int DELAY = 3500; //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_round6_3);

        //      startService(                new Intent(this, BackgroundSoundService.class));

        im1 = findViewById(R.id.imageView61); //pravilniy
        im2 = findViewById(R.id.imageView60); //
        im3 = findViewById(R.id.imageView59);  //
        im4 = findViewById(R.id.imageView62);  //

        mediaPlayer = MediaPlayer.create(this, R.raw.lvl6_3);
        mediaPlayer.setOnPreparedListener(this);
        good = MediaPlayer.create(this, R.raw.ura);
        good.setOnPreparedListener(this);
        repeat= MediaPlayer.create( this, R.raw.repeat);
        repeat.setOnPreparedListener(this);
        kon = MediaPlayer.create(this, R.raw.lvl6);
        kon.setOnPreparedListener(this);
zalupa();


        im1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.pause();
                kon.pause();
                good.start();
                Intent f2= new Intent(round6_3.this, map7_final.class);
                startActivity(f2);
                finish();
            }
        });

        im2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                repeat.start();
            }
        });

        im3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                repeat.start();
            }
        });

        im4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // mediaPlayer.start();
                kon.start();
                zalupa();
            }
        });

    }
    public void zalupa() {
        handler.postDelayed(new Runnable(){
            @Override
            public void run() {

                mediaPlayer.start();
            }

        }, DELAY);}
    @Override
    public void onPrepared(MediaPlayer mp) {
     //   mediaPlayer.start();
    }



    @Override
    protected void onPause() {

        //   System.runFinalizersOnExit(true);

        //  System.exit(0);
        //  finishAffinity();
        stopService(
                new Intent(this, BackgroundSoundService.class));

        super.onPause();

    }

    @Override
    protected void onResume() {

        startService(                new Intent(this, BackgroundSoundService.class));
        super.onResume();
    }
}
