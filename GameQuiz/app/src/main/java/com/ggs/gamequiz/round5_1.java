package com.ggs.gamequiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

public class round5_1 extends AppCompatActivity implements MediaPlayer.OnPreparedListener {
    ImageView im1, im2, im3, im4;
    MediaPlayer mediaPlayer, kon, gitara, dudka, repeat, violla, good;
    Handler handler = new Handler();
    public final int DELAY = 5500; //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_round5_1);
        //     startService(                new Intent(this, BackgroundSoundService.class));

        im1 = findViewById(R.id.imageView41); //
        im2 = findViewById(R.id.imageView43); //
        im3 = findViewById(R.id.imageView42);  //pravilniy
        im4 = findViewById(R.id.imageView44);  //




        mediaPlayer = MediaPlayer.create(this, R.raw.lvl5_1);
        mediaPlayer.setOnPreparedListener(this);

        good = MediaPlayer.create(this, R.raw.gj);
        good.setOnPreparedListener(this);
        repeat= MediaPlayer.create( this, R.raw.repeat);
        repeat.setOnPreparedListener(this);

        im4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mediaPlayer.start();

            }
        });


        im1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                repeat.start();

            }
        });


        im2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                repeat.start();

            }
        });


        im3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.pause();
                good.start();

                Intent m5 = new Intent(round5_1.this, round5_2.class);
                startActivity(m5);

                finish();


            }
        });




    }
    @Override
    protected void onPause() {

        //   System.runFinalizersOnExit(true);

        //  System.exit(0);
        //  finishAffinity();
        stopService(
                new Intent(this, BackgroundSoundService.class));

        super.onPause();

    }

    @Override
    protected void onResume() {

        startService(                new Intent(this, BackgroundSoundService.class));
        super.onResume();
    }
    @Override
    public void onPrepared(MediaPlayer mp) {
        mediaPlayer.start();
    }
}
