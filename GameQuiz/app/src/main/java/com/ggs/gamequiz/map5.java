package com.ggs.gamequiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

public class map5 extends AppCompatActivity {
    ImageView im1, im2, im3, im4, im5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_map5);
        //  startService(                new Intent(this, BackgroundSoundService.class));

        im1=findViewById(R.id.imageView10); //1
        im2=findViewById(R.id.imageView11); //2
        im3=findViewById(R.id.imageView12); //3
        im4=findViewById(R.id.imageView40); //3
        im5=findViewById(R.id.imageView31); //3

        im1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent first = new Intent(map5.this, round1.class);
                startActivity(first);
                finish();
            }
        });

        im2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sec = new Intent(map5.this, round2.class);
                startActivity(sec);
                finish();
            }
        });


        im3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent kek = new Intent(map5.this, round3_1.class);
                startActivity(kek);
                finish();
            }
        });

        im4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cock = new Intent(map5.this, round5_1.class);
                startActivity(cock);
                finish();
            }
        });
        im5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent kek = new Intent(map5.this, round4_1.class);
                startActivity(kek);
                finish();
            }
        });
    }
    @Override
    protected void onPause() {

        //   System.runFinalizersOnExit(true);

        //  System.exit(0);
        //  finishAffinity();
        stopService(
                new Intent(this, BackgroundSoundService.class));

        super.onPause();

    }

    @Override
    protected void onResume() {

        startService(                new Intent(this, BackgroundSoundService.class));
        super.onResume();
    }
}
