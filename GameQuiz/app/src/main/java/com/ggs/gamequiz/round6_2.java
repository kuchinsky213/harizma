package com.ggs.gamequiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

public class round6_2 extends AppCompatActivity implements MediaPlayer.OnPreparedListener {
    ImageView im1, im2, im3, im4;
    MediaPlayer mediaPlayer, kon, gitara, dudka, repeat, violla, good;
    Handler handler = new Handler();
    public final int DELAY = 6500; //
    public final int DELAY2 = 3500; //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_round6_2);

//        startService(                new Intent(this, BackgroundSoundService.class));
        im1 = findViewById(R.id.imageView56); //pravilniy
        im2 = findViewById(R.id.imageView55); //
        im3 = findViewById(R.id.imageView54);  //
        im4 = findViewById(R.id.imageView57);  //
        mediaPlayer = MediaPlayer.create(this, R.raw.lvl6_2);
        mediaPlayer.setOnPreparedListener(this);

        good = MediaPlayer.create(this, R.raw.harder);
        good.setOnPreparedListener(this);
        repeat= MediaPlayer.create( this, R.raw.repeat);
        repeat.setOnPreparedListener(this);
        kon = MediaPlayer.create(this, R.raw.lvl6);
        kon.setOnPreparedListener(this);
zalupa2();
        im1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.pause();
                kon.pause();
                good.start();
                Intent f2= new Intent(round6_2.this, round6_3.class);
                startActivity(f2);
                finish();
            }
        });

        im2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                repeat.start();
            }
        });

        im3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                repeat.start();
            }
        });

        im4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // mediaPlayer.start();
                kon.start();
                zalupa();
            }
        });


    }
    public void zalupa() {
        handler.postDelayed(new Runnable(){
            @Override
            public void run() {

                mediaPlayer.start();
            }

        }, DELAY);}

    public void zalupa2() {
        handler.postDelayed(new Runnable(){
            @Override
            public void run() {

                mediaPlayer.start();
            }

        }, DELAY2);}
    @Override
    public void onPrepared(MediaPlayer mp) {
       // mediaPlayer.start();
    }
    @Override
    protected void onPause() {

        //   System.runFinalizersOnExit(true);

        //  System.exit(0);
        //  finishAffinity();
        stopService(
                new Intent(this, BackgroundSoundService.class));

        super.onPause();

    }

    @Override
    protected void onResume() {

        startService(                new Intent(this, BackgroundSoundService.class));
        super.onResume();
    }
}
