package com.ggs.crol;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {
    private TabLayout tabLayout;
    private AppBarLayout appBarLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tabLayout=(TabLayout) findViewById(R.id.tablayout_id);
        appBarLayout=findViewById(R.id.appbarid);
        viewPager=findViewById(R.id.viewpager_id);

        //----init+add fragmenttt
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.AddFragment(new Opisanie(), getString(R.string.zo));
        adapter.AddFragment(new Klassificia(), getString(R.string.zk));
        adapter.AddFragment(new Liubitelsskie(), getString(R.string.zpdr));


//----------- start adaptera

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);










    }
}
