package com.ggs.crol;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Liubitelsskie#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Liubitelsskie extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    RecyclerView NewsRecyclerview;
    NewsAdapter newsAdapter;
    List<NewsItem> mData;
    FloatingActionButton fabSwitcher;
    boolean isDark = false;
    ConstraintLayout rootLayout;
    LinearLayout firstLayout;
    EditText searchInput ;
    CharSequence search="";

    View view;

    public Liubitelsskie() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Liubitelsskie.
     */
    // TODO: Rename and change types and number of parameters
    public static Liubitelsskie newInstance(String param1, String param2) {
        Liubitelsskie fragment = new Liubitelsskie();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }







    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_liubitelsskie, container, false);



        fabSwitcher = view.findViewById(R.id.fab_switcher);
        rootLayout = view.findViewById(R.id.root_layout);
        firstLayout = view.findViewById(R.id.kek);
        searchInput = view.findViewById(R.id.search_input);
        NewsRecyclerview = view.findViewById(R.id.news_rv);
        mData = new ArrayList<>();
       // fabSwitcher.setVisibility(View.INVISIBLE);
        isDark = getThemeStatePref();
        if(isDark) {
            // dark theme is on

            searchInput.setBackgroundResource(R.drawable.search_input_dark_style);
          rootLayout.setBackgroundColor(getResources().getColor(R.color.black));

        }
        else
        {
            // light theme is on
            searchInput.setBackgroundResource(R.drawable.search_input_style);
            rootLayout.setBackgroundColor(getResources().getColor(R.color.white));

        }









        mData.add(new NewsItem(getString(R.string.b),getString(R.string.mp),R.drawable.bab,R.drawable.krol,getString(R.string.fds) +
                 getString(R.string.kmyfuiloky)+

                getString(R.string.hds) +
                getString(R.string.fhbgf) +
                getString(R.string.dfs) +
               getString(R.string.fdssa)+
"\n"+
                getString(R.string.fs)






                ));
        mData.add(new NewsItem(getString(R.string.dd),"Hollander ",R.drawable.holand,R.drawable.krol,

                getString(R.string.sdf) +
                        getString(R.string.ghj) +
                        "\n" +
                        "\n"+
                        getString(R.string.khf)






        ));

        mData.add(new NewsItem(getString(R.string.rhc),getString(R.string.hc),R.drawable.him,R.drawable.krol,
                getString(R.string.vcx) +
                getString(R.string.cz) +
                getString(R.string.czz) +
                 "\n"+
                getString(R.string.zxc)






        ));

        mData.add(new NewsItem(getString(R.string.vcs),getString(R.string.dsfs),R.drawable.chog,R.drawable.krol,

                getString(R.string.jgd) +
                getString(R.string.f) +
                getString(R.string.s) +
                getString(R.string.fdsa) +
                getString(R.string.vb)+
                "\n"+
                getString(R.string.h)+
                getString(R.string.gg)

               + getString(R.string.u)  + getString(R.string.sss)



        ));


        mData.add(new NewsItem(getString(R.string.tu),getString(R.string.vx),R.drawable.tyur,R.drawable.krol,

                getString(R.string.vbbxx) +
                        getString(R.string.czcx)+
                        getString(R.string.ko) +
                        getString(R.string.fsz) +
                        getString(R.string.czzz) +"\n"+
                        getString(R.string.m1)+
                        getString(R.string.n4rw)



        ));

        mData.add(new NewsItem(getString(R.string.angl),getString(R.string.anhghdj),R.drawable.angl,R.drawable.krol,

                getString(R.string.the) +
                        getString(R.string.hid)+
                        getString(R.string.gsdf) +
                        getString(R.string.jgfd) +
                        getString(R.string.opis) +"\n"+
                        getString(R.string.thissss)+
                        getString(R.string.jkfd)+
                        getString(R.string.oj)+
                        getString(R.string.jgcfd)
                       + getString(R.string.gd)
                       + getString(R.string.jhds)
                       + getString(R.string.ras)
                       + getString(R.string.gas)



        ));
        mData.add(new NewsItem(getString(R.string.gfsh),"Ankara tavşanı",R.drawable.angor,R.drawable.krol,

                getString(R.string.xzz) +
                getString(R.string.hjgd) +
                        getString(R.string.ip)+
                        getString(R.string.bcvsxz) +
                        getString(R.string.gnfxng) +
                        getString(R.string.bfx) +"\n"+
                        getString(R.string.gnhfsn)+
                        getString(R.string.d)+
                        getString(R.string.as) +
                        getString(R.string.gbfd) +
                        getString(R.string.dfa) +
                        getString(R.string.dfsfd) +
                        getString(R.string.hjkyt) +
                        getString(R.string.jkt)+
                        getString(R.string.hgds)
                       + getString(R.string.gfrhs)
                       + getString(R.string.lkyfr)
                       + getString(R.string.mf)
                       + getString(R.string.bnsd)



        ));

        mData.add(new NewsItem(getString(R.string.alyaska),getString(R.string.gjdt),R.drawable.alyaska,R.drawable.krol,

                getString(R.string.gbsd)+

                        getString(R.string.gds) +
                        getString(R.string.gesda) +
                        getString(R.string.lguhj) +
                        getString(R.string.sa) +
                        getString(R.string.ass)+
                        getString(R.string.ghrs) +
                         getString(R.string.njhdtjk) +"\n"+
                        getString(R.string.hjsd)+
                        getString(R.string.lgf)+
getString(R.string.meh)+
                        getString(R.string.sgbserda)

                        + getString(R.string.lug)



        ));

        mData.add(new NewsItem(getString(R.string.hgjkjk),getString(R.string.gfjfy),R.drawable.belpuh,R.drawable.krol,

                "История\n\n"+

                        getString(R.string.ghfs) +
                        getString(R.string.ty) +
                        getString(R.string.jkmdt) +
                        getString(R.string.fgvbda) +
                        getString(R.string.dbz)+
                        getString(R.string.bfs) +
                        getString(R.string.fgf) +
                        getString(R.string.puh) +"\n"+
                        getString(R.string.fsf)+
                        getString(R.string.hfs)+
                        getString(R.string.kyf)+
                        getString(R.string.poi)

                        + getString(R.string.ljhg)
                        + getString(R.string.ghg)



        ));

        mData.add(new NewsItem(getString(R.string.bz),getString(R.string.bzz),R.drawable.belgzay,R.drawable.krol,

                getString(R.string.pr)+


                        getString(R.string.gda) +
                        getString(R.string.dfsa) +
                        getString(R.string.gshb)



        ));

        mData.add(new NewsItem(getString(R.string.berl),getString(R.string.hnsnfhgs),R.drawable.belvelik,R.drawable.krol,

                getString(R.string.hnrs)+
                getString(R.string.bgasr)+
                getString(R.string.b56)+
                getString(R.string.jye56)+
                getString(R.string.brt)+

                        getString(R.string.bn5yw) +
                        getString(R.string.hn56)+
                        getString(R.string.cqq2) +
                        getString(R.string.gew3r6t) +"\n"+
                        getString(R.string.b345)+
                        getString(R.string.g345q)+
                        getString(R.string.g54w)+
                        getString(R.string.h4qw)

                        + getString(R.string.g34q)
                        + getString(R.string.gh457)
                        + getString(R.string.bh4wtyu)



        ));

        mData.add(new NewsItem(getString(R.string.sv),getString(R.string.gbed),R.drawable.seryvelik,R.drawable.krol,

                getString(R.string.nhdt)+
                        getString(R.string.nbsfrj)+
                        getString(R.string.basrg)+
                        getString(R.string.khdgk)+
                        getString(R.string.vbaerthj)+
                        getString(R.string.rtwsy)+
                        getString(R.string.vbdfa)+
                        getString(R.string.bngs)+
                        getString(R.string.nmhdtgk)+
                        getString(R.string.ns)+
                        getString(R.string.hfk) +
                        getString(R.string.hlgkcfd)+
                        getString(R.string.hgkdg) +
                        getString(R.string.shgj)+
                        getString(R.string.bea5) +
                        getString(R.string.n5rw6)+

                        getString(R.string.bea53)+
                        getString(R.string.fw3e)+
                        getString(R.string.k67t)+
                        getString(R.string.n56e)+
                        getString(R.string.rw67)+
                        getString(R.string.h4q)+



                        getString(R.string.n65) +"\n"+
                        getString(R.string.n56ew)+
                        getString(R.string.jk687r)+
                        getString(R.string.bv3q4)+
                        getString(R.string.y5w)

                        + getString(R.string.m6er7)+
                getString(R.string.n67e) +"\n"+
                        getString(R.string.mj87r)
                        + getString(R.string.nw5w)










        ));


        mData.add(new NewsItem(getString(R.string.hrws),getString(R.string.hgaret),R.drawable.vensky,R.drawable.krol,
getString(R.string.brsa)+
                getString(R.string.jdst)+
                        getString(R.string.ntds)+
                        getString(R.string.njythdki)+
                        getString(R.string.bsraj)+
                        getString(R.string.nsytjkdlk)+
                        getString(R.string.ndts)+
                        getString(R.string.bahjns) +
        getString(R.string.mdghx)+
        getString(R.string.nsfjhdgk)+
        getString(R.string.nghfkjhl)+


                        getString(R.string.hgfok)+
                        getString(R.string.jhplgf) +
                        getString(R.string.gjfxh) +
                        getString(R.string.ghjncfgxd) +
                        getString(R.string.ghjfsdhb) +
                        getString(R.string.gngsjhjg) +
                        getString(R.string.ndtkm)+
                        getString(R.string.gfjfsdxhg) +
                        getString(R.string.gnbvfczx)




        ));




        mData.add(new NewsItem(getString(R.string.hab),getString(R.string.hgrbas),R.drawable.havan,R.drawable.krol,
                getString(R.string.nbrstj)+
                        getString(R.string.b4r56)+
                        getString(R.string.brstgahjngf)+

                        getString(R.string.nt5)+
                        getString(R.string.gber5) +
                        getString(R.string.vfqeq2) +
                        getString(R.string.vb34)+
                        getString(R.string.gb3e5)+
                        getString(R.string.g354y)

        ));


        mData.add(new NewsItem(getString(R.string.nyt5re6),getString(R.string.ntyy),R.drawable.novozel,R.drawable.krol,
                getString(R.string.nm6te7)+
                        getString(R.string.n5tye)+
                        getString(R.string.ny5e)+
                        getString(R.string.j76e4r)


        ));


        mData.add(new NewsItem(getString(R.string.n55),getString(R.string.brtswe4),R.drawable.krasny,R.drawable.krol,
                getString(R.string.ver54)+
                        getString(R.string.b4r)+
                        getString(R.string.b4e5)+
                        getString(R.string.b45)+
                        getString(R.string.m87)+
                        getString(R.string.jm6e7)+
                        getString(R.string.v3s)+
                        getString(R.string.kg35) +
                        getString(R.string.hgertw)+
                       getString(R.string.kl76)+


                        getString(R.string.bvasd)+
                        getString(R.string.bhsrjmhg) +
                         getString(R.string.bsrdga)+
                        getString(R.string.bsarjn) +
                        getString(R.string.vead)+
                        getString(R.string.bsrgnj)+
                        getString(R.string.bns)+
                        getString(R.string.gabe)+
                        getString(R.string.gbahn)+
                        getString(R.string.vbaeh)+
                        getString(R.string.bgfaghj)+
                        getString(R.string.vcsgb)+
                        getString(R.string.bsnb)


        ));

        mData.add(new NewsItem(getString(R.string.jhdt),getString(R.string.njhtd),R.drawable.palomino,R.drawable.krol,
                getString(R.string.nbgfshjn)+
                        getString(R.string.vedfa)+
                        getString(R.string.fhda)+
                        getString(R.string.bvdas)+
                        getString(R.string.jmhytdik)



        ));

        mData.add(new NewsItem(getString(R.string.siolv),getString(R.string.k7),R.drawable.serebristy,R.drawable.krol,
                getString(R.string.nye5w)+
                        getString(R.string.n75e)+
                        getString(R.string.j64e7)+
                        getString(R.string.nmuert)+
                        getString(R.string.nmj6uer)+
                        getString(R.string.jre67)+
                        getString(R.string.nje5)+
                        getString(R.string.nm6ure)+
                        getString(R.string.ne5wy6) +
                        getString(R.string.je67) +
                        getString(R.string.b56yew6u) +
                        getString(R.string.nbye5w6) +
                        getString(R.string.nuyoikf)+
                        getString(R.string.mkujyfdoi)+


                        getString(R.string.nhgds)+
                        getString(R.string.nmhtd7u) +
                        getString(R.string.hj5re6t)


        ));




        mData.add(new NewsItem(getString(R.string.ssh),getString(R.string.ssh2),R.drawable.shinshilla,R.drawable.krol,
                getString(R.string.h675)+
                        getString(R.string.j64)+
                        getString(R.string.jk6ur)+
                        getString(R.string.k78t)+
                        getString(R.string.sdfs)+
                        getString(R.string.ghsr)+
                        getString(R.string.khdt) +
                          getString(R.string.fsa) +
                        getString(R.string.ьыр)+
                        getString(R.string.jdg) +
                        getString(R.string.jghd)+


                        getString(R.string.nbnb)+
                        getString(R.string.dfsavc) +
                        getString(R.string.khj)
                      +  getString(R.string.hjtd)
                      +  getString(R.string.vdea)
                      +  getString(R.string.lugfc)
                      +  getString(R.string.vedadj)


        ));



        mData.add(new NewsItem(getString(R.string.ioi),getString(R.string.jgdd),R.drawable.flandr,R.drawable.krol,
                getString(R.string.gdsd)+
                        getString(R.string.vcc)+
                        getString(R.string.c)+
                        getString(R.string.saf)+
                        getString(R.string.lh)+
                        getString(R.string.hjg)+
                        getString(R.string.ger)+
                        getString(R.string.fgh)+
                        getString(R.string.vde)+
                        getString(R.string.fdsaa)+
                        getString(R.string.z) +
                        getString(R.string.fcsz) +
                        getString(R.string.ds) +
                        getString(R.string.ghd)+
                        getString(R.string.fsfsd) +



                        getString(R.string.jh) +
                        getString(R.string.hgd)
                         +  getString(R.string.kjhygfd)


        ));


        mData.add(new NewsItem(getString(R.string.rt),getString(R.string.ui),R.drawable.hotot,R.drawable.krol,
                getString(R.string.kj)+
                        getString(R.string.tr)+
                        getString(R.string.rte)+
                        getString(R.string.hfgedrsa)+
                        getString(R.string.khjf)+
                        getString(R.string.kfd)+
                        getString(R.string.lofdc)+
                        getString(R.string.lfd)
        ));


        mData.add(new NewsItem(getString(R.string.pop),getString(R.string.blan),R.drawable.blankhotot,R.drawable.krol,
                getString(R.string.uyu)+
                        getString(R.string.ljgf)+
                        getString(R.string.ligfh)+
                        getString(R.string.o9)+
                        getString(R.string.hfg)+
                        getString(R.string.lksd)+
                        getString(R.string.hfsdg)

        ));

        mData.add(new NewsItem(getString(R.string.sdgfs),getString(R.string.lk),R.drawable.chernobury,R.drawable.krol,
                getString(R.string.vcxz)+
                        getString(R.string.ik)+
                        getString(R.string.hsgr)+
                        getString(R.string.hns)+
                        getString(R.string.hd)+
                        getString(R.string.sd)+
                        getString(R.string.opisas)+
                        getString(R.string.sm)+
                        getString(R.string.uu)+
                      "\n"+
                      getString(R.string.l)+
                      getString(R.string.p)+
                      getString(R.string.op)

        ));


        // adapter ini and setup

        newsAdapter = new NewsAdapter(getContext(),mData,isDark);
        NewsRecyclerview.setAdapter(newsAdapter);
        NewsRecyclerview.setLayoutManager(new LinearLayoutManager(getContext()));


        fabSwitcher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isDark = !isDark ;
                if (isDark) {

                    rootLayout.setBackgroundColor(getResources().getColor(R.color.black));

                    searchInput.setBackgroundResource(R.drawable.search_input_dark_style);

                }
                else {
                    rootLayout.setBackgroundColor(getResources().getColor(R.color.white));

                    searchInput.setBackgroundResource(R.drawable.search_input_style);
                }

                newsAdapter = new NewsAdapter(getContext(),mData,isDark);
                if (!search.toString().isEmpty()){

                    newsAdapter.getFilter().filter(search);

                }
                NewsRecyclerview.setAdapter(newsAdapter);
                saveThemeStatePref(isDark);




            }
        });















        searchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                newsAdapter.getFilter().filter(s);
                search = s;


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });






        return view;
    }

    private void saveThemeStatePref(boolean isDark) {

        SharedPreferences pref = getActivity().getSharedPreferences("myPref",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("isDark",isDark);
        editor.commit();
    }



    private boolean getThemeStatePref () {

        SharedPreferences pref = getActivity().getSharedPreferences("myPref",MODE_PRIVATE);
        boolean isDark = pref.getBoolean("isDark",false) ;
        return isDark;

    }


}
