package com.ggs.magicpencil.act;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.ggs.magicpencil.MainActivity;
import com.ggs.magicpencil.R;

public class WhatCollors extends AppCompatActivity {
    View vector;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_what_collors);
        vector=findViewById(R.id.vector2);
        vector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent back = new Intent(WhatCollors.this, MainActivity.class);
                startActivity(back);
              //  finish();
            }
        });
    }
}