package com.ggs.marafonJelaniy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
Button pristupit;
    Animation butAnim;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        pristupit = findViewById(R.id.pristupit);

        butAnim = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.uptodown);
        pristupit.startAnimation(butAnim);

        pristupit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pristupit = new Intent(MainActivity.this, Pristupit.class);
                startActivity(pristupit);
            }
        });
    }
}
