package com.ggs.marafonJelaniy;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class SplashScreen extends AppCompatActivity {
    private final int SPLASH_DISPLAY_LENGHT = 4500;
    ImageView logo;
    Animation animFadeIn,animFadeOut,animBlink,animZoomIn,animZoomOut,animRotate
            ,animMove,animSlideUp,animSlideDown,animBounce,animSequential,animTogether,animCrossFadeIn,animCrossFadeOut;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);
        logo=findViewById(R.id.logo);
        animCrossFadeIn = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.rotate);
        logo.startAnimation(animCrossFadeIn);
        new Handler().postDelayed(new Runnable() {

            @Override

            public void run() {

                Intent mainIntent = new Intent(SplashScreen.this, MainActivity.class);

                SplashScreen.this.startActivity(mainIntent);

                SplashScreen.this.finish();

            }

        }, SPLASH_DISPLAY_LENGHT);

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}