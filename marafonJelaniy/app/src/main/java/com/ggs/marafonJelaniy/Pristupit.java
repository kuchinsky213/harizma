package com.ggs.marafonJelaniy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
public class Pristupit extends AppCompatActivity {
public static final String SHARED_PREFERENCE = "FILE";
String e1=" ", e2=" ", e3=" ", e4=" ", e5=" ", e6=" ", e7=" ";
EditText edit1, edit2, edit3, edit4, edit5, edit6,edit7;
    Button button, test;
    SharedPreferences sharedPreferences;
    ImageView info;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
            setContentView(R.layout.activity_pristupit);

        info=findViewById(R.id.info);
        button= findViewById(R.id.button);
        test= findViewById(R.id.test);

      edit1 = findViewById(R.id.editText);
      edit2 = findViewById(R.id.editText2);
      edit3 = findViewById(R.id.editText3);
      edit4 = findViewById(R.id.editText4);
      edit5 = findViewById(R.id.editText5);
      edit6 = findViewById(R.id.editText6);
      edit7 = findViewById(R.id.editText7);

        sharedPreferences = getSharedPreferences(SHARED_PREFERENCE, MODE_PRIVATE);



        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {}
        });

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-7802337876801234/5091188460");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
              //  mInterstitialAd.show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                if (!mInterstitialAd.isLoading() && !mInterstitialAd.isLoaded()) {
                    AdRequest adRequest = new AdRequest.Builder().build();
                    mInterstitialAd.loadAd(adRequest);
                }
            }

            @Override
            public void onAdClosed() {
                if (!mInterstitialAd.isLoading() && !mInterstitialAd.isLoaded()) {
                    AdRequest adRequest = new AdRequest.Builder().build();
                    mInterstitialAd.loadAd(adRequest);}
            }
        });















test.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        showInterstitial();
        retrieveData();
    }
});


info.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent info = new Intent(Pristupit.this, Info.class);
        startActivity(info);

    }
});


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
saveData();

                showInterstitial();

            }
        });




}

    private void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }  }
public void saveData(){
    sharedPreferences = getSharedPreferences(SHARED_PREFERENCE, MODE_PRIVATE);
SharedPreferences.Editor editor = sharedPreferences.edit();
editor.putString("e1", edit1.getText().toString());
editor.putString("e2", edit2.getText().toString());
editor.putString("e3", edit3.getText().toString());
editor.putString("e4", edit4.getText().toString());
editor.putString("e5", edit5.getText().toString());
editor.putString("e6", edit6.getText().toString());
editor.putString("e7", edit7.getText().toString());
editor.apply();


    Toast.makeText(Pristupit.this, R.string.bloknot, Toast.LENGTH_SHORT).show();
}

public void retrieveData(){
   sharedPreferences = getSharedPreferences(SHARED_PREFERENCE, MODE_PRIVATE);
     edit1.setText(sharedPreferences.getString("e1", ""));
     edit2.setText(sharedPreferences.getString("e2", ""));
     edit3.setText(sharedPreferences.getString("e3", ""));
     edit4.setText(sharedPreferences.getString("e4", ""));
     edit5.setText(sharedPreferences.getString("e5", ""));
     edit6.setText(sharedPreferences.getString("e6", ""));
     edit7.setText(sharedPreferences.getString("e7", ""));
}

  }