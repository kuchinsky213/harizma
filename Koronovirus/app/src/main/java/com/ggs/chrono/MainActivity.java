package com.ggs.chrono;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private AppBarLayout appBarLayout;
    private ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        ///---------------------------------------pios

        tabLayout=(TabLayout) findViewById(R.id.tablayout_id);
        appBarLayout=findViewById(R.id.appbarid);
        viewPager=findViewById(R.id.viewpager_id);

        //----init+add fragmenttt
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.AddFragment(new FragmentKoronovirusi(), getString(R.string.krono_fr));
        adapter.AddFragment(new FragmentTypes(), getString(R.string.vidi_fr));
        adapter.AddFragment(new FragmentDeffend(), getString(R.string.defebd_fr));

//----------- start adaptera

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);










    }
}
