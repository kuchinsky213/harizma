package com.ggs.chrono;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class NewsAdapter  extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder> implements Filterable {

 //   private Context mContext ;
 //   private List<Book> mData ;



    Context mContext;
    List<NewsItem> mData ;
    List<NewsItem> mDataFiltered ;
    boolean isDark = false;


    public NewsAdapter(Context mContext, List<NewsItem> mData, boolean isDark) {
        this.mContext = mContext;
        this.mData = mData;
        this.isDark = isDark;
        this.mDataFiltered = mData;
    }

    public NewsAdapter(Context mContext, List<NewsItem> mData) {
        this.mContext = mContext;
        this.mData = mData;
        this.mDataFiltered = mData;

    }

    @NonNull
    @Override
    public NewsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View layout;
        layout = LayoutInflater.from(mContext).inflate(R.layout.item_news,viewGroup,false);
        return new NewsViewHolder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewHolder newsViewHolder, final int position) {

        // first lets create an animation for user photo
        newsViewHolder.img_user.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_transition_animation));
        newsViewHolder.container.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_scale_animation));
        newsViewHolder.tv_title.setText(mDataFiltered.get(position).getTitle());
        newsViewHolder.tv_content.setText(mDataFiltered.get(position).getContent());
        newsViewHolder.vkladka.setText(mDataFiltered.get(position).getVkladka());





        newsViewHolder.img_user.setImageResource(mDataFiltered.get(position).getUserPhoto());

        newsViewHolder.tv_title.setText(mData.get(position).getTitle());
        newsViewHolder.img_user.setImageResource(mData.get(position).getThumbnail());






        newsViewHolder.tv_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext,virus.class);

                // passing data to the book activity
                intent.putExtra("Title",mData.get(position).getTitle());

                intent.putExtra("Thumbnail",mData.get(position).getUserPhoto());

                intent.putExtra("Description",mData.get(position).getVkladka());
                // start the activity
                mContext.startActivity(intent);

            }
        });










        newsViewHolder.tv_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext,virus.class);

                // passing data to the book activity
                intent.putExtra("Title",mData.get(position).getTitle());

                intent.putExtra("Thumbnail",mData.get(position).getUserPhoto());

                intent.putExtra("Description",mData.get(position).getVkladka());
                // start the activity
                mContext.startActivity(intent);

            }
        });



        newsViewHolder.img_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext,virus.class);

                // passing data to the book activity
                intent.putExtra("Title",mData.get(position).getTitle());

                intent.putExtra("Thumbnail",mData.get(position).getUserPhoto());

                intent.putExtra("Description",mData.get(position).getVkladka());
                // start the activity
                mContext.startActivity(intent);

            }
        });












    }

    @Override
    public int getItemCount() {
        return mDataFiltered.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                String Key = constraint.toString();
                if (Key.isEmpty()) {

                    mDataFiltered = mData ;

                }
                else {
                    List<NewsItem> lstFiltered = new ArrayList<>();
                    for (NewsItem row : mData) {

                        if (row.getTitle().toLowerCase().contains(Key.toLowerCase())){
                            lstFiltered.add(row);
                        }

                    }

                    mDataFiltered = lstFiltered;

                }


                FilterResults filterResults = new FilterResults();
                filterResults.values= mDataFiltered;
                return filterResults;

            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {


                mDataFiltered = (List<NewsItem>) results.values;
                notifyDataSetChanged();

            }
        };




    }

    public class NewsViewHolder extends RecyclerView.ViewHolder {



        TextView tv_title,tv_content,tv_date, vkladka;
        ImageView img_user;
        RelativeLayout container;





        public NewsViewHolder(@NonNull View itemView) {
            super(itemView);

            vkladka = itemView.findViewById(R.id.vkladka);

            container = itemView.findViewById(R.id.container);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_content = itemView.findViewById(R.id.tv_description);

            img_user = itemView.findViewById(R.id.img_user);


            if (isDark) {
                setDarkTheme();
            }



        }


        private void setDarkTheme() {

            container.setBackgroundResource(R.drawable.card_bg_dark);

        }



    }
}
