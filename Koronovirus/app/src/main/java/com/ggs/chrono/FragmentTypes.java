package com.ggs.chrono;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class FragmentTypes extends Fragment {
    RecyclerView NewsRecyclerview;
    NewsAdapter newsAdapter;
    List<NewsItem> mData;
    FloatingActionButton fabSwitcher;
    boolean isDark = false;
    ConstraintLayout rootLayout;
    LinearLayout firstLayout;
    EditText searchInput ;
    CharSequence search="";

    View view;
    public FragmentTypes() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.types_fr,container,false);





        // ini view

        fabSwitcher = view.findViewById(R.id.fab_switcher);
        rootLayout = view.findViewById(R.id.root_layout);
        firstLayout = view.findViewById(R.id.list_korona);
        searchInput = view.findViewById(R.id.search_input);
        NewsRecyclerview = view.findViewById(R.id.news_rv);
        mData = new ArrayList<>();


        // vikl v dannom pril fab

       fabSwitcher.setVisibility(View.INVISIBLE);

        // load theme state

        isDark = getThemeStatePref();
        if(isDark) {
            // dark theme is on

            searchInput.setBackgroundResource(R.drawable.search_input_dark_style);
            rootLayout.setBackgroundColor(getResources().getColor(R.color.black));

        }
        else
        {
            // light theme is on
            searchInput.setBackgroundResource(R.drawable.search_input_style);
            rootLayout.setBackgroundColor(getResources().getColor(R.color.white));

        }




        mData.add(new NewsItem("COVID-19","COrona VIrus Disease 2019",R.drawable.ncov,R.drawable.virus,getString(R.string.n) +
                getString(R.string.n1) +
                getString(R.string.n2) +
                getString(R.string.n3) +
                getString(R.string.n4)));
        mData.add(new NewsItem("Bat SARS-like coronavirus RsSHC014","RsSHC014",R.drawable.sars,R.drawable.virus,getString(R.string.b1) +
                               getString(R.string.b2)));
        mData.add(new NewsItem("Severe acute respiratory syndrome-related coronavirus (SARS-CoV)","Sarbecovirus",R.drawable.sss,R.drawable.virus,getString(R.string.b3) +
                getString(R.string.b4) +
                getString(R.string.b5)));
        mData.add(new NewsItem("Microhyla letovirus","Milecovirus",R.drawable.virus,R.drawable.virus,"Regnum: Virus\n" +
                "Group IV: ssRNA(+)\n" +
                "Ordo: Nidovirales\n" +
                "Familia: Coronaviridae\n" +
                "Subfamilia: Letovirinae\n" +
                "Genus: Alphaletovirus\n" +
                "Subgenus: Milecovirus\n" +
                "Species: Microhyla letovirus 1"));
        mData.add(new NewsItem("Bat coronavirus CDPHE15","Colacovirus",R.drawable.bc,R.drawable.virus,"Regnum: Virus\n" +
                "Group IV: ssRNA(+)\n" +
                "Ordo: Nidovirales\n" +
                "Familia: Coronaviridae\n" +
                "Subfamilia: Coronavirinae\n" +
                "Genus: Alphacoronavirus\n" +
                "Species: Bat coronavirus CDPHE15"));
        mData.add(new NewsItem("Bat coronavirus HKU10","Decacovirus",R.drawable.sm,R.drawable.virus,"NCBI BLAST name: viruses\n" +
                "Rank: species\n" +
                "Genetic code: Translation table 1 (Standard)\n" +
                "Host: vertebrates\n" +
                "Lineage( full )\n" +
                "Viruses; Riboviria; Nidovirales; Cornidovirineae; Coronaviridae; Orthocoronavirinae; Alphacoronavirus; Decacovirus\n" +
                "Nucleotide 9\n" +
                "Protein 79\n" +
                "Genome 1\n" +
                "PubMed Central t9\n" +
                "Gene 19\n" +
                "Identical Protein Groups 46 \n" +
                "Assembly 1 \n" +
                "Taxonomy 3 \n" +
                "Notes:\n" +
                "  Name is currently accepted by the International Committee on Taxonomy of Viruses"));
        mData.add(new NewsItem("Rhinolophus ferrumequinum alphacoronavirus HuB-2013","Decacovirus",R.drawable.rancho,R.drawable.virus,getString(R.string.s)));
        mData.add(new NewsItem("Human coronavirus 229E","Duvinacovirus",R.drawable.virus,R.drawable.virus,getString(R.string.a) +    getString(R.string.a1)));
        mData.add(new NewsItem("Lucheng Rn rat coronavirus","Luchacovirus",R.drawable.nop,R.drawable.virus,"Assembly level: Complete Genome\n" +
                "Assembly: GCA_001962315.1 ViralProj361952 scaffolds: 1 contigs: 1 N50: 0 L50: 0\n" +
                "Statistics: total length (Mb): 0.028763\n" +
                " protein count: 5\n" +
                " GC%: 40.2"));
        mData.add(new NewsItem("Ferret coronavirus","Minacovirus",R.drawable.ferret,R.drawable.virus,getString(R.string.f) +
                getString(R.string.f1)));
        mData.add(new NewsItem("Mink coronavirus 1","Minacovirus",R.drawable.nop,R.drawable.virus,"Regnum: Virus\n" +
                "Group IV: ssRNA(+)\n" +
                "Ordo: Nidovirales\n" +
                "Familia: Coronaviridae\n" +
                "Subfamilia: Coronavirinae\n" +
                "Genus: Alphacoronavirus\n" +
                "Species: Mink coronavirus 1"));
        mData.add(new NewsItem("Miniopterus bat coronavirus 1","Minunacovirus",R.drawable.coron1,R.drawable.virus,getString(R.string.as)));
        mData.add(new NewsItem("Miniopterus bat coronavirus HKU8","Minunacovirus",R.drawable.sm,R.drawable.virus,getString(R.string.h)));
        mData.add(new NewsItem("Myotis ricketti alphacoronavirus Sax-2011","Myotacovirus",R.drawable.virus,R.drawable.virus,getString(R.string.u)));
        mData.add(new NewsItem("Nyctalus velutinus alphacoronavirus SC-2013","Nyctacovirus",R.drawable.bc,R.drawable.virus,"taxon name\n" +
                "Nyctalus velutinus alphacoronavirus SC-2013\n"+ "taxon rank\n" +
                "species\n" +"ICTV virus genome composition\n" +
                "positive-sense single-stranded RNA virus\n" +"NCBI taxonomy ID\n" +
                "2501928"));
        mData.add(new NewsItem("Porcine epidemic diarrhea virus","Pedacovirus",R.drawable.virus,R.drawable.virus,getString(R.string.ped) +
                "\n" +
                getString(R.string.ped1) +
                "\n" +
                getString(R.string.ped2)));
        mData.add(new NewsItem("Scotophilus bat coronavirus 512","Pedacovirus",R.drawable.virus,R.drawable.virus,getString(R.string.bat)));
        mData.add(new NewsItem("Rhinolophus bat coronavirus HKU2","Rhinacovirus",R.drawable.mt,R.drawable.virus,getString(R.string.hk)));
        mData.add(new NewsItem("Human coronavirus NL63","Setracovirus",R.drawable.f1,R.drawable.virus,getString(R.string.k) +
                "\n" +
                getString(R.string.k2)));
         mData.add(new NewsItem("Alphacoronavirus 1","Tegacovirus",R.drawable.alp,R.drawable.virus,getString(R.string.k3)));





        mData.add(new NewsItem("Human coronavirus OC43","Betacoronavirus 1",R.drawable.midcor,R.drawable.virus,getString(R.string.o) +
                "\n" +
                getString(R.string.uy)));

        mData.add(new NewsItem("Human coronavirus HKU1","HCoV-HKU1",R.drawable.bc,R.drawable.virus,getString(R.string.sp) +
                "\n" +
                getString(R.string.sd)));
        mData.add(new NewsItem("Murine coronavirus"," ",R.drawable.midcor,R.drawable.virus,getString(R.string.w) +
                "\n" +
                getString(R.string.ww)));

        mData.add(new NewsItem("Hedgehog coronavirus 1","Betacoronavirus",R.drawable.bc,R.drawable.virus,getString(R.string.hed) +
                "\n" +
                getString(R.string.ret) +
                "\n" +
                "The virus was found in the highest concentrations in the lower gastrointestinal tract."));
        mData.add(new NewsItem("Middle East respiratory syndrome-related coronavirus (MERS-CoV)","MERS-CoV",R.drawable.virus,R.drawable.virus,getString(R.string.r2) +
                "\n" +
                getString(R.string.r22) +
                "\n" +
                getString(R.string.r222) +
                "\n" +
                getString(R.string.r2222)));
        mData.add(new NewsItem("Pipistrellus bat coronavirus HKU5","Bat-CoV HKU5",R.drawable.midcor,R.drawable.virus,getString(R.string.jj)));
        mData.add(new NewsItem("Tylonycteris bat coronavirus HKU4","Bat-CoV HKU4",R.drawable.rancho,R.drawable.virus,getString(R.string.lo)));

        mData.add(new NewsItem("Rousettus bat coronavirus HKU9","HKU9-1",R.drawable.ncov,R.drawable.virus,getString(R.string.hki)));
        mData.add(new NewsItem("Wigeon coronavirus HKU20","Wigeon coronavirus HKU20",R.drawable.bc,R.drawable.virus,"Regnum: Virus\n" +
                "Group IV: ssRNA(+)\n" +
                "Ordo: Nidovirales\n" +
                "Familia: Coronaviridae\n" +
                "Subfamilia: Coronavirinae\n" +
                "Genus: Deltacoronavirus"));
        mData.add(new NewsItem("Bulbul coronavirus HKU11","Bulbul-CoV HKU11",R.drawable.virus,R.drawable.virus,getString(R.string.bul)));
        mData.add(new NewsItem("Coronavirus HKU15","HKU15",R.drawable.nop,R.drawable.virus,getString(R.string.porcil)));

        mData.add(new NewsItem("White-eye coronavirus HKU16","HKU16",R.drawable.ncov,R.drawable.virus,"Assembly level: Complete Genome\n" +
                "Assembly: GCA_000896875.1 ViralProj109273 scaffolds: 1 contigs: 1 N50: 0 L50: 0\n" +
                "Statistics:  total length (Mb): 0.026041\n" +
                " protein count: 8\n" +
                " GC%: 39.8"));
        mData.add(new NewsItem("Night heron coronavirus HKU19","HKU19",R.drawable.mt,R.drawable.virus,"Regnum: Virus\n" +
                "Group IV: ssRNA(+)\n" +
                "Ordo: Nidovirales\n" +
                "Familia: Coronaviridae\n" +
                "Subfamilia: Coronavirinae\n" +
                "Genus: Deltacoronavirus"));

        mData.add(new NewsItem("Beluga whale coronavirus SW1","SW1",R.drawable.bc,R.drawable.virus,"Beluga whale coronavirus " +
                getString(R.string.sw1)));
        mData.add(new NewsItem("Avian coronavirus","IBV",R.drawable.midcor,R.drawable.virus,getString(R.string.ivb)));
















        // adapter ini and setup

        newsAdapter = new NewsAdapter(getContext(),mData,isDark);
        NewsRecyclerview.setAdapter(newsAdapter);
        NewsRecyclerview.setLayoutManager(new LinearLayoutManager(getContext()));


        fabSwitcher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isDark = !isDark ;
                if (isDark) {

                    rootLayout.setBackgroundColor(getResources().getColor(R.color.black));

                    searchInput.setBackgroundResource(R.drawable.search_input_dark_style);

                }
                else {
                    rootLayout.setBackgroundColor(getResources().getColor(R.color.white));

                    searchInput.setBackgroundResource(R.drawable.search_input_style);
                }

                newsAdapter = new NewsAdapter(getContext(),mData,isDark);
                if (!search.toString().isEmpty()){

                    newsAdapter.getFilter().filter(search);

                }
                NewsRecyclerview.setAdapter(newsAdapter);
                saveThemeStatePref(isDark);




            }
        });















        searchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                newsAdapter.getFilter().filter(s);
                search = s;


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });






        return view;
    }

    private void saveThemeStatePref(boolean isDark) {

        SharedPreferences pref = getActivity().getSharedPreferences("myPref",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("isDark",isDark);
        editor.commit();
    }



    private boolean getThemeStatePref () {

        SharedPreferences pref = getActivity().getSharedPreferences("myPref",MODE_PRIVATE);
        boolean isDark = pref.getBoolean("isDark",false) ;
        return isDark;

    }


}
