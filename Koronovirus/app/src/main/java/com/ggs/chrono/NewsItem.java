package com.ggs.chrono;

public class NewsItem {

    private int Thumbnail ;
    String Title,Content,vkladka;
    int userPhoto;

    public NewsItem() {
    }


    public NewsItem(String title, String content,int userPhoto, int thumbnail, String vkladka) {
        Title = title;
        Content = content;
        Thumbnail = thumbnail;
        this.vkladka = vkladka;
        this.userPhoto = userPhoto;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public void setContent(String content) {
        Content = content;
    }

    public String getVkladka() {
        return vkladka;
    }

    public void setVkladka(String vkladka) {
        this.vkladka = vkladka;
    }

    public void setUserPhoto(int userPhoto) {
        this.userPhoto = userPhoto;
    }

    public String getTitle() {
        return Title;
    }

    public String getContent() {
        return Content;
    }

    public int getThumbnail() {
        return Thumbnail;
    }

    public int getUserPhoto() {
        return userPhoto;
    }
}
