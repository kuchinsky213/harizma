package com.ggs.cweta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import com.ggs.cweta.act.ChtoEto;
import com.ggs.cweta.act.Cvet12;
import com.ggs.cweta.act.OpisanieCvetotipov;
import com.ggs.cweta.act.TestCvet;
import com.ggs.cweta.act.Vajnost;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

public class MainActivity extends AppCompatActivity {
Button cto_eto, opisanie, test, ctv12, ideal;
    Animation poyavlenie;
    InterstitialAd mInterstitialAd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        MobileAds.initialize(this,
                "ca-app-pub-7802337876801234~1797183970");
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-7802337876801234/3157339438");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        cto_eto = findViewById(R.id.button);
        opisanie = findViewById(R.id.button2);
        test = findViewById(R.id.button3);
        ctv12 = findViewById(R.id.button4);
        ideal = findViewById(R.id.button5);

        poyavlenie = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fade_in);
        cto_eto.startAnimation(poyavlenie);
        opisanie.startAnimation(poyavlenie);
        test.startAnimation(poyavlenie);
        ctv12.startAnimation(poyavlenie);
        ideal.startAnimation(poyavlenie);

        cto_eto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chto = new Intent(MainActivity.this, ChtoEto.class);
                startActivity(chto);
            }
        });

        opisanie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent opis = new Intent(MainActivity.this, OpisanieCvetotipov.class);
                startActivity(opis);
            }
        });

        ctv12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ctv = new Intent(MainActivity.this, Cvet12.class);
                startActivity(ctv);
            }
        });
        ideal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent vaj = new Intent(MainActivity.this, Vajnost.class);
                startActivity(vaj);
            }
        });
        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent testc = new Intent(MainActivity.this, TestCvet.class);
                startActivity(testc);
            }
        });
    }
}
