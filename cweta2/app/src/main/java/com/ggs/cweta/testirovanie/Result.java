package com.ggs.cweta.testirovanie;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.ggs.cweta.MainActivity;
import com.ggs.cweta.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

public class Result extends AppCompatActivity {
    private final String STATE_PLAYER_NAME = "PLAYER_NAME";
    private final String STATE_SCORE = "SCORE";
    private final int NUMBER_OF_QUESTIONS = 10;
    private int score;
    private String playerName;
    private int correctAnswer;
    private TextView nameTextView;
    private TextView scoreTextView;
    private TextView outOfTextView;
    private InterstitialAd mInterstitialAd;







    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_PLAYER_NAME, playerName);
        outState.putInt(STATE_SCORE, correctAnswer);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        playerName = savedInstanceState.getString(STATE_PLAYER_NAME);
        correctAnswer = savedInstanceState.getInt(STATE_SCORE);
        FillTextPopUp();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int layoutId = R.layout.activity_result;
        setContentView(layoutId);
        Intent intent = getIntent();
        playerName = intent.getStringExtra(STATE_PLAYER_NAME);
        correctAnswer = intent.getIntExtra(STATE_SCORE, 0);
        nameTextView = findViewById(R.id.textCongrats);
        scoreTextView = findViewById(R.id.theScore);
        outOfTextView = findViewById(R.id.outOf);
        FillTextPopUp();
        MobileAds.initialize(this,
                "ca-app-pub-7802337876801234~1797183970");
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-7802337876801234/8629157963");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());


        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
              //  mInterstitialAd.show();

            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the interstitial ad is closed.
                Intent intent = new Intent(Result.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });


    }

    private void FillTextPopUp() {
        String congrats = getString(R.string.congrats);
        nameTextView.setText(String.format(congrats, playerName));

        score = 0;
        if (correctAnswer == 4){
            scoreTextView.setText(R.string.z);
            outOfTextView.setText(getString(R.string.r1) +
                    "\n" +
                    getString(R.string.r11));
        } else   if (correctAnswer == 3){
            scoreTextView.setText(R.string.o);
            outOfTextView.setText(getString(R.string.r2) +
                    "\n" +
                    getString(R.string.r22));
        } else   if (correctAnswer == 2){
            scoreTextView.setText(R.string.v);
            outOfTextView.setText(getString(R.string.r3) +
                    "\n" +
                    getString(R.string.r33));
        }
        else   if (correctAnswer == 1){
            scoreTextView.setText(R.string.l);
            outOfTextView.setText(getString(R.string.r4) +
                    "\n" +
                    getString(R.string.r44));
        }




    }

    public void startNewGame(View view) {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.");
        }

    }
}
