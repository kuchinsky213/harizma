package com.ggs.cweta.act;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import com.ggs.cweta.R;
import com.ggs.cweta.testirovanie.Main;


public class TestCvet extends AppCompatActivity {
Button nachat;
    Animation poyavlenie;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_test_cvet);

        nachat = findViewById(R.id.nachat);

        poyavlenie = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fade_in);
        nachat.startAnimation(poyavlenie);

        nachat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startQ = new Intent(TestCvet.this, Main.class);
                startActivity(startQ);
            }
        });
    }
}
